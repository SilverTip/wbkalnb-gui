# LNB_Communication
# Written by Benjamin Stadnik
# Orbital Research Ltd.

import socket
import serial
import time
import re
import threading
import serial.tools.list_ports

STOP_THREADS = False
ENCODE_FORMAT = 'utf-8'

class RS485Connection:  # TriKa/DKa/DKu communications

    STX = b'\x02'
    EXT = b'\x03'
    ACK = b'\x06'

    def __init__(self, port):
        self.addr = b'\x39\x32'
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=2.0
        )
        self.connection = True

    def close(self):
        self.ser.close()
        self.connection = False

    def test(self) -> bool:
        if self.query("$build,*"):  # Check if list has contents
            return True
        return False

    def send(self, message: str) -> None:
        command = self.STX + self.addr + message.encode(ENCODE_FORMAT) + self.EXT
        checksum = 0
        for num in command:
            checksum ^= num
        command += (checksum).to_bytes(1, byteorder='big')
        if (self.connection):
            self.ser.write(command)

    def read(self) -> list[str]:
        character = ''
        word = ''
        reply = []
        if (self.connection):
            while ((self.ser.in_waiting > 0) and (self.ser.isOpen())):
                character = self.ser.read(1)
                if (character == self.EXT):
                    checksum = self.ser.read(1)  # Next byte is checksum ignore for now
                    if (word):
                        reply.append(word)
                        word = ''
                elif (character == self.ACK):
                    if (word):
                        reply.append(word)
                        word = ''
                    address = self.ser.read(2)  # next 2 bytes are address so we can remove and ignore them (for now)
                else:
                    try:
                        character = character.decode(ENCODE_FORMAT)
                    except:
                        continue
                    word += character
        if (word):
            reply.append(word)

        return reply

    def query(self, message: str) -> list[str]:
        if (self.connection):
            self.send(message)
            time.sleep(self.delay)  # unstable <= 0.12.
            reply = self.read()

            return reply


class RS232Connection:  # WBKaLNB serial connection

    def __init__(self, port: int):
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=38400,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
        )
        self.connection = True

    def close(self):
        self.connection = False
        self.ser.close()

    def test(self) -> bool:
        if self.query("$build,*"):
            return True
        return False

    def send(self, message: str) -> None:
        if (self.connection):
            tx = bytes(str(message) + '\r\n', ENCODE_FORMAT)
            self.ser.write(tx)

    def read(self) -> list[str]:
        character = ''
        word = ''
        reply = []
        if (self.connection):
            while (self.ser.in_waiting > 0):
                character = self.ser.read(1)
                try:
                    character = character.decode(ENCODE_FORMAT)
                except:
                    # print("Failed")
                    continue  # decode of character failed
                word += character

            reply = word.split('\r\n')

        if (reply[-1]) == '':
            reply.pop(-1)

        return reply

    def query(self, message: str) -> list[str]:
        if (self.connection):
            self.send(message)
            time.sleep(self.delay)  # unstable <= 0.12.
            reply = self.read()

            return reply


class SocketConnection:  # WBKaLNB network connection
    def __init__(self, host: str, port: int):
        self.timeout = 5
        self.delay = 0.15
        self.bytes = 1024
        self.address = (host, port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.connection = False
        self.open()

    def open(self) -> None:
        self.sock.connect(self.address)
        self.connection = True

    def close(self) -> None:
        self.sock.close()
        self.connection = False

    def test(self) -> bool:
        if self.query("$build,*"):
            return True
        return False

    def send(self, message: str) -> None:
        if (self.connection):
            self.sock.sendall(bytes(str(message) + '\r\n', ENCODE_FORMAT))

    def read(self) -> list[str]:
        reply = []
        word = ''
        if (self.connection):
            received = self.sock.recv(self.bytes)
            for item in received:
                try:
                    character = chr(item)
                except:
                    # binary decode failed due to garbage non-ASCII characters
                    continue
                word += character
            reply = word.split('\r\n')
            if (reply[-1]) == '':
                reply.pop(-1)

        return reply

    def query(self, message: str) -> list[str]:
        if (self.connection):
            self.send(message)
            time.sleep(self.delay)
            reply = self.read()

            return reply

def dB_to_Hex(version: int, dB: float) -> str:
    hex_value = '0x02'
    if (dB > 31.5):  # Maximum value
        dB = 31.5
    if (dB < 0):     # Minimum value
        dB = 0
    if (version == 1):
        decimal = dB % 1
        if decimal == 0.25 or decimal == 0.75:
            dB += 0.25  # Round up
        hex_value = hex(int(63 - (dB * 2)))
    elif (version == 2):
        hex_value = hex(int(dB / 0.25))

    return hex_value

def Hex_to_dB(version: int, hex_value: str) -> float:
    dB = 0.5
    if (version == 1):
        dB = abs(int(hex_value, 16) - 63) / 2
    elif (version == 2):
        dB = int(hex_value, 16) * 0.25

    return dB

def Discover_Serial_Devices() -> list[dict]:
    def Decode_Build(message: list[str]) -> dict:
        build = {'ID': 'Unknown', 'Firmware': 'Unknown'}
        if not message:
            return build
        for item in message:
            if (item.count('$BUILD')):
                build_string = item.split(',')
                build['ID'] = build_string[1] + '-' + build_string[2]
                build['Firmware'] = build_string[4]
                break

        return build

    def List_Available_COM_Ports() -> list:
        return [int(item.device.strip('COM')) for item in list(serial.tools.list_ports.comports())]

    serial_device_info = {"Port": 'Unknown', "Type": 'Unknown', "ID": 'Unknown', "Firmware": 'Unknown'}
    # Check each device connected COM channels
    devices = []
    for port in List_Available_COM_Ports():
        device = serial_device_info.copy()
        try:
            com = RS485Connection(port)
            if com.test():
                device['Type'] = "RS485"
                build = com.query('$build,*')
                device.update(Decode_Build(build))
            com.close()
        except OSError:
            pass
        try:
            com = RS232Connection(port)
            if com.test():
                device['Type'] = "RS232"
                build = com.query('$build,*')
                device.update(Decode_Build(build))
            com.close()
        except OSError:
            pass
        device['Port'] = port
        devices.append(device)

    return devices

def Discover_Network_Devices(broadcast: bool) -> list[dict]:
    def TLV_Decode(message: bytes) -> list[dict]:
        #   Using Reqular Expressions module and Horizon ICD_R7 (20220228)
        tlv_dict = {'field': None,
                    'description': None,
                    'address': None,
                    'sub': None,
                    'len': None,
                    'contents': None,
                    'value': None
                    }

        bytes_list = [x.to_bytes(1, 'big') for x in message]

        TLV = [(1, 'Chassis_ID', b'\x02', None, hex, '^([0-9A-Fa-f]{12})$'),
               (2, 'Port_ID', b'\x04', None, int, None),
               (3, 'Time-to-Live', b'\x06', None, int, None),
               (4, 'Port_Description', b'\x08', None, str, '^(ETH0)$'),
               (5, 'System_Name', b'\x0a', None, str, '^((ORBLNB-){1}[0-9A-Fa-f]{8})$'),
               (6, 'System_Description', b'\x0c', None, str,
                '^((LNB858-900XWS60,){1}[0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
               (7, 'System_Capability', b'\x0e', None, str, '^(LNB)$'),
               (8, 'Management_Address', b'\x10', None, str,
                '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'),
               (9, 'LLDP_MED_Capabilities', b'\xfe', b'\x01', int, None),
               (10, 'Hardware_Revision', b'\xfe', b'\x05', str, '^(r[0-9]{3})$'),
               (11, 'Firmware_Revision', b'\xfe', b'\x06', str, '^([0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
               (12, 'Serial_Number', b'\xfe', b'\x08', str, '^([0-9]{4,5})(-)[1-9]{1}[0-9]{0,4})$'),
               (13, 'Manufacturer_Name', b'\xfe', b'\x09', str, '^(ORBITAL RESEARCH)$'),
               (14, 'Model_Name', b'\xfe', b'\x0a', str, '^(LNB858-900XWS60)$'),
               (15, ' Asset_ID', b'\xfe', b'\x0b', str,
                '^((LNB858-900XWS60-00-){1}([0-9A-Fa-f]{2}[.]){5}[0-9A-Fa-f]{2})$')
               ]

        tlv_list = []
        step = 0
        for i, character in enumerate(bytes_list):
            field, description, address, sub, character_format, decoder = TLV[step]
            if character == address:
                index = 2
                tlv = tlv_dict.copy()
                if sub:
                    if sub == bytes_list[i + 5]:
                        tlv['sub'] = '0x{:02x}'.format(int.from_bytes(sub, 'big'))
                        index = 6
                    else:
                        continue
                if step < len(TLV) - 1:
                    step += 1
                tlv['field'] = field
                tlv['description'] = description
                tlv['address'] = '0x{:02x}'.format(int.from_bytes(address, 'big'))
                tlv['len'] = int.from_bytes(bytes_list[i + 1], "big")
                tlv['contents'] = ''.join('/0x{:02x}'.format(x) for x in [int.from_bytes(y, "big") for y in
                                                                          bytes_list[i:i + index + tlv['len']]])
                trimmed_contents = b''.join(bytes_list[i + index:i + index + tlv['len']])
                if character_format == int:
                    tlv['value'] = int.from_bytes(trimmed_contents, 'big')
                elif character_format == hex:
                    tlv['value'] = '{:02x}'.format(int.from_bytes(trimmed_contents, 'big'))
                else:
                    tlv['value'] = trimmed_contents.decode('ascii')

                tlv_list.append(tlv)

        return tlv_list

    def TLV_to_Useful(tlv_list: list[dict]) -> dict:
        network_device_info = {"IP": 'Unknown', "ID": 'Unknown', "Model": 'Unknown', "Firmware": 'Unknown'}
        for item in tlv_list:
            if item['field'] == 8:
                network_device_info['IP'] = item['value']
            elif item['field'] == 12:
                network_device_info['ID'] = item['value']
            elif item['field'] == 14:
                network_device_info['Model'] = item['value']
            elif item['field'] == 11:
                network_device_info['Firmware'] = item['value']

        return network_device_info

    def Broadcast(device: tuple) -> list[tuple]:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.sendto(b'D\r\n', device)
        device_list = Listen(sock, 2)
        sock.close()
        return device_list

    def Listen(sock: socket.socket, timeout: int) -> list[tuple]:
        device_list = []
        sock.settimeout(timeout)
        try:
            while(True):
                device_list.append(sock.recvfrom(2048))  # Stopping function. Timeout will cause exception
        except Exception:
            return device_list

    target = '255.255.255.255'
    port_solicited = 30304
    port_unsolicited = 30303
    devices = []

    # Solicited Broadcast on port 30304
    if broadcast:
        devices = []
        responses = Broadcast((target,port_solicited))
        for response, address in responses:
            devices.append(TLV_to_Useful(TLV_Decode(response)))

        return devices

    #   Wait for unsolicited announcements
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    server.bind((target, port_unsolicited))
    responses = Listen(server, 30)  # Stopping function
    for response, address in responses:
        devices.append(TLV_to_Useful(TLV_Decode(response)))

    return devices

def Discover():
    print('Discover Serial Devices...')
    serial_devices = Discover_Serial_Devices()
    for device in serial_devices:
        print(device)
    print("Discover Network devices...")
    network_devices = Discover_Network_Devices(broadcast=True)
    for device in network_devices:
        print(device)

def Testing():
    COM_PORT = 1
    com = SocketConnection('10.0.10.89', 32019)
    # com = RS232Connection(COM_PORT)
    # com = RS485Connection(COM_PORT)
    while (1):
        try:
            message = input('Command:')
            # com.delay = 3.1
            if not com.connection:
                print("Open communications Method")
                print(com.address)
                com.open()
            reply = com.query(message)
            print(reply)
            # for item in reply: print(item)
        except KeyboardInterrupt:
            com.close()
            print('Closed communications method')

if __name__ == '__main__':
    Discover()
    #Testing()

# Version Log
# ---------------------------------------
# R1 - 2022-03-16 - Import code from KaLNB_TCP_Serial_Communication_R6.py. Rename module to include all LNBs.
# R2 - 2022-03-23 - Filter responses based on type of connection - class methods to return interested values
# R3 - 2022-04-04 - filter, attenuation, and faults not dependent on comm type.
# R4 - 2022-04-12 - updated faults
# R5 - 2022-05-04 - RS232 and Socket updated with /r/n
# R6 - 2022-05-05 - Manage decode exceptions, lengthen read delay for longest command ("cfg", ~150ms)
# R7 - 2022-07-27 - Minor timing and filter changes
# R8 - 2022-07-27 - Minor filter changes
# R9 - 2022-09-22 - Filter changes
# R10 - 20221108 -  Move Filter to external database.
#                   RS485 dump everything after ACK into item in list.
#                   Breakup send/read/query. Attenuation function round up 0.25 and 0.75 for TriKa units
# R11 - 20220216 -  Add Discover_Network_Devices and Discover_Serial_Devices
#                   Proper pythonic annotation, rename class to follow python structure
#                   Rename/split attenuation functions for ease of use
