import csv

def Load_All_Settings(file: str) -> list[dict[any]]:
    # Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)

    # Generate list of dictionaries from matrix, grouping bands together if model name left blank
    key_list = matrix[0]
    output_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            if value == key_list[index]:  # Skips all keys
                continue
            elif (key_list[index] == 'band_name' or key_list[index] == 'name'):  # Only columns preserved as string
                temp_dict[key_list[index]] = str(value)
                continue
            elif (value == 'TRUE'):
                value = True
            elif (value == 'FALSE'):
                value = False
            temp_dict[key_list[index]] = int(value)
        output_list.append(temp_dict)
    output_list.reverse()

    output_list.pop(0)

    return output_list

def DecodeFault(fault_list: list, version: int, bits: int, fault: bytes) -> str:
    fault = str(fault)
    if (fault_list):
        error_list = fault_list[version]
    else:
        return fault

    # Format string to list of bits, little endian
    number, pad, rjust, size, kind = int(fault, 16), '0', '>', bits, 'b'
    t = F'{number:{pad}{rjust}{size}{kind}}'
    bit_list = list(t)
    bit_list.reverse()

    # Decode error from list of bits
    error = ''
    for index, bit in enumerate(bit_list):
        if (bit == '1'):
            if (error_list[index] == ''):  # skip unlabelled/ignored fault bits
                continue
            error += F'{error_list[index]}, '
    if (error):
        error = error[:-2]  # pop last ', ' off list

    return error

def LoadFaultCodes(file: str) -> list[list[str]]:
    # Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)

    fault_matrix = []
    i = 0
    while (i < len(matrix[0])):
        fault_list = []
        for index, row in enumerate(matrix):
            if (index == 0):
                continue
            if (index > 32):
                break
            fault_list.append(row[i])
        fault_matrix.append(fault_list)
        i += 1

    fault_matrix.pop(0)

    return fault_matrix

def LoadCommandFilter(file: str) -> list[list[str]]:
    # Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)

    matrix.pop(0)
    filter_list = []
    command_list = []
    for index, row in enumerate(matrix):
        if (row[0] != '' and index != 0):
            filter_list.append(command_list)
            command_list = []
        row.pop(0)
        command_list.append(row)
        if (row == matrix[-1]):
            filter_list.append(command_list)

    return filter_list
