#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-31 -      Version 1. "As Benjamin see's it"
#R2 - 2022-02-02 -      Changing GUI to class structure to remove global variables
#R3 - 2022-03-01 -      Added colour. Minor reformatting of variables
#R4 - 2022-03-04 -      Properly formatting variables depending on unit type
#R5 - 2022-03-07 -      Beta Release. Minor bug fixes
#R6 - 2022-03-14 -      DKa and TriKa temporary solution - disabled IP, TriKa specific commands.
#                       Updated communication module to LNB_Communications_R1.py that includes TriKa command
#                       encoding/decoding
#R7 - 2022-04-08 -      Generate GUI based on model number.
#                       New communication module LNB_Communication R2 with built-in encoding/decoding and
#                       filtering of data.
#R8 - 2022-04-11 -      New polling algorithm increase speed of GUI
#R9 - 2022-05-24 -      Advanced features overhaul - scrollable rx commands and saved and ordered tx commands.
#                       Update to LNB_Communication_R6 with fault decoding.
#                       Offset TriKa band settings
#R10 - 2022-07-27 -     Minor improvements. Improved reply filtering. LNB_Comm_R8
#R11 - 2022-09-02 -     Changed title and password according to CDR. RTM
#R12 - 2022-09 - 09 -   Display band LO on band buttons. Add model name to master and updated model numbers
#R1.4 - 2022-09-13 -    Skip R13. Changed revision number for initial release.
#                       Solved memory leak.
#                       Add version number to GUI interface
#2.0 - 2022-10-14 -     Unit status from entry boxes to text window, all grid() to pack()
#                       for resizing windows, menu-bar with reset, exit, set thresholds, set ip, orbital information,
#                       build number, advanced features. Pack manager. Reset unit, mute DSA, and Clear SS buttons.
#                       Load model features from .csv
#2.1 - 2022-11-28 -     FS/SS to Fault_Status/System_Status.
#                       LNB_COMMAND_REF_R2.csv, tidied up settings window, advanced window.
#                       Changed advanced window from ListBox to TextBox to enabled/disable state and ease of copy/paste
#2.2 - 2023-01-25 -     WBKaLNB database update. RS232 fix. Current Removal
#2.3 - 2023-01-31 -     Band display overhaul
#2.4 - 2023-02-23 -     Move base64 icon to separate module. Move database to Database module
#                       Reformatted communication menu in preparation for discovery protocol.
#                       Added LLDP support to settings
#                       Added support for commands $getmm, $setan, $sethost


import os
import sys
import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox
from tkinter import ttk
import tkinter.font
import datetime
import webbrowser
import json
import serial.tools.list_ports

from LNB_Communication_R11 import *
import Database
import Icon64

VERSION = "2.4"

#Size parameters
WIDTH = 475
HEIGHT = 850
BUTTONWIDTH = 10
ENTRYWIDTH = 14

#Colours
BG1 = '#042B60' #Orbital dark blue
BG2 = '#36A9E1' #Orbital light blue
FG1 = 'white'

PASSWORD = '6413c550b2' #Super secure password

DEFAULT_MODEL = None #"LNB858-900XWS60"

SOCKET_PORT = 32019

def get_path(filename):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, filename)
    else:
        return filename

SETTINGS = Database.Load_All_Settings(get_path('GUI_MODEL_REF_R02_04.csv'))
FAULT_CODES = Database.LoadFaultCodes(get_path("GUI_ERROR_REF_R02_00.csv"))
COMMAND_FILTER = Database.LoadCommandFilter(get_path('GUI_COMMAND_REF_R02_04.csv'))

class GUI():
    def __init__(self):
        #super().__init__()
        self.master = tk.Tk()
        self.master.geometry(str(WIDTH)+'x'+str(HEIGHT))
        self.master.resizable(height=1,width=1)
        self.master.minsize(WIDTH, HEIGHT)
        self.master.title("Orbital KaLNB GUI")

        self.master.protocol("WM_DELETE_WINDOW", self.Quit)

        #Window Icon
        self.master.wm_iconphoto(True, tk.PhotoImage(data=Icon64.decodedImage))

        self.com = None
        self.PERIOD = 1     #seconds
        self.font = tkinter.font.Font(family="Helvetica",size=10)
        self.fontSpacer = tkinter.font.Font(family="Helvetica",size=2)
        self.fontLarge = tkinter.font.Font(family="Helvetica",size=11,weight='normal')
        self.fontLargeBold = tkinter.font.Font(family="Helvetica",size=11,weight='bold')
        self.master.configure(bg=BG1)
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        self.clock_text = tk.StringVar()
        self.clock_text.set(updatetime())
        self.ip_change_text = tk.StringVar()
        self.subnet_change_text = tk.StringVar()
        self.gateway_change_text = tk.StringVar()
        self.DHCP_change_text = tk.StringVar()
        self.quit = False

        self.buffer =       {
                            'Error':'' 
                            }

        self.hidden_buffer ={
                            'Band': 0, 'DSA_hex': None, 'DSA_f': None,
                            'serial_EN':False, 'IP_EN':False,
                            'serial_port': None,'IP_server': None,'IP_port': None,
                            'conn_EN':False, 'Mute': False
                            }
        
        self.settings_buffer = {
                            'YYYYMMDD': '',
                            'HHMMSS': '',
                            'UTCOFFSET': '',
                            'UNIXTS': '' ,          
                            'DHCP_Retries': '',
                            'Static_IP': '',
                            'Static_Subnet': '',
                            'Static_Gateway': '',
                            'MAC': '',
                            'Hostname': '',
                            'LLDP_Enable': '',
                            'LLDP_Timer': '',
                            'Temperature_High': '',
                            'Temperature_Low': '', 
                            'Voltage_High': '', 
                            'Voltage_Low': '', 
                            'Current_High': '', 
                            'Current_Low': '',
                            'Stack_L': '',
                            'CPU_H': ''
                            }

        self.settings_text = {}
        for setting in self.settings_buffer.keys():
            self.settings_text[setting] = tk.StringVar()
                    
        i = 0
        temp = {}
        for key, value in self.settings_buffer.items():
            if(i > 3):
                break
            temp.update({key:value})
            i += 1
        self.settings_buffer.clear()
        self.settings_buffer = temp.copy()
        self.settings_buffer_reset = temp.copy()

        self.hidden_buffer_reset = self.hidden_buffer.copy()

        #Pack Tracking
        global SETTINGS
        self.model_settings_list = SETTINGS
        self.frame_list = []
        self.window_list = []
        self.button_list = []

        self.model_selected = False
        self.band = {'index':[], 'name': [], 'button_display_number': [], 'button': []}
        self.first_loop = True
        self.variable = True
        self.model = None
        self.advancedcheck = 0
        self.settings_check = False

        self.Menu()
        self.Header()
        self.PackManager()
        
        self.main()

    def Header(self):
        # DiplayFrame
        self.DisplayFrame = tk.Label(self.master, font=self.font, bg=BG1, fg=FG1)
        self.logo = tk.PhotoImage(file=get_path('orblogob2.png'))
        self.logoFrame = tk.Label(self.DisplayFrame, image=self.logo, bg=BG1)
        self.logoFrame.pack(side='left', anchor='nw')
        tk.Label(self.DisplayFrame, font=self.font, textvariable=self.clock_text, bg=BG1, fg=FG1).pack(side='top',
                                                                                                       anchor='ne')
        self.statusLightBackground = tk.Canvas(self.DisplayFrame, bg=BG1, width=30, height=30, highlightbackground=BG1)
        self.statusLightBackground.pack(side='right', anchor='e')
        self.statusLight = self.statusLightBackground.create_oval(3, 3, 30, 30, fill='red')
        tk.Label(self.DisplayFrame, font=self.font, text='Connection Status:', bg=BG1, fg=FG1).pack(side='right',
                                                                                                    anchor='e')
        self.DisplayFrame.pack(side='top', anchor='e', fill='both')

    def Menu(self):
        #   Menu
        self.menubar = tk.Menu(self.master, font=self.font)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        # self.filemenu.add_command(label="Save Status as...", command=self.SaveStatusText)
        # self.filemenu.add_command(label="Advanced Features", command=self.AdvancedFeaturesWall)
        self.filemenu.add_command(label="Factory Reset", command=self.FactoryReset)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Reset GUI", command=self.ResetGUI)
        self.filemenu.add_command(label="Exit", command=self.Quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.editmenu = tk.Menu(self.menubar, tearoff=0)
        # self.editmenu.add_command(label="Unit Time", command=self.SetUnitTime)
        # self.editmenu.add_command(label="IP Address", command=self.SetUnitIP)
        self.editmenu.add_command(label="Unit Settings", command=self.UnitSettings)
        self.menubar.add_cascade(label="Settings", menu=self.editmenu)

        self.helpmenu = tk.Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Find Help Online",
                                  command=lambda url='https://orbitalresearch.net/': webbrowser.open(url))
        self.helpmenu.add_command(label="Explore Products",
                                  command=lambda url='https://orbitalresearch.net/products/': webbrowser.open(url))
        self.helpmenu.add_command(label="Contact Us",
                                  command=lambda url="https://orbitalresearch.net/contact-us/": webbrowser.open(url))
        self.helpmenu.add_command(label="About GUI...", command=self.Help)
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)

        self.master.config(menu=self.menubar)

    def PackManager(self):
        global DEFAULT_MODEL
        self.ClearGUI()
        if(DEFAULT_MODEL):
            self.model = LoadSettings('name',DEFAULT_MODEL,self.model_settings_list)
            self.PackGUI()
            return
        elif self.model_selected:
            self.model = LoadSettings('name',self.Model.get(),self.model_settings_list)
            if(self.model):
                self.PackGUI()
                return
        self.PackModelFrame()

    def ClearGUI(self):
        if(self.frame_list != []):
            for frame in self.frame_list:
                frame.pack_forget()
            self.frame_list.clear()
        if(self.window_list != []):
            for name,window in self.window_list:
                window.destroy()
            self.window_list.clear()

    def ModelSelection(self):
        self.model_selected = True
        self.PackManager()

    def ResetGUI(self):
        if not self.ModelSelected():
            return
        self.Disconnect()
        #self.Close_Communication()
        clearbuffer(self.buffer)
        self.settings_buffer = self.settings_buffer_reset.copy()
        self.hidden_buffer = self.hidden_buffer_reset.copy()
        self.master.geometry(F"{WIDTH}x{HEIGHT}")
        self.model_selected = False
        self.model = None
        self.band.clear()
        self.PackManager()    
        
    def PackModelFrame(self):
        model_options = []
        for item in self.model_settings_list:
            model_options.append(item['name'])

        #ModelFrame
        self.ModelFrame = tk.LabelFrame(self.master,font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.ModelFrame,text="Select Model:",font=self.font,bg=BG1,fg=FG1).pack(side=tk.LEFT)
        self.Model = ttk.Combobox(self.ModelFrame,value=model_options,width=40) ##Makes the menu
        self.Model.pack(side=tk.LEFT,expand=True, fill=tk.X) ## Positions the Menu
        tk.Button(self.ModelFrame,text='Enter',font=self.font,bg=BG2,fg=FG1,activebackground=BG1,
                  activeforeground=FG1,disabledforeground=FG1,command=self.ModelSelection,
                  width=BUTTONWIDTH,state='normal').pack(side=tk.RIGHT,padx=3)
        self.ModelFrame.pack(ipadx=5,ipady=5,padx=5,pady=5)
        self.frame_list.append(self.ModelFrame)

    def PackConnectionFrame(self):
        # Connection Frame
        self.ConnectionFrame = tk.LabelFrame(self.master, text="Communication Menu", font=self.font, bg=BG1, fg=FG1)
        IPFrame = tk.Label(self.ConnectionFrame, font=self.font, bg=BG1, fg=FG1)
        SerialFrame = tk.Label(self.ConnectionFrame, font=self.font, bg=BG1, fg=FG1)
        ButtonFrame = tk.Label(self.ConnectionFrame, font=self.font, bg=BG1, fg=FG1)
        self.IP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.IP_EN.set(True)
        self.IPCheckbutton = tk.Checkbutton(IPFrame, font=self.font, text='', variable=self.IP_EN,
                                            bg=BG1, fg=FG1, activebackground=BG1, selectcolor=BG2,
                                            command=lambda method=1:
                                            self.UpdateCommunication(method)
                                            )
        self.SerialCheckbutton = tk.Checkbutton(SerialFrame, font=self.font, text='',
                                                variable=self.Serial_EN, bg=BG1, fg=FG1, activebackground=BG1,
                                                selectcolor=BG2,
                                                command=lambda method=0: self.UpdateCommunication(method)
                                                )

        self.found_devices = {"COM": [], "IP": []}
        for port in list(serial.tools.list_ports.comports()):
            self.found_devices["COM"].append(port.description)
        combo_width = 30
        self.SerialPort = ttk.Combobox(SerialFrame, value=self.found_devices["COM"], state='normal', width=combo_width)
        self.IP = ttk.Combobox(IPFrame, value=self.found_devices["IP"], state='normal', width=combo_width)
        # self.TCPPort = tk.Entry(IPFrame,state='normal',disabledbackground=BG1,width=ENTRYWIDTH)

        #   Pack connection interface based on settings
        if (self.model['IP'] and self.model['serial']):
            self.SerialPort['state'] = 'disabled'
            self.SerialCheckbutton.pack(side=tk.LEFT)
            self.IPCheckbutton.pack(side=tk.LEFT)
        tk.Label(IPFrame, font=self.font, text='IP:', bg=BG1, fg=FG1,width=4,anchor=tk.E).pack(side=tk.LEFT)
        tk.Label(SerialFrame, font=self.font, text='COM:', bg=BG1, fg=FG1,width=4,anchor=tk.E).pack(side=tk.LEFT)
        self.SerialPort.pack(fill=tk.X)
        self.IP.pack(fill=tk.X)
        if self.model['IP']:
            IPFrame.pack(fill=tk.BOTH)
        if self.model['serial']:
            SerialFrame.pack(fill=tk.BOTH)

        self.connectButton = tk.Button(ButtonFrame, font=self.font, text='Connect', bg=BG2, fg=FG1,
                                       activebackground=BG2, activeforeground=FG1, command=self.Connect,
                                       width=BUTTONWIDTH)
        self.searchButton = tk.Button(ButtonFrame, font=self.font, text='Search', bg=BG2, fg=FG1,
                                      activebackground=BG2, activeforeground=FG1, command=self.Discover,
                                      width=BUTTONWIDTH)
        self.connectButton.pack(side=tk.RIGHT,padx=5)
        #self.searchButton.pack(side=tk.LEFT,padx=5)
        ButtonFrame.pack(fill=tk.BOTH)
        self.ConnectionFrame.pack(fill=tk.X)
        self.frame_list.append(self.ConnectionFrame)

    def PackGUI(self):
        self.model_selected = True
        #Model
        self.ModelTitle = tk.Label(self.master,font=self.font,text=F"Model: {self.model['name']}",bg=BG1,fg=FG1)
        self.ModelTitle.pack()
        self.frame_list.append(self.ModelTitle)
        self.buffer = {}

        self.PackConnectionFrame()

        #Band Select Frame
        self.band['index'] = [x + self.model['band_DSA_offset'] for x in range(self.model['bands'])]
        self.band['button_display_number'] = [x + self.model['band_display_offset'] for x in self.band['index']]
        
        #Add bandname if applicable
        self.band['name'] = [F"Band {self.band['button_display_number'][i]}" for i,x in enumerate(self.band['button_display_number'])]
        isalpha_flag = False
        if(self.model['band_name']):
            names = self.model['band_name'].split(',')
            for index,name in enumerate(names):
                self.band['name'].pop(index)
                self.band['name'].insert(index,name)
                if(name.isalpha()):
                    isalpha_flag = True
            if not isalpha_flag:
                self.FreqTitle = tk.Label(self.master,font=self.font,text=F"LO Frequency (GHz)",bg=BG1,fg=FG1)
                self.FreqTitle.pack()
                self.frame_list.append(self.FreqTitle) 

        #Generate and pack band buttons        
        self.BandSelectFrame = tk.Label(self.master,bg=BG1)
        frame = tk.Label(self.BandSelectFrame,bg=BG1)
        buttons_per_section = 4
        self.band['button'] = []
        count = 1
        for index, number in enumerate(self.band['index']):
            bandbutton = tk.Button(frame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text=self.band['name'][index],command=lambda band=number:self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
            self.band['button'].append(bandbutton)
            self.button_list.append(bandbutton)
            bandbutton.pack(side=tk.LEFT,padx=10,pady=5)
            count += 1
            if(count > buttons_per_section):
                count = 1
                frame.pack()
                frame = tk.Label(self.BandSelectFrame,bg=BG1)
        if count != 1:
            frame.pack()
        if self.model['bands']:
            self.BandSelectFrame.pack()
            self.frame_list.append(self.BandSelectFrame)
            self.buffer['Band'] = ''

        #DSA Scale Frame
        if(self.model['DSA']):
            self.DSA_range = (0.5,31.5)
            ScaleFrame = tk.LabelFrame(self.master,text='DSA Attenuation (dB)',font=self.font,bg=BG1,fg=FG1)
            frame1 = tk.Label(ScaleFrame,font=self.font,bg=BG1,fg=FG1)
            frame2 = tk.Label(ScaleFrame,font=self.font,bg=BG1,fg=FG1)
            frame3 = tk.Label(ScaleFrame,font=self.font,bg=BG1,fg=FG1)
            downbutton = tk.Button(frame1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text="-",command=lambda:self.DSA_Change(False),width=2,height=1,bd=6,state='disabled')
            upbutton = tk.Button(frame3,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text="+",command=lambda:self.DSA_Change(True),width=2,height=1,bd=6,state='disabled')
            if(self.model['DSA'] == 1):
                self.DSA_resolution = 0.5

                self.DSAScale = tk.Scale(frame2,from_=str(self.DSA_range[0]),to=str(self.DSA_range[1]),resolution=self.DSA_resolution,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')
            else:
                self.DSA_resolution = 0.25
                self.DSAScale = tk.Scale(frame2,digits=4,from_=str(self.DSA_range[0]),to=str(self.DSA_range[1]),resolution=self.DSA_resolution,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')      
            self.DSAScale.bind("<ButtonRelease-1>", self.updateDSAfromSlider)
            #ScaleFrame.grid_propagate(0)
            downbutton.pack(side=tk.BOTTOM)
            self.DSAScale.pack(fill='both',padx=5,pady=5)
            upbutton.pack(side=tk.BOTTOM)
            frame1.pack(fill=tk.Y,side=tk.LEFT)
            frame3.pack(fill=tk.Y,side=tk.RIGHT)
            frame2.pack(fill='both')
            ScaleFrame.pack(fill='both',ipadx=5,padx=5,pady=5,expand=False)
            self.buffer['DSA'] = ''
            self.frame_list.append(ScaleFrame)
            self.button_list.append(upbutton)
            self.button_list.append(downbutton)

        #Refresh Frame
        self.RefreshFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.RefreshFrame,text='   Refresh:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,columnspan=1, sticky='SE') 
        tk.Label(self.RefreshFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=3, sticky='SW')   
        self.PeriodScale = tk.Scale(self.RefreshFrame,from_='1',to='30', length=300, orient='horizontal',state='normal',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1)
        self.PeriodScale.grid(row=1,column=2,columnspan=1,sticky='W')
        self.PeriodScale.set(self.PERIOD)
        self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        #self.RefreshFrame.pack()

        #Additional Features
        self.AdditionalFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        self.ResetButton = tk.Button(self.AdditionalFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text="Reset Unit",command=self.Reset,width=BUTTONWIDTH,bd=6,state='disabled')
        self.ClearAllButton = tk.Button(self.AdditionalFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text="Clear System_Status",command=lambda com="$clral,*": self.Send(com),width=BUTTONWIDTH+7,bd=6,state='disabled')
        self.MuteDSAButton = tk.Button(self.AdditionalFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text="Mute Unit",command=self.MuteDSA,width=BUTTONWIDTH,bd=6,state='disabled')
        self.button_list.append(self.ResetButton)
        self.button_list.append(self.ClearAllButton)
        self.button_list.append(self.MuteDSAButton)
        self.ResetButton.pack(side=tk.LEFT,padx=5,pady=2)
        if(self.model['filter'] > 0):
            self.ClearAllButton.pack(side=tk.LEFT,padx=5,pady=2)
        if(self.model['Mute']):
            self.MuteDSAButton.pack(side=tk.LEFT,padx=5,pady=2)
        self.AdditionalFrame.pack()
        self.frame_list.append(self.AdditionalFrame)

        #Status Frame
        if(self.model['Voltage']):
            self.buffer['Voltage'] = ''
        if(self.model['Current']):
            self.buffer['Current'] = ''
        if(self.model['Temperature']):
            self.buffer['Temperature'] = ''
        if(self.model['Uptime']):
            self.buffer['Uptime'] = ''
        self.buffer['Serial_Number'] = ''
        self.buffer['Firmware_Ver'] = ''
        if self.model['info']:
            self.buffer['Hardware_Ver'] = ''
            self.buffer['Mfg'] = ''
            self.buffer['Model'] = ''
            self.buffer['Hostname'] = ''
        if self.model['log']:
            self.buffer['Stack_WM'] = ''
            self.buffer['CPU'] = ''
            self.buffer['CPU_WM'] = ''
        if(self.model['filter'] > 0):
            self.buffer['Fault_Status'] = ''
            self.buffer['System_Status'] = ''
        else:
            self.buffer['Fault'] = ''
        self.buffer['Error'] = ''
        self.StatusFrame = tk.LabelFrame(self.master,text='Unit Status',font=self.font,bg=BG1,fg=FG1)
        self.StatusWindow = tk.Text(self.StatusFrame,font=self.fontSpacer,state='normal',height=len(self.buffer)+1,undo=False,wrap=tk.WORD)
        #self.StatusWindow.set(yscrollcommand=False)
        self.StatusWindow.pack(expand=tk.YES, fill=tk.BOTH, ipadx=5,padx=5,ipady=5,pady=5)
        self.StatusFrame.pack(expand=tk.YES, fill=tk.BOTH, ipadx=5,padx=5,ipady=5,pady=5)
        self.frame_list.append(self.StatusFrame)
        self.UpdateStatusWindow()

    def bandbutton(self,number):
        self.Send(F"$setst,{number},*")
        self.first_loop = True

    def Discover(self):
        frame = self.Popup("Searching for devices...",200,200)
        self.found_devices['COM'].clear()
        self.found_devices['IP'].clear()

        self.SerialPort['values'] = self.found_devices['COM']
        self.IP['values'] = self.found_devices['IP']

    def UnitSettings(self):
        if not self.ModelSelected():
            return
        if not self.ModelSupport('filter'):
            return
        if WindowExists(self.window_list, 'Settings'):
            return
        
        state = 'disabled'
        bg = BG1
        if self.hidden_buffer['conn_EN']:
            state = 'normal'
            bg = BG2
        
        width = 15

        SettingsFrame = self.Popup('Unit Settings',300,600)

        #Menu
        menubar = tk.Menu(SettingsFrame,font=self.font)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Load Settings", command=self.LoadSettings)
        filemenu.add_command(label="Save Settings as...", command=self.SaveSettings)
        menubar.add_cascade(label="File", menu=filemenu)
        SettingsFrame.config(menu=menubar,pady=4)

        #Settings
        if(self.model['IP']):
            IP = {'DHCP_Retries': '',
                  'Static_IP': '',
                  'Static_Gateway': '',
                  'Static_Subnet': '',
                  'MAC': '',
                  'Hostname': '',
                  'LLDP_Enable': '',
                  'LLDP_Timer': ''
                  }
            self.settings_buffer.update(IP)

        thresholds = {
                    'Temperature_High': '',
                    'Temperature_Low': '', 
                    'Voltage_High': '', 
                    'Voltage_Low': '', 
                    'Current_High': '', 
                    'Current_Low': '',
                    'Stack_L': '',
                    'CPU_H': ''
                    }

        for key, value in thresholds.items():
            if(self.model[key]):
                self.settings_buffer.update({key:value})

        largest_key = ''
        for key in self.settings_buffer.keys():
                if(len(key) > len(largest_key)):
                    largest_key = key

        for key in self.settings_buffer.keys():
            frame = tk.Label(SettingsFrame,font=self.font,bg=BG1,fg=FG1)
            tk.Entry(frame,textvariable=self.settings_text[key],width=width+3).pack(side=tk.RIGHT)         
            tk.Label(frame,text=F"{key}",font=self.font,bg=BG1,fg=FG1,width=len(largest_key)-2,anchor=tk.E).pack(side=tk.LEFT) 
            tk.Label(frame,text=F":",font=self.font,bg=BG1,fg=FG1).pack(side=tk.LEFT) 
            frame.pack()

        #timebuttonframe = tk.Label(SettingsFrame,font=self.font,bg=BG1,fg=FG1)
        #self.AutoSetTimeButton = tk.Button(timebuttonframe,text='Auto Set Time',font=self.font,bg=bg,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.AutoSetTime,width=buttonwidth,state=state)
        #self.AutoSetTimeButton.pack(padx=5,pady=2)
        #timebuttonframe.pack(fill=tk.X)
        #self.button_list.append(self.AutoSetTimeButton)

        buttonframe = tk.Label(SettingsFrame,font=self.font,bg=BG1,fg=FG1)                                                         
        self.GetSettingsButton = tk.Button(buttonframe,text='Get',font=self.font,bg=bg,fg=FG1,activebackground=BG2
                                           ,activeforeground=FG1,disabledforeground=FG1,command=self.GetSettings,
                                           width=BUTTONWIDTH,state=state)
        self.SetSettingsButton = tk.Button(buttonframe,text='Set',font=self.font,bg=bg,fg=FG1,
                                           activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,
                                           command=self.SetSettings,width=BUTTONWIDTH,state=state)
        self.GetSettingsButton.pack(side=tk.LEFT,padx=5,pady=2)
        self.SetSettingsButton.pack(side=tk.LEFT,padx=5,pady=2)
        self.button_list.append(self.GetSettingsButton)
        self.button_list.append(self.SetSettingsButton)
        buttonframe.pack(padx=5,pady=2)    
        
        self.window_list.append(('Settings',SettingsFrame))

    def AutoSetTime(self):
        self.donothing()

    def GetSettings(self):
        settings_messages = ['$wrrtc,*','$wrutc,*','$getip,*','$setip,*','$setth,*', '$setan,*', '$sethost,*']

        for message in settings_messages:
            self.SendSettings(message)

        for key in self.settings_buffer.keys():
            self.settings_text[key].set(self.settings_buffer[key])

        #self.buffer['Error'] = self.settings_buffer

    def SetSettings(self):
        for key in self.settings_buffer.keys():
            self.settings_buffer[key] = self.settings_text[key].get().strip() #Remove leading and final whitespaces

        #Set Time
        self.SendSettings(F"$wrrtc,{self.settings_buffer['YYYYMMDD']},{self.settings_buffer['HHMMSS']},\
                            {self.settings_buffer['UTCOFFSET']},*")
        self.SendSettings(F"$wrutc,{self.settings_buffer['UNIXTS']},*")
            
        #Set Thresholds
        if(self.model['filter'] > 0):
            message = F"$setth,{self.settings_buffer['Current_High']},{self.settings_buffer['Current_Low']},\
                        {self.settings_buffer['Temperature_High']},{self.settings_buffer['Temperature_Low']},\
                        {self.settings_buffer['Voltage_High']},{self.settings_buffer['Voltage_Low']},\
                        {self.settings_buffer['Stack_L']},{self.settings_buffer['CPU_H']},*"
            self.SendSettings(message)

        #Set Network
        if(self.model['IP']):
            self.SendSettings(F"$setip,{self.settings_buffer['DHCP_Retries']},{self.settings_buffer['Static_IP']},\
                                {self.settings_buffer['Static_Subnet']},{self.settings_buffer['Static_Gateway']},*")
            self.SendSettings(F"$setan,{self.settings_buffer['LLDP_Enable']},{self.settings_buffer['LLDP_Timer']},*")

        if self.model['info']:
            self.SendSettings(F"$sethost,{self.settings_buffer['Hostname']},*")

        self.first_loop = True

    def SendSettings(self, message):
        self.settings_check = True
        reply = []
        try:
            if(self.com):
                #self.buffer['Error'] += [message]
                reply = self.com.query(message)
                #self.buffer['Error'] += [reply]
            
            if(reply == []):
                return

            #Filter all item in reply    
            for item in reply:      
                if(item.count('$')):
                    newbuffer = self.CommandFilter(self.model['filter'],item)
                    n = list(newbuffer.keys())
                    k = list(self.settings_buffer.keys())
                    for key in n:
                        if not k.count(key):
                            newbuffer.pop(key)
                    self.settings_buffer.update(newbuffer)
                        
        except Exception as e:
            self.Handle_Exception(e)

        return reply

    def LoadSettings(self): 
        try:
            path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.json"), ("All files", "*.*"))).name     
                    
            with open(path, 'r') as f:
                temp_buffer = json.load(f)

            self.settings_buffer.update(temp_buffer)
            for key in self.settings_buffer.keys():
                self.settings_text[key].set(self.settings_buffer[key])
            
        except AttributeError:
            #No selection
            pass            

    def SaveSettings(self):
        try:
            path = tk.filedialog.asksaveasfile(initialfile='Untitled.json', filetypes=(("Text files", "*.json"), ("All files", "*.*"))).name

            temp = {}
            for key in self.settings_buffer.keys():
                temp[key] = self.settings_text[key].get()

            with open(path, 'w') as f:
                json.dump(temp,f)

        except AttributeError:
            #No selection
            pass

    def MuteDSA(self):
        self.hidden_buffer['Mute'] = not self.hidden_buffer['Mute']
        self.Send(F"$muteda,0,{int(self.hidden_buffer['Mute'])},*")
        self.first_loop = True

    def AdvancedFeaturesWall(self):
        if not (self.advancedcheck > 3):
            self.advancedcheck += 1 
            return
        self.advancedcheck = 0 

        if WindowExists(self.window_list,'Advanced'):
            return      

        self.AdvancedWindow = self.Popup("Advanced Features",700,300)
        self.window_list.append(('Advanced',self.AdvancedWindow))

        self.PasswordFrame = tk.Label(self.AdvancedWindow,font=self.font,bg=BG1,fg=FG1)
        self.pass_text = tk.StringVar()
        tk.Label(self.PasswordFrame,text='Enter Password:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
        tk.Entry(self.PasswordFrame,textvariable=self.pass_text,state='normal').grid(row=1,column=2)
        self.PasswordButton = tk.Button(self.PasswordFrame,font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Enter',command=self.passcheck,width=BUTTONWIDTH)
        self.PasswordButton.grid(row=1,column=3,padx=5)
        self.PasswordFrame.pack(ipadx=5,ipady=5,padx=5,pady=5) 

    def passcheck(self):
        if(self.pass_text.get() == PASSWORD):
            self.PasswordFrame.pack_forget()
            self.AdvancedFeatures()
        #else:
            #tk.Label(self.PasswordFrame,text='Password Incorrect',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2)
            #self.AdvancedWindow.update()

    def AdvancedFeatures(self):
        state = 'disabled'
        bg = BG1
        if self.hidden_buffer['conn_EN']:
            state = 'normal'
            bg = BG2

        #Command Profile Frame
        CommandProfileFrame = tk.LabelFrame(self.AdvancedWindow,text="Custom Command Macro",font=self.font,bg=BG1,fg=FG1) #Select table from .csv
        path_text = tk.StringVar()
        tk.Button(CommandProfileFrame,text='Load',command=lambda:self.LoadCommandProfile(path_text),width=BUTTONWIDTH,bd=2,font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,state='normal').pack(side=tk.LEFT,padx=2)
        tk.Entry(CommandProfileFrame,textvariable=path_text,width=40,font=self.font).pack(fill=tk.X,expand=True,side=tk.LEFT)
        run_profile_button = tk.Button(CommandProfileFrame,text='Run',command=lambda:self.RunCommandProfile(path_text.get()),width=BUTTONWIDTH,bd=2,font=self.font,bg=bg,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,state=state)
        self.button_list.append(run_profile_button)
        run_profile_button.pack(side=tk.RIGHT,padx=4)
        CommandProfileFrame.pack(fill=tk.X,side=tk.BOTTOM)

        #Command Frame
        self.txlist = ['$build,*']
        CommandFrame = tk.LabelFrame(self.AdvancedWindow,text="Command Window",font=self.font,bg=BG1,fg=FG1)

        #Send items
        TxFrame = tk.Label(CommandFrame,font=self.font,bg=BG1,fg=FG1)
        frame_left = tk.Label(TxFrame,font=self.font,bg=BG1,fg=FG1)
        frame_middle = tk.Label(TxFrame,font=self.font,bg=BG1,fg=FG1)
        frame_right = tk.Label(TxFrame,font=self.font,bg=BG1,fg=FG1)
        tk.Label(frame_left,text="TX:",font=self.font,bg=BG1,fg=FG1).pack()
        self.Command = ttk.Combobox(frame_middle,value=self.txlist) ##Makes the menu
        self.Command.pack(expand=tk.YES, fill=tk.BOTH)
        SendButton = tk.Button(frame_right,text='Send',font=self.font,bg=bg,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.CommandLineSend,width=BUTTONWIDTH,state=state)
        self.button_list.append(SendButton)
        SendButton.pack()
        frame_left.pack(side=tk.LEFT,fill=tk.Y)
        frame_right.pack(side=tk.RIGHT,fill=tk.Y)
        frame_middle.pack(fill='both') 
        TxFrame.pack(fill=tk.X)

        #Receive items
        RxFrame = tk.Label(CommandFrame,font=self.font,bg=BG1,fg=FG1)
        frame_left = tk.Label(RxFrame,font=self.font,bg=BG1,fg=FG1)
        frame_middle = tk.Label(RxFrame,font=self.font,bg=BG1,fg=FG1)
        frame_right = tk.Label(RxFrame,font=self.font,bg=BG1,fg=FG1)
        tk.Label(frame_left,text="RX:",font=self.font,bg=BG1,fg=FG1).pack(side=tk.TOP)      
        self.rxText = tk.Text(frame_middle,font=self.font,state='normal',undo=False,wrap=tk.WORD)
        self.rxText.pack(side=tk.LEFT,fill='both',expand=True)
        ClearButton = tk.Button(frame_right,text='Clear Rx',font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,command=self.ClearRx,width=BUTTONWIDTH,state='normal')
        ClearButton.pack(side=tk.TOP)
        frame_left.pack(side=tk.LEFT,fill=tk.Y)
        frame_right.pack(side=tk.RIGHT,fill=tk.Y)
        frame_middle.pack(fill='both') 
        RxFrame.pack(fill='both', expand=True)
        CommandFrame.pack(fill='both')

    def RunCommandProfile(self, filepath):
        if(filepath == ''):
            tk.messagebox.showwarning("Error", "No file selected")
            return

        command_list = []
        with open(filepath, 'r') as f:
            command_list = f.readlines()

        for command in command_list:
            self.Command.set(command.strip('\n'))
            self.CommandLineSend()

    def LoadCommandProfile(self, path_text):
        path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name
        path_text.set(path)

    def SaveCommandProfile(self):
        path = tk.filedialog.asksaveasfile(initialfile='Untitled.txt', filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name

        global txlist
        txt = ''
        for command in txlist:
            txt += F"{command}\n"

        with open(path, 'w') as f:
            f.write(txt)

    def SaveStatusText(self):
        if not self.ModelSelected():
            return
        temp = ''    
        if(self.buffer['CID'] and self.buffer['UID']):
            temp = F"{self.buffer['CID']}-{self.buffer['UID']}_"
        path = tk.filedialog.asksaveasfile(initialfile=temp+'Status.txt', filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name

        total = {}
        total.update(self.buffer)
        total.update(self.hidden_buffer)
        total.update(self.thresholds_buffer)
        txt = ''
        for key, value in total.items():
            txt += F"{key}: {value}\n"

        with open(path, 'w') as f:
            f.write(txt)

    def updatePeriod(self,event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)

    def Reset(self):
        self.clear()
        self.Send(F'$reset,*')
        self.Disconnect()

    def updateDSAfromSlider(self,event):
        if(self.hidden_buffer['conn_EN']):
            value = float(self.DSAScale.get())
            #self.DSAScale.set(value)
            self.master.update()
            atten = dB_to_Hex(self.model['DSA'], value)
            if(checkdict(self.hidden_buffer, 'Band')):
                self.Send('$setda,'+str(self.hidden_buffer['Band'] - self.model['band_DSA_offset'])+','+str(atten)+',*')
            self.first_loop = True

    def updateDSA(self):
        if(self.hidden_buffer['conn_EN']):
            value = float(self.DSAScale.get())
            #self.DSAScale.set(value)
            self.master.update()
            atten = dB_to_Hex(self.model['DSA'],value)
            if(checkdict(self.hidden_buffer,'Band')):
                self.Send('$setda,'+str(self.hidden_buffer['Band'] - self.model['band_DSA_offset'])+','+str(atten)+',*')
            self.first_loop = True

    def DSA_Change(self,direction):
        if(direction):
            value = float(self.DSAScale.get()) + self.DSA_resolution
        else:
            value = float(self.DSAScale.get()) - self.DSA_resolution
        if((value > self.DSA_range[1]) or (value < self.DSA_range[0])):
            return
        self.DSAScale.set(value)
        self.updateDSA()

    def UpdateCommunication(self,method):
        if(method == 1):
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            #self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.IP_EN.set(not self.IP_EN.get())
            self.IP['state'] = 'disabled'
            #self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'        
        self.Disconnect()

    def Connect(self):
        #self.TCPPort.get()
        port = filterPort(self.SerialPort.get()) #Ideally, I pass the serial.tools variable and callm port.device to get the COMX connection
        if not self.hidden_buffer['conn_EN']:
            if(self.Serial_EN.get()):
                if(self.SerialPort.get()):
                    try: 
                        self.StatusLightFlash('orange', False)
                        if(self.model['serial'] == 2):                 
                            self.com = RS232Connection(port)
                        else:
                            self.com = RS485Connection(port)
                        self.hidden_buffer['conn_EN'] = True   
                    except Exception as e:
                        self.buffer['Error'] = e
                        self.UpdateStatusWindow()
                        self.StatusLightFlash('red', True)
                    
            elif(self.IP_EN.get()):
                address = self.IP.get()
                if address:
                    if(type(address) == tuple):
                        address = address[1]
                    try:
                        self.StatusLightFlash('orange', False)             
                        self.com = SocketConnection(address, SOCKET_PORT)
                        self.com.delay = 0.3
                        self.hidden_buffer['conn_EN'] = True   
                    except Exception as e:
                        self.buffer['Error'] = e
                        self.UpdateStatusWindow()
                        self.StatusLightFlash('red', True)        
            if(self.hidden_buffer['conn_EN']):
                self.connectButton['text'] = 'Disconnect'
                self.statusLightBackground.itemconfig(self.statusLight,fill='light green')
                self.EnableButtons('state', 'normal')
                self.EnableButtons('bg', BG2)
                self.DSAScale['state'] = 'normal'
                self.first_loop = True
                
                self.Poll()
        else:           
            self.Disconnect()

    def Disconnect(self):
        self.hidden_buffer['conn_EN'] = False
        self.connectButton['text'] ='Connect'
        self.statusLightBackground.itemconfig(self.statusLight,fill='red')
        self.clear()
        self.EnableButtons('state', 'disabled')
        self.EnableButtons('bg', BG1)
        self.DSAScale['state'] = 'disabled'
        self.master.update()
        self.Close_Communication()

    def Quit(self):
        self.quit = True
        self.Close_Communication()
        self.master.destroy()
        sys.exit()

    def Close_Communication(self):
        if(self.com != None):
            self.com.close()
            self.com = None
        
    def CommandLineSend(self):
        message = self.Command.get()
        if(self.txlist.count(message) > 0):
            self.txlist.remove(message)
        self.txlist.insert(0, message)
        self.Command['values'] = self.txlist
        delay = self.com.delay
        self.com.delay = 1
        reply = self.Send(message)
        self.com.delay = delay
        self.rxText['state'] = 'normal'
        reply_string = ''
        for items in reply:
            reply_string += items
        self.rxText.insert(tk.END, reply_string + '\n')
        self.rxText.see(tk.END)
        self.rxText['state'] = 'disabled'  

    def ClearRx(self):
        self.rxText['state'] = 'normal'
        self.rxText.delete('1.0', tk.END)
        self.rxText['state'] = 'disabled'
    
    def clear(self):
        clearbuffer(self.buffer)
        self.UpdateStatusWindow()
        self.DSAScale.set('0.5')
        self.first_loop = True

    def clear_band_colour(self):
        for button in self.band['button']:
            button['bg'] = BG2

    def UpdateStatusWindow(self):
        self.StatusWindow['state'] = 'normal'
        self.StatusWindow.delete('1.0', tk.END)
        line = 1
        for key, value in self.buffer.items():
            self.StatusWindow.insert(tk.END, F"{key}:  {value}\n")
            self.StatusWindow.tag_add(F"{line}", F"{line}.0",  F"{line}.{len(key)+1}")
            self.StatusWindow.tag_config(F"{line}", font=self.fontLargeBold)
            self.StatusWindow.tag_add(F"{line}.1", F"{line}.{len(key)+2}",  F"{line}.end")
            self.StatusWindow.tag_config(F"{line}.1", font=self.fontLarge)
            line += 1
            self.StatusWindow.insert(tk.END, F"\n")
            line += 1
        self.StatusWindow['state'] = 'disabled'
            
    def EnableButtons(self, option, newstate): 
        for button in self.button_list: 
            try:
                button[option] = newstate
            except:
                pass

    def StatusLightFlash(self,colour,flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight,'fill')  
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
        self.master.update()
        if(flash == True):
            colour2 = 'dark ' + colour
            while(i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
                self.master.update()
                time.sleep(delay/2)
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour2)
                self.master.update()
                time.sleep(delay/2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
            self.master.update()    

    def Send(self, message):
        reply = []
        try:
            if(self.com):
                reply = self.com.query(message)             

            if(reply == []):
                return

            # Filter all item in reply
            for item in reply:      
                if(item.count('$')):
                    newbuffer = self.CommandFilter(self.model['filter'],item)
                    self.buffer.update(newbuffer)
                        
        except Exception as e:
            self.Handle_Exception(e)

        return reply

    def main(self):
        poll_timer = time.time()
        error_timer = time.time()
        error_trigger = False
        while(1):
            if self.quit:
                break
            timer = time.time()
            self.clock_text.set(updatetime())

            if(timer - poll_timer > self.PERIOD and not self.quit):
                if(self.hidden_buffer['conn_EN']):
                    if(self.settings_check):
                        self.settings_check = False
                    else:
                        self.Poll()
                poll_delay = timer
                if(self.buffer['Error'] and not error_trigger):
                    error_trigger = True
                    error_timer = timer
            
            if(error_trigger):
                if(timer - error_timer > 10):
                    self.buffer['Error'] = ''
                    try:
                        self.UpdateStatusWindow()
                    except:
                        pass
                    error_trigger = False

            if self.quit:
                break 
            self.master.update_idletasks() #    Use instead of mainloop()
            if self.quit:
                break 
            self.master.update()
            if self.quit:
                break 
            for name,window in self.window_list:
                if self.quit:
                    break 
                window.update()
            time.sleep(0.01)

    def Poll(self):
        #Get state
        self.Send('$getst,*')

        if(self.model['bands']):
            index = self.hidden_buffer['Band'] - self.model['band_DSA_offset']
            self.clear_band_colour()
            self.band['button'][index]['bg'] = 'green' 

        if(self.first_loop):
            self.Send('$build,*')
            self.Send(F"$setda,{self.hidden_buffer['Band']},*")
            if(self.buffer['DSA']):
                self.first_loop = False
                self.DSAScale.set(self.hidden_buffer['DSA_f'])
            if self.model['info']:
                self.Send('$getmm,*')
            if self.model['log']:
                self.Send('$getlog,*')

        if(self.model['Mute']):
            if(self.hidden_buffer['DSA_hex'] == '0x7F'):
                self.hidden_buffer['Mute'] = True
                self.MuteDSAButton['bg'] = 'red'
            else:
                self.hidden_buffer['Mute'] = False
                self.MuteDSAButton['bg'] = BG2  
        
        self.UpdateStatusWindow()

    def Handle_Exception(self,exception):
        self.buffer['Error']  = Catch_Exception(exception)
        self.UpdateStatusWindow()

    def Popup(self,name,width,height):
        self.window = tk.Toplevel(self.master)
        self.window.title(F"{name}")
        self.window.geometry(F"{width}x{height}")
        self.window.minsize(width, height)
        self.window.configure(bg=BG1)
        self.window.resizable(height=1,width=1)

        return self.window

    def Help(self):
        if WindowExists(self.window_list,'Help'):
            return
        self.helpwindow = self.Popup("About",250,200)
        self.window_list.append(('Help',self.helpwindow))
        self.helpwindow.resizable(height=0,width=0)
        tk.Label(self.helpwindow, image=self.logo,bg=BG1).pack()
        tk.Label(self.helpwindow,text=F"Copyright © 2023 Orbital Research Ltd",font=self.font,bg=BG1,fg=FG1).pack()
        #tk.Label(self.helpwindow,text=F"Build: {VERSION}",font=self.font,bg=BG1,fg=FG1).pack()
        tk.Button(self.helpwindow,font=self.font,bg=BG1,fg=FG1,activebackground=BG1,activeforeground=FG1,disabledforeground=FG1,text=F"Build: {VERSION}",command=self.AdvancedFeaturesWall,bd=0,state='active').pack()
        tk.Button(self.helpwindow,font=self.font,bg=BG1,fg=FG1,activebackground=BG1,activeforeground=FG1,disabledforeground=FG1,text='https://orbitalresearch.net',command=lambda url='https://orbitalresearch.net/':webbrowser.open(url),bd=0,state='active').pack()

    def ModelSelected(self):
        if(self.model_selected):
            return True
        tk.messagebox.showwarning("Warning", "Please select model")
        return False

    def ModelSupport(self, key):
        if(key == 'filter'):
            if(self.model['filter'] >= 1):
                return True
        if(key == 'IP'):
            if(self.model['IP']):
                return True
        tk.messagebox.showwarning("Warning", "Model does not support functionality")
        return False

    def FactoryReset(self):
        if not self.ModelSelected():
            return
        if not self.ModelSupport('filter'):
            return
        if not self.com:
            tk.messagebox.showwarning("Warning", "No connection found")
            return
        answer = tk.messagebox.askokcancel("Factory Reset", "Do you wish to reset this unit?")
        if(answer):
            self.Send("$factory,*")

    def donothing(self):
        if not self.ModelSelected():
            return
        tk.messagebox.showwarning("Warning", "Functionality under construction")

    def Testing(self,item):
        self.buffer['Error'] = str(item)
        self.UpdateStatusWindow()

    def CommandFilter(self, version, message):
        newbuffer = {}
        command_keys = []
        reply = message.split(',')
        reply.pop(-1) #remove '*'

        try:
            if COMMAND_FILTER:
                for command in COMMAND_FILTER[version]:
                    if(command[0] == reply[0].lower()):
                        command_keys = command
            
            if(command_keys):
                for index,value in enumerate(reply):
                    if(command_keys[index] != '-'):
                        flag = False
                        temp = {}
                        temp_key = ''
                        temp_value = ''
                    if(index == 0): #skip identifier
                        continue
                    elif(command_keys[index] == '-' or flag): #grouping items to leftmost key with "-" character
                        if not flag:
                            flag = True
                            temp_key = command_keys[index-1]
                            temp_value = reply[index-1] 
                        temp_value += ', ' + value
                        temp = {temp_key:temp_value}
                        newbuffer.update(temp)
                    elif(command_keys[index].isdigit()): #skip numerical placeholders
                        continue
                    elif(command_keys[index] == ''): #if key is blank
                        continue
                    else:
                        newbuffer[command_keys[index]] = reply[index]
        
                #Special cases + units
                for key, value in newbuffer.items():
                    if(key == 'Band'):
                        self.hidden_buffer['Band'] = int(value)
                        newbuffer[key] = str(self.hidden_buffer['Band'] + self.model['band_display_offset'])
                    elif(key == 'Voltage'):
                        if(version > 0):
                            newbuffer[key] = str(round(float(newbuffer[key])*0.1,1))
                        newbuffer[key] += ' V'
                    elif key == 'DSA':
                        self.hidden_buffer['DSA_hex'] = value
                        self.hidden_buffer['DSA_f'] = Hex_to_dB(self.model['DSA'], str(value))
                        if(self.hidden_buffer['DSA_f'] > self.DSA_range[1]):
                            newbuffer[key] = 'MUTE'
                        else:
                            newbuffer[key] = str(self.hidden_buffer['DSA_f']) + ' dB'
                    elif(key == 'Current'):
                        newbuffer[key] += ' mA'
                    elif(key == 'Temperature'):
                        newbuffer[key] += ' C'
                    elif(key == 'Uptime'):
                        newbuffer[key] += ' seconds'
                    elif(key == 'Fault'):
                        newbuffer[key] = Database.DecodeFault(FAULT_CODES,0,16,newbuffer[key])
                    elif(key =='Fault_Status'):
                        newbuffer[key] = Database.DecodeFault(FAULT_CODES,1,32,newbuffer[key])
                    elif(key == 'System_Status'):
                        newbuffer[key] = Database.DecodeFault(FAULT_CODES,2,32,newbuffer[key])
                    elif(key == 'CPU' or key.count('WM')):
                        newbuffer[key] += ' %'
                    #elif(key == 'RCODE'):
                        #if(value == '200'): #Error
                            #newbuffer.pop('Error')
                        #newbuffer.pop('RCODE')

        except Exception as e:
            error = Catch_Exception(e)
            newbuffer['Error'] = F"Command Filter Exeption: {reply[0]},{error[0],error[3]}: len(command_keys){len(command_keys)}, len(reply){len(reply)}"
        
        try:
            newbuffer['Serial_Number'] = newbuffer['CID'] + '-' + newbuffer['UID']
            newbuffer.pop('CID')
            newbuffer.pop('UID')
        except:
            pass

        return newbuffer

def WindowExists(window_list,name):
    for n,window in window_list:
        if(n == name):
            if(tk.Toplevel.winfo_exists(window)):
                window.deiconify()
                return True
            else:
                window_list.remove((n,window))
                return False
    return False

def Catch_Exception(exception):
    error = ''
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error = [exc_type ,fname,exc_tb.tb_lineno,exception]

    if(exc_type == IndexError):
        error = ''

    return error

def updatetime():
    timer = datetime.datetime.now()
    clock = timer.strftime("%Y-%b-%d  %H:%M:%S")

    return str(clock)

def clearbuffer(dictionary):
    for key in dictionary:
        if(type(dictionary[key]) != bool):
            dictionary[key] = ''
        elif(type(dictionary[key]) == bool):
            dictionary[key] = False

def LoadSettings(key, identifier, settings_list):
    for settings in settings_list:
        if(settings[key] == identifier):
            return settings
    return None

def FormatText(text, max_characters):
    temp = text.zfill(max_characters)
    spaced_text = temp.replace('0',' ',max_characters - len(text))

    return spaced_text

def filterPort(text: any) -> str:
    #filter port number from string
    if type(text) == tuple:
        text = text[1]
    stripped_port = text.split('COM')[-1]
    filtered_port = ''
    for item in stripped_port:
        if(item.isdigit()):
            filtered_port += item

    return filtered_port

def checkdict(dictionary: dict, key: str) -> bool:
    if (list(dictionary.keys()).count(key)):
        return True
    return False

if __name__ == '__main__':
    myapp = GUI()



