![GUI_Logo](orblogob2.png)

---

The KaLNB GUI is a software wrapper designed to be used as a quickstart for customers to communicted with Orbital Research LNB's. It is coded in Python 3.11

It is only tested and supported on **Windows 10**. (Though it will likely work on Windows 11 and there is an unofficial Windows 7 version available.)

Communication interfaces supported include: RS232, RS485, and Ethernet.

For the latest release of the executable, please visit `...\Engineering\projects\_Project_Doc\22.11_GUI2\Release_candidate`

---
## Distribution

Recommend distribution of **KaLNB GUI.exe** 

Do NOT distribute .cfg files that are unlabelled or designated IN_HOUSE ONLY

---

## Advanced Features

The advanced features adds additional functionality to the GUI that is restricted to in-house use or emergency troubleshooting only. It includes a command line interface, command macros, and TLV dumps for troubleshooting ethernet connectivity.

The access the advanced features:

1. Select LNB from dropdown menu
2. Help->About...
3. Press the build number 5x
4. Enter password: 6413c550b2

---

## Building the .py to .exe

1. Open cmd
2. Use 'cd' command to change directory to this repository, and into the folder specific to the version to build
3. Updated the version number in version.rc
4. Copy Orbital_KaLNB_GUI.spec to the build directory, update the file name to the newest version
5. Use command line: python PyInstaller -m Orbital_KaLNB_GUI.spec

---

## Create .cfg file

WARNING: .json configuration files may not be distributed to customers!

1. Update GUI_MODEL_REF_R02_XX_XX.csv to the desired configuration as outlined by the headings
2. Uncomment line 191 in GUI_Profile_Modifier_R2.py and run to dump all encrypted .cfg files to the local directory. Alternatively, uncomment line 190 in GUI_Profile_Modifier_R2.py and run to dump all .json versions of the configuration files into the local direcotry. 

---

## Credits

Main developer: Benjamin Stadnik

Contributors: Doug Forsythe, Tian Hao Xu