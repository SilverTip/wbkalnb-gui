# KaLNB GUI
# Written by Benjamin Stadnik
# Orbital Research Ltd.

# Version Log
# ---------------------------------------
# R1 - 2022-01-31 -     Version 1. "As Benjamin see's it"
# R2 - 2022-02-02 -     Changing GUI to class structure to remove global variables
# R3 - 2022-03-01 -     Added colour. Minor reformatting of variables
# R4 - 2022-03-04 -     Properly formatting variables depending on unit type
# R5 - 2022-03-07 -     Beta Release. Minor bug fixes
# R6 - 2022-03-14 -     DKa and TriKa temporary solution - disabled IP, TriKa specific commands.
#                       Updated communication module to LNB_Communications_R1.py that includes TriKa command
#                       encoding/decoding
# R7 - 2022-04-08 -     Generate GUI based on model number.
#                       New communication module LNB_Communication R2 with built-in encoding/decoding and
#                       filtering of data.
# R8 - 2022-04-11 -     New polling algorithm increase speed of GUI
# R9 - 2022-05-24 -     Advanced features overhaul - scrollable rx commands and saved and ordered tx commands.
#                       Update to LNB_Communication_R6 with fault decoding.
#                       Offset TriKa band settings
# R10 - 2022-07-27 -    Minor improvements. Improved reply filtering. LNB_Comm_R8
# R11 - 2022-09-02 -    Changed title and password according to CDR. RTM
# R12 - 2022-09 - 09 -  Display band LO on band buttons. Add model name to master and updated model numbers
# R1.4 - 2022-09-13 -   Skip R13. Changed revision number for initial release.
#                       Solved memory leak.
#                       Add version number to GUI interface
# 2.0 - 2022-10-14 -    Unit status from entry boxes to text window, all grid() to pack()
#                       for resizing windows, menu-bar with reset, exit, set thresholds, set ip, orbital information,
#                       build number, advanced features. Pack manager. Reset unit, mute DSA, and Clear SS buttons.
#                       Load model features from .csv
# 2.1 - 2022-11-28 -    FS/SS to Fault_Status/System_Status.
#                       LNB_COMMAND_REF_R2.csv, tidied up settings window, advanced window.
#                       Changed advanced window from ListBox to TextBox to enabled/disable state and ease of copy/paste
# 2.2 - 2023-01-25 -    WBKaLNB database update. RS232 fix. Current Removal
# 2.3 - 2023-01-31 -    Band display overhaul
# 2.4 - 2023-02-23 -    Move base64 icon to separate module. Move database to Database module
#                       Reformatted communication menu in preparation for discovery protocol.
#                       Added LLDP support to settings
#                       Added support for commands $getmm, $setan, $sethost
# 2.5 - 2023-04-10 -    Reformat main function
#                       Add VLNB as stand-alone module
#                       Add Search class from LNB_Communication_R11.py           
# 2.5.1 - 2023-04-17 -  Fixed bugs. Add TVL dump and rxtext save to Advanced Window
#                       Include /r/n in all communications to preserve structure of non-$ commands
#                       Updated to LNB_Communication_R12.py
# 2.5.2 - 2023-05-20 -  Added LNB profile "LNBKAWE-XWS60K-GD"
#                       Fixed model selection bug - reject unknown profile names
#                       Reject unknown units after search
# 2.6.0 - 2023-06-26 -  Update to LNB_Command_Manger_R2.py hardcode command syntax and fault syntax
#                       Update to LNB_Communication_R12d.py
#                       Add GUI_Profile_Manager_R1.py to load/encrypt/manage model profiles
#                       Minor bug fixes to VLNB_R1.py
#                       Added button to load profiles from Model Selection frame
#                       Added function to search current directory for model profiles upon launch
#                       Better exception handling - Errors now bold/red
#                       New Orbital icon - circular
#                       Add Orbital Logo to B64 encoding to avoid compiling issues
#                       Bug Fixes:
#                           Errors now clear even if no model selected (connection error, timeout, etc)
#                           Close communication during file dialog to prevent socket timeout
# 2.6.2 - 2023-07-11 -  Bug fix path. GUI_Profile_Manager_R2.py
#                       Obscure items
#                       Fix command typos in LNB_Command_Manager_R2.py
# 2.6.3 - 2023-11-02 -  Network search returns list IP/MAC. Also returns S/N if available
# 2.6.4 - 2024-04-05 -  Fixed gateway/subnet switching error: LNBGUI-2

import socket
import sys
import time
import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox
from tkinter import ttk
import tkinter.font
import datetime
import webbrowser
import json
import serial.tools.list_ports
import threading
import os
import re
import csv

import Icon64_R2 as Icon64
import LNB_Communication_R12d as Communication
import LNB_Command_Manager_R2 as CommandManager
import VLNB_R1 as VLNB
import GUI_Profile_Modifier_R2 as Profile

VERSION = "2.6.3"

# Size parameters
WIDTH = 475
HEIGHT = 850
BUTTONWIDTH = 8
ENTRYWIDTH = 14

# Colours
BG1 = '#042B60'  # Orbital dark blue
BG2 = '#36A9E1'  # Orbital light blue
FG1 = 'white'

DEFAULT_MODELS = [
    'Orbital STANDARD - RS485.cfg',
    'Orbital WIDEBAND - Ethernet.cfg',
    'Orbital WIDEBAND - RS485.cfg'
]

DEFAULT_MODEL = None #'LNB858-900XWS60'  # "LNBKAMM-XIWS60A"

SOCKET_PORT = 32019
FOUND_DEVICES = []  # Global variable used to transfer items between threads
THREAD_COMPLETE = False

class GUI():
    def __init__(self, master):
        # super().__init__()
        self.master = master
        self.master.geometry(str(WIDTH) + 'x' + str(HEIGHT))
        self.master.resizable(height=1, width=1)
        self.master.minsize(WIDTH, HEIGHT)
        self.master.title("Orbital KaLNB GUI")

        #   Window Icon
        self.master.wm_iconphoto(True, tk.PhotoImage(data=Icon64.decoded_icon))

        self.com = None
        self.VLNB = None
        self.PERIOD = 5  # seconds
        self.font = tkinter.font.Font(family="Helvetica", size=10)
        self.fontSpacer = tkinter.font.Font(family="Helvetica", size=2)
        self.fontLarge = tkinter.font.Font(family="Helvetica", size=11, weight='normal')
        self.fontLargeBold = tkinter.font.Font(family="Helvetica", size=11, weight='bold')
        self.master.configure(bg=BG1)
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        self.clock_text = tk.StringVar()
        self.clock_text.set(updatetime())
        self.ip_change_text = tk.StringVar()
        self.subnet_change_text = tk.StringVar()
        self.gateway_change_text = tk.StringVar()
        self.DHCP_change_text = tk.StringVar()
        self.quit = False
        self.announce_capture_flag = False
        self.serial_devices = []
        self.ip_devices = []
        self.search = None

        self.buffer = {
            'Error': ''
        }

        self.hidden_buffer = {
            'Band': 0,
            'DSA_hex': None,
            'DSA_f': None,
            'Mute': False
        }

        self.settings_buffer = {
            'YYYYMMDD': '',
            'HHMMSS': '',
            'UTCOFFSET': '',
            'UNIXTS': '',
            'DHCP_Retries': '',
            'Static_IP': '',
            'Static_Subnet': '',
            'Static_Gateway': '',
            'MAC': '',
            'Hostname': '',
            'LLDP_Enable': '',
            'LLDP_Timer': '',
            'Temperature_High': '',
            'Temperature_Low': '',
            'Voltage_High': '',
            'Voltage_Low': '',
            'Current_High': '',
            'Current_Low': '',
            'Stack_L': '',
            'CPU_H': ''
        }

        self.settings_text = {}
        for setting in self.settings_buffer.keys():
            self.settings_text[setting] = tk.StringVar()

        i = 0
        temp = {}
        for key, value in self.settings_buffer.items():
            if (i > 3):
                break
            temp.update({key: value})
            i += 1
        self.settings_buffer.clear()
        self.settings_buffer = temp.copy()
        self.settings_buffer_reset = temp.copy()

        self.hidden_buffer_reset = self.hidden_buffer.copy()

        # Pack Tracking
        #self.model_settings_list = Load_All_Settings(get_path(SETTINGS))
        self.model_settings_list = []
        self.model_settings_list += load_cfgs() #Class Model

        self.frame_list = []
        self.window_list = []
        self.button_list = []

        self.model_selected = False
        self.band = {'index': [], 'name': [], 'button_display_number': [], 'button': []}
        self.first_loop = True
        self.DSA_update = True
        self.variable = True
        self.model = None
        self.advancedcheck = 0
        self.settings_check = False
        self.search_abandon = False

        self.Menu()
        self.PackManager()

    def Menu(self):
        #   Menu
        self.menubar = tk.Menu(self.master, font=self.font)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        # self.filemenu.add_command(label="Save Status as...", command=self.SaveStatusText)
        self.filemenu.add_command(label="Search Devices", command=self.Search)
        self.filemenu.add_command(label="Factory Reset", command=self.FactoryReset)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Select New Model", command=self.ResetGUI)
        self.filemenu.add_command(label="Exit", command=self.Quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.editmenu = tk.Menu(self.menubar, tearoff=0)
        # self.editmenu.add_command(label="Unit Time", command=self.SetUnitTime)
        # self.editmenu.add_command(label="IP Address", command=self.SetUnitIP)
        self.editmenu.add_command(label="Unit Settings", command=self.UnitSettings)
        self.menubar.add_cascade(label="Settings", menu=self.editmenu)

        self.helpmenu = tk.Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Find Help Online",
                                  command=lambda url='https://orbitalresearch.net/': webbrowser.open(url))
        self.helpmenu.add_command(label="Explore Products",
                                  command=lambda url='https://orbitalresearch.net/products/': webbrowser.open(url))
        self.helpmenu.add_command(label="Contact Us",
                                  command=lambda url="https://orbitalresearch.net/contact-us/": webbrowser.open(url))
        self.helpmenu.add_command(label="About GUI...", command=self.Help)
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)

        self.master.config(menu=self.menubar)

    def Header(self):
        # DiplayFrame
        self.DisplayFrame = tk.Label(self.master, font=self.font, bg=BG1, fg=FG1)
        self.logo = tk.PhotoImage(data=Icon64.decoded_logo)
        self.logoFrame = tk.Label(self.DisplayFrame, image=self.logo, bg=BG1)
        self.logoFrame.pack(side='left', anchor='nw')
        tk.Label(self.DisplayFrame, font=self.font, textvariable=self.clock_text, bg=BG1, fg=FG1).pack(side='top',
                                                                                                       anchor='ne')
        self.statusLightBackground = tk.Canvas(self.DisplayFrame, bg=BG1, width=30, height=30, highlightbackground=BG1)
        self.statusLightBackground.pack(side='right', anchor='e')
        self.statusLight = self.statusLightBackground.create_oval(3, 3, 30, 30, fill='red')
        tk.Label(self.DisplayFrame, font=self.font, text='Connection Status:', bg=BG1, fg=FG1).pack(side='right',
                                                                                                    anchor='e')
        self.DisplayFrame.pack(side='top', anchor='e', fill='both')
        self.frame_list.append(self.DisplayFrame)

    def PackManager(self):
        self.ClearGUI()
        self.Header()
        if DEFAULT_MODEL:
            for model in self.model_settings_list:
                if model.name == DEFAULT_MODEL:
                    self.model = model.settings
            if self.model:
                self.PackGUI()
                return
        elif self.model_selected:
            for model in self.model_settings_list:
                if model.name == self.ModelCombobox.get():
                    self.model = model.settings
            if self.model:
                self.PackGUI()
                return

        self.PackModelFrame()

    def ClearGUI(self):
        if self.frame_list:
            for frame in self.frame_list:
                frame.pack_forget()
            self.frame_list.clear()
        if self.window_list:
            for name, window in self.window_list:
                window.destroy()
            self.window_list.clear()

    def ModelSelection(self):
        if self.ModelCombobox.get() != '':
            self.model_selected = True
        self.PackManager()

    def ResetGUI(self):
        if not self.ModelSelected():
            return
        self.Disconnect()
        # self.Close_Communication()
        self.clearbuffer()
        self.settings_buffer = self.settings_buffer_reset.copy()
        self.hidden_buffer = self.hidden_buffer_reset.copy()
        self.master.geometry(F"{WIDTH}x{HEIGHT}")
        self.model_selected = False
        self.model = None
        self.search = None
        self.serial_devices.clear()
        self.ip_devices.clear()
        self.band.clear()
        self.PackManager()

    def PackModelFrame(self):
        model_options = []
        for model in self.model_settings_list:
            model_options.append(model.name)

        modelFrame = tk.Label(self.master, font=self.font, bg=BG1, fg=FG1)
        subFrame1 = tk.LabelFrame(modelFrame, font=self.font, bg=BG1, fg=FG1)
        tk.Label(subFrame1, text="Select Model:", font=self.font, bg=BG1, fg=FG1).pack(side=tk.LEFT)
        self.ModelCombobox = ttk.Combobox(subFrame1, values=model_options, width=40)
        self.ModelCombobox.pack(side=tk.LEFT, expand=True, fill=tk.X)
        tk.Button(subFrame1,
                  text='Enter',
                  font=self.font,
                  bg=BG2,
                  fg=FG1,
                  activebackground=BG1,
                  activeforeground=FG1,
                  disabledforeground=FG1,
                  command=self.ModelSelection,
                  width=BUTTONWIDTH,
                  state='normal'
                  ).pack(side=tk.RIGHT, padx=3)
        subFrame2 = tk.Label(modelFrame, font=self.font, bg=BG1, fg=FG1)
        tk.Button(subFrame2,
                  text='Load Model',
                  font=self.font,
                  bg=BG2,
                  fg=FG1,
                  activebackground=BG1,
                  activeforeground=FG1,
                  disabledforeground=FG1,
                  command=self.LoadModel,
                  width=15,
                  state='normal'
                  ).pack()
        subFrame1.pack(side=tk.TOP, ipadx=5, ipady=5, padx=5, pady=5)
        subFrame2.pack(side=tk.BOTTOM, ipadx=5, ipady=5, padx=5, pady=5)
        modelFrame.pack()
        self.frame_list.append(modelFrame)

    def LoadModel(self):
        try:
            path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.cfg"), ("All files", "*.*"))).name
        except AttributeError:
            return

        profile = Profile.Model(path)

        if not profile.is_valid:
            tk.messagebox.showwarning("Error", "Error loading file")
            return

        if profile.name not in [model.name for model in self.model_settings_list]:
            self.model_settings_list.append(profile)
        self.ModelCombobox.set(profile.name)
        self.model_selected = True
        self.PackManager()

    def PackConnectionFrame(self):
        # Connection Frame
        connectionFrame = tk.LabelFrame(self.master, text="Communication Menu", font=self.font, bg=BG1, fg=FG1)
        IPFrame = tk.Label(connectionFrame, font=self.font, bg=BG1, fg=FG1)
        SerialFrame = tk.Label(connectionFrame, font=self.font, bg=BG1, fg=FG1)
        ButtonFrame = tk.Label(connectionFrame, font=self.font, bg=BG1, fg=FG1)
        self.IP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.Serial_EN.set(True)
        IPCheckbutton = tk.Checkbutton(IPFrame,
                                       font=self.font,
                                       text='',
                                       variable=self.IP_EN,
                                       bg=BG1,
                                       fg=FG1,
                                       activebackground=BG1,
                                       selectcolor=BG2,
                                       command=lambda method=1: self.UpdateCommunication(method)
                                       )
        serialCheckbutton = tk.Checkbutton(SerialFrame,
                                           font=self.font,
                                           text='',
                                           variable=self.Serial_EN,
                                           bg=BG1,
                                           fg=FG1,
                                           activebackground=BG1,
                                           selectcolor=BG2,
                                           command=lambda method=0: self.UpdateCommunication(method)
                                           )

        combo_width = 30
        self.SerialPort = ttk.Combobox(SerialFrame, values=self.serial_devices, state='normal', width=combo_width)
        self.IP = ttk.Combobox(IPFrame, values=self.ip_devices, state='normal', width=combo_width)

        #   Pack connection interface based on settings
        if self.model['IP'] and self.model['serial']:
            self.IP_EN.set(True)
            self.Serial_EN.set(False)
            self.SerialPort['state'] = 'disabled'
            serialCheckbutton.pack(side=tk.LEFT)
            IPCheckbutton.pack(side=tk.LEFT)
        elif self.model['IP']:
            self.IP_EN.set(True)
            self.Serial_EN.set(False)
        tk.Label(IPFrame, font=self.font, text='IP:', bg=BG1, fg=FG1, width=4, anchor=tk.E).pack(side=tk.LEFT)
        tk.Label(SerialFrame, font=self.font, text='COM:', bg=BG1, fg=FG1, width=4, anchor=tk.E).pack(side=tk.LEFT)
        self.SerialPort.pack(fill=tk.X)
        self.IP.pack(fill=tk.X)
        if self.model['IP']:
            IPFrame.pack(fill=tk.BOTH)
        if self.model['serial']:
            SerialFrame.pack(fill=tk.BOTH)
        self.connectButton = tk.Button(ButtonFrame,
                                       font=self.font,
                                       text='Connect',
                                       bg=BG2,
                                       fg=FG1,
                                       activebackground=BG2,
                                       activeforeground=FG1,
                                       command=self.Connect,
                                       width=BUTTONWIDTH)
        self.connectButton.pack(side=tk.RIGHT)
        ButtonFrame.pack(fill=tk.BOTH)
        connectionFrame.pack(fill=tk.X)
        self.frame_list.append(connectionFrame)

    def PackGUI(self):
        self.model_selected = True

        # Initialize VLNB with model parameters
        self.VLNB = None
        if self.model['name'] == 'DEMO':
            # This will be available for all units in the future
            # For now, tie VLNB to 'DEMO' unit
            self.VLNB = VLNB.VLNB(self.model)

        # Model
        self.ModelTitle = tk.Label(self.master, font=self.font, text=F"Model: {self.model['name']}", bg=BG1, fg=FG1)
        self.ModelTitle.pack()
        self.frame_list.append(self.ModelTitle)
        self.buffer = {}

        #   Generate/Overwrite command structure for unit
        #error_path = get_path("GUI_ERROR_REF_R02_00.csv")
        #command_path = get_path('GUI_COMMAND_REF_R02_05.csv')
        self.command = CommandManager.Command(int(self.model['filter']))

        self.PackConnectionFrame()

        if self.model['filter'] == 0:
            self.PERIOD = 5

        # Band Select Frame
        self.band['index'] = [x + int(self.model['band_DSA_offset']) for x in range(self.model['bands'])]
        self.band['button_display_number'] = [x + self.model['band_display_offset'] for x in self.band['index']]

        # Add band name if applicable
        self.band['name'] = [F"Band {self.band['button_display_number'][i]}" for i, x in
                             enumerate(self.band['button_display_number'])]
        isalpha_flag = False
        if (self.model['band_name']):
            names = self.model['band_name'].split(',')
            for index, name in enumerate(names):
                self.band['name'].pop(index)
                self.band['name'].insert(index, name)
                if (name.isalpha()):
                    isalpha_flag = True
            if not isalpha_flag:
                self.FreqTitle = tk.Label(self.master, font=self.font, text=F"LO Frequency (GHz)", bg=BG1, fg=FG1)
                self.FreqTitle.pack()
                self.frame_list.append(self.FreqTitle)

                # Generate and pack band buttons
        self.BandSelectFrame = tk.Label(self.master, bg=BG1)
        frame = tk.Label(self.BandSelectFrame, bg=BG1)
        buttons_per_section = 5
        self.band['button'] = []
        count = 1
        for index, number in enumerate(self.band['index']):
            bandbutton = tk.Button(frame, font=self.font, bg=BG1, fg=FG1,
                                   activebackground=BG2,
                                   activeforeground=FG1,
                                   disabledforeground=FG1,
                                   text=self.band['name'][index],
                                   command=lambda band=number: self.Bandbutton(str(band)),
                                   width=BUTTONWIDTH,
                                   bd=6,
                                   state='disabled')
            self.band['button'].append(bandbutton)
            self.button_list.append(bandbutton)
            bandbutton.pack(side=tk.LEFT, padx=5, pady=5)
            count += 1
            if (count > buttons_per_section):
                count = 1
                frame.pack()
                frame = tk.Label(self.BandSelectFrame, bg=BG1)
        if count != 1:
            frame.pack()
        if self.model['bands']:
            self.BandSelectFrame.pack()
            self.frame_list.append(self.BandSelectFrame)
            self.buffer['Band'] = ''

        # DSA Scale Frame
        if self.model['DSA']:
            self.DSA_range = (0.5, 31.5)
            ScaleFrame = tk.LabelFrame(self.master, text='DSA Attenuation (dB)', font=self.font, bg=BG1, fg=FG1)
            frame1 = tk.Label(ScaleFrame, font=self.font, bg=BG1, fg=FG1)
            frame2 = tk.Label(ScaleFrame, font=self.font, bg=BG1, fg=FG1)
            frame3 = tk.Label(ScaleFrame, font=self.font, bg=BG1, fg=FG1)
            downbutton = tk.Button(frame1,
                                   font=self.font,
                                   bg=BG1,
                                   fg=FG1,
                                   activebackground=BG2,
                                   activeforeground=FG1,
                                   disabledforeground=FG1,
                                   text="-",
                                   command=lambda: self.DSA_Change(False),
                                   width=2,
                                   height=1,
                                   bd=6,
                                   state='disabled')
            upbutton = tk.Button(frame3,
                                 font=self.font,
                                 bg=BG1,
                                 fg=FG1,
                                 activebackground=BG2,
                                 activeforeground=FG1,
                                 disabledforeground=FG1,
                                 text="+",
                                 command=lambda: self.DSA_Change(True),
                                 width=2,
                                 height=1,
                                 bd=6,
                                 state='disabled')
            self.DSA_resolution = 0.5
            digits = 3
            if self.model['DSA'] == 2:
                self.DSA_resolution = 0.25
                digits = 4
            self.DSAScale = tk.Scale(frame2,
                                     from_=self.DSA_range[0],
                                     to=self.DSA_range[1],
                                     digits=digits,
                                     resolution=self.DSA_resolution,
                                     orient='horizontal',
                                     state='disabled',
                                     troughcolor=FG1,
                                     font=self.font,
                                     bg=BG1,
                                     fg=FG1,
                                     activebackground=BG2,
                                     highlightbackground=BG1,
                                     bd='1')

            self.DSAScale.bind("<ButtonRelease-1>", self.updateDSAfromSlider)
            # ScaleFrame.grid_propagate(0)
            downbutton.pack(side=tk.BOTTOM)
            self.DSAScale.pack(fill='both', padx=5, pady=5)
            upbutton.pack(side=tk.BOTTOM)
            frame1.pack(fill=tk.Y, side=tk.LEFT)
            frame3.pack(fill=tk.Y, side=tk.RIGHT)
            frame2.pack(fill='both')
            ScaleFrame.pack(fill='both', ipadx=5, padx=5, pady=5, expand=False)
            self.buffer['DSA'] = ''
            self.frame_list.append(ScaleFrame)
            self.button_list.append(upbutton)
            self.button_list.append(downbutton)

        # Refresh Frame
        # self.RefreshFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        # tk.Label(self.RefreshFrame,text='   Refresh:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,columnspan=1, sticky='SE')
        # tk.Label(self.RefreshFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=3, sticky='SW')
        # self.PeriodScale = tk.Scale(self.RefreshFrame,from_='1',to='30', length=300, orient='horizontal',state='normal',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1)
        # self.PeriodScale.grid(row=1,column=2,columnspan=1,sticky='W')
        # self.PeriodScale.set(self.PERIOD)
        # self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        # self.RefreshFrame.pack()

        # Additional Features
        additionalFrame = tk.Label(self.master, text='', font=self.font, bg=BG1, fg=FG1)
        self.ResetButton = tk.Button(additionalFrame,
                                     font=self.font,
                                     bg=BG1,
                                     fg=FG1,
                                     activebackground=BG2,
                                     activeforeground=FG1,
                                     disabledforeground=FG1,
                                     text='Reset Unit',
                                     command=self.Reset,
                                     width=BUTTONWIDTH,
                                     bd=6,
                                     state='disabled')
        self.ClearAllButton = tk.Button(additionalFrame,
                                        font=self.font,
                                        bg=BG1,
                                        fg=FG1,
                                        activebackground=BG2,
                                        activeforeground=FG1,
                                        disabledforeground=FG1,
                                        text='Clear System_Status',
                                        command=lambda com=self.command.clral(): self.SendMessage(com),
                                        bd=6,
                                        state='disabled')
        self.MuteDSAButton = tk.Button(additionalFrame,
                                       font=self.font,
                                       bg=BG1,
                                       fg=FG1,
                                       activebackground=BG2,
                                       activeforeground=FG1,
                                       disabledforeground=FG1,
                                       text='Mute Unit',
                                       command=self.MuteDSA,
                                       width=BUTTONWIDTH,
                                       bd=6,
                                       state='disabled')
        self.button_list += [self.ResetButton, self.ClearAllButton, self.MuteDSAButton]
        self.ResetButton.pack(side=tk.LEFT, padx=5, pady=2)
        if self.model['filter'] > 0:
            self.ClearAllButton.pack(side=tk.LEFT, padx=5, pady=2)
        if self.model['Mute']:
            self.MuteDSAButton.pack(side=tk.LEFT, padx=5, pady=2)
        additionalFrame.pack()
        self.frame_list.append(additionalFrame)

        # Status Frame
        if self.model['Voltage']:
            self.buffer['Voltage'] = ''
        if self.model['Current']:
            self.buffer['Current'] = ''
        if self.model['Temperature']:
            self.buffer['Temperature'] = ''
        if self.model['Uptime']:
            self.buffer['Uptime'] = ''
        self.buffer['Serial_Number'] = ''
        self.buffer['Firmware_Ver'] = ''
        if self.model['Info']:
            self.buffer['Hardware_Ver'] = ''
            self.buffer['Mfg'] = ''
            self.buffer['Model'] = ''
            self.buffer['Hostname'] = ''
        if self.model['Log']:
            self.buffer['Stack_WM'] = ''
            self.buffer['CPU'] = ''
            self.buffer['CPU_WM'] = ''
        if self.model['filter'] > 0:
            self.buffer['Fault_Status'] = ''
            self.buffer['System_Status'] = ''
        else:
            self.buffer['Fault'] = ''
        self.buffer['Error'] = ''
        self.StatusFrame = tk.LabelFrame(self.master, text='Unit Status', font=self.font, bg=BG1, fg=FG1)
        self.StatusWindow = tk.Text(self.StatusFrame,
                                    font=self.fontLarge,
                                    state='normal',
                                    height=len(self.buffer) + 1,
                                    undo=False,
                                    wrap=tk.WORD,
                                    spacing1=5)
        # self.StatusWindow.set(yscrollcommand=False)
        self.StatusWindow.pack(expand=tk.YES, fill=tk.BOTH, ipadx=5, padx=5, ipady=5, pady=5)
        self.StatusFrame.pack(expand=tk.YES, fill=tk.BOTH, ipadx=5, padx=5, ipady=5, pady=5)
        self.frame_list.append(self.StatusFrame)
        self.UpdateStatusWindow()

    def Bandbutton(self, number):
        self.command.setst(number)
        self.SendMessage(self.command.setst(number))
        self.DSA_update = True
        self.Poll()

    def Search(self):
        if not self.ModelSelected():
            return
        if self.com:
            message = 'Disconnect from unit to search other network devices?'
            answer = tkinter.messagebox.askyesno(title='Search', message=message)
            if not answer:
                return
            self.Disconnect()
        if not self.search:
            self.search = Communication.Search()
        serial_check = 'no'
        ip_check = 'no'
        if self.model['serial']:
            serial_check = tk.messagebox.askquestion(title='Search',
                                                     message=f"Search for {self.model['serial']} devices?")
        if serial_check == 'yes':
            self.search.Serial(self.model['serial'])
            self.serial_devices = self.ConvertFoundDevices(self.search.serial_devices)
            self.SerialPort['values'] = self.serial_devices
            if len(self.serial_devices) > 0:
                myapp.SerialPort.set('NEW DEVICES ------------>')
            message = f"{self.model['serial']} search complete.\n{len(self.search.serial_devices)} devices found."
            tk.messagebox.showinfo(title='Search', message=message)

        if self.model['IP']:
            ip_check = tk.messagebox.askquestion(title='Search', message="Search for Network devices?")
        if ip_check == 'yes':
            if self.model['discovery']:
                self.search.Network(solicitation=True)  # Discovery Feature (~20ms)
                self.ip_devices = self.ConvertFoundDevices(self.search.network_devices)
                self.IP['values'] = self.ip_devices
                if len(self.ip_devices) > 0:
                    myapp.IP.set('NEW DEVICES ------------>')
                message = f"Network search complete.\n{len(self.search.network_devices)} devices found."
                tk.messagebox.showinfo(title='Search', message=message)
            else:
                self.announce_capture_flag = True

    @staticmethod
    def ConvertFoundDevices(devices: list[dict]) -> list[str]:
        new_list = []
        for device in devices:
            if type(device) != dict:
                continue
            #if device.get('S/N', None) or device.get('ID', None):  #only valid units have ID or S/N, i.e. reject invalid # Removed 2.6.3
            s = ''
            for key, value in device.items():
                s += f'{key}:{value}, '
            s = s[:-2]

            new_list.append(s)

        return new_list

    def UnitSettings(self):
        if not self.ModelSelected():
            return
        if not self.ModelSupport('filter'):
            return
        if WindowExists(self.window_list, 'Settings'):
            return

        state = 'disabled'
        bg = BG1
        if self.com:
            state = 'normal'
            bg = BG2

        width = 15

        SettingsFrame = self.Popup('Unit Settings', 300, 600)

        # Menu
        menubar = tk.Menu(SettingsFrame, font=self.font)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Load Settings", command=self.LoadSettings)
        filemenu.add_command(label="Save Settings as...", command=self.SaveSettings)
        menubar.add_cascade(label="File", menu=filemenu)
        SettingsFrame.config(menu=menubar, pady=4)

        # Settings
        if self.model['IP']:
            IP = {'DHCP_Retries': '',
                  'Static_IP': '',
                  'Static_Subnet': '',
                  'Static_Gateway': '',
                  'MAC': ''}
            self.settings_buffer.update(IP)
        if self.model['Info']:
            self.settings_buffer.update({'Hostname': ''})
        if self.model['LLDP']:
            self.settings_buffer.update({'LLDP_Enable': '', 'LLDP_Timer': ''})
        if self.model['Thresholds']:
            thresholds = {
                'Temperature_High': '',
                'Temperature_Low': '',
                'Voltage_High': '',
                'Voltage_Low': '',
                'Current_High': '',
                'Current_Low': ''}
            self.settings_buffer.update(thresholds)
        if self.model['Log']:
            self.settings_buffer.update({'Stack_L': '', 'CPU_H': ''})

        largest_key = ''
        for key in self.settings_buffer.keys():
            if (len(key) > len(largest_key)):
                largest_key = key

        for key in self.settings_buffer.keys():
            frame = tk.Label(SettingsFrame, font=self.font, bg=BG1, fg=FG1)
            tk.Entry(frame, textvariable=self.settings_text[key], width=width + 3).pack(side=tk.RIGHT)
            tk.Label(frame, text=F"{key}", font=self.font, bg=BG1, fg=FG1, width=len(largest_key) - 2,
                     anchor=tk.E).pack(side=tk.LEFT)
            tk.Label(frame, text=F":", font=self.font, bg=BG1, fg=FG1).pack(side=tk.LEFT)
            frame.pack()

        # timebuttonframe = tk.Label(SettingsFrame,font=self.font,bg=BG1,fg=FG1)
        # self.AutoSetTimeButton = tk.Button(timebuttonframe,text='Auto Set Time',font=self.font,bg=bg,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.AutoSetTime,width=buttonwidth,state=state)
        # self.AutoSetTimeButton.pack(padx=5,pady=2)
        # timebuttonframe.pack(fill=tk.X)
        # self.button_list.append(self.AutoSetTimeButton)

        buttonframe = tk.Label(SettingsFrame, font=self.font, bg=BG1, fg=FG1)
        self.GetSettingsButton = tk.Button(buttonframe, text='Get', font=self.font, bg=bg, fg=FG1, activebackground=BG2,
                                           activeforeground=FG1, disabledforeground=FG1, command=self.GetSettings,
                                           width=BUTTONWIDTH, state=state)
        self.SetSettingsButton = tk.Button(buttonframe, text='Set', font=self.font, bg=bg, fg=FG1,
                                           activebackground=BG2, activeforeground=FG1, disabledforeground=FG1,
                                           command=self.SetSettings, width=BUTTONWIDTH, state=state)
        self.ClearSettingsButton = tk.Button(SettingsFrame, text='Clear', font=self.font, bg=bg, fg=FG1,
                                             activebackground=BG2, activeforeground=FG1, disabledforeground=FG1,
                                             command=self.ClearSettings, width=BUTTONWIDTH, state=state)
        self.GetSettingsButton.pack(side=tk.LEFT, padx=5, pady=2)
        self.SetSettingsButton.pack(side=tk.LEFT, padx=5, pady=2)
        self.button_list.append(self.GetSettingsButton)
        self.button_list.append(self.SetSettingsButton)
        self.button_list.append(self.ClearSettingsButton)
        buttonframe.pack(padx=5, pady=2)
        self.ClearSettingsButton.pack(padx=5, pady=2)

        self.window_list.append(('Settings', SettingsFrame))
        SettingsFrame.take_focus = True

    def AutoSetTime(self):
        self.donothing()

    def ClearSettings(self):
        for key in self.settings_buffer.keys():
            self.settings_text[key].set('')

    def GetSettings(self):
        self.settings_check = True
        settings_messages = [self.command.gettime()]
        if self.model['IP']:
            settings_messages += [self.command.getip(), self.command.getStaticIP()]
        if self.model['Thresholds']:
            settings_messages += [self.command.getth()]
        if self.model['Info']:
            settings_messages += [self.command.gethost()]
        if self.model['LLDP']:
            settings_messages += [self.command.getan()]

        for message in settings_messages:
            self.SendSettings(message)

        for key in self.settings_buffer.keys():
            self.settings_text[key].set(self.settings_buffer[key])

        # self.buffer['Error'] = self.settings_buffer

    def SetSettings(self):
        self.settings_check = True
        for key in self.settings_buffer.keys():
            self.settings_buffer[key] = self.settings_text[key].get().strip()  # Remove leading and final whitespaces

        # Set Time
        self.SendSettings(self.command.setwrrtc(self.settings_buffer['YYYYMMDD'],
                                                self.settings_buffer['HHMMSS'],
                                                self.settings_buffer['UTCOFFSET']))
        #   Both wrrtc and  wrrutc send same $TIME reply so update UNIXTS directly from entry box variable
        self.SendSettings(self.command.setwrutc(self.settings_text['UNIXTS'].get().strip()))

        # Set Thresholds
        if self.model['Thresholds']:
            if self.model['Log']:
                self.SendSettings(self.command.setth(self.settings_buffer['Current_High'],
                                                     self.settings_buffer['Current_Low'],
                                                     self.settings_buffer['Temperature_High'],
                                                     self.settings_buffer['Temperature_Low'],
                                                     self.settings_buffer['Voltage_High'],
                                                     self.settings_buffer['Voltage_Low'],
                                                     self.settings_buffer['Stack_L'],
                                                     self.settings_buffer['CPU_H']))
            else:
                self.SendSettings(self.command.setth(self.settings_buffer['Current_High'],
                                                     self.settings_buffer['Current_Low'],
                                                     self.settings_buffer['Temperature_High'],
                                                     self.settings_buffer['Temperature_Low'],
                                                     self.settings_buffer['Voltage_High'],
                                                     self.settings_buffer['Voltage_Low']))
        # Set Network
        if self.model['IP']:
            self.SendSettings(self.command.setStaticIP(self.settings_buffer['DHCP_Retries'],
                                                       self.settings_buffer['Static_IP'],
                                                       self.settings_buffer['Static_Gateway'],
                                                       self.settings_buffer['Static_Subnet']))

        if self.model['Info']:
            self.SendSettings(self.command.sethost(self.settings_buffer['Hostname']))
        if self.model['LLDP']:
            self.SendSettings(self.command.setan(self.settings_buffer['LLDP_Enable'],
                                                 self.settings_buffer['LLDP_Timer']))

        self.first_loop = True
        self.Poll()

    def LoadSettings(self):
        if self.com:
            self.com.close()
        try:
            path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.json"), ("All files", "*.*"))).name

            with open(path, 'r') as f:
                temp_buffer = json.load(f)

            self.settings_buffer.update(temp_buffer)
            for key in self.settings_buffer.keys():
                self.settings_text[key].set(self.settings_buffer[key])

        except AttributeError:
            # No selection
            pass

        if self.com:
            self.com.open()

    def SaveSettings(self):
        if self.com:
            self.com.close()
        try:
            path = tk.filedialog.asksaveasfile(initialfile='Untitled.json',
                                               filetypes=(("Text files", "*.json"), ("All files", "*.*"))).name

            temp = {}
            for key in self.settings_buffer.keys():
                temp[key] = self.settings_text[key].get()

            with open(path, 'w') as f:
                json.dump(temp, f)

        except AttributeError:
            # No selection
            pass

        if self.com:
            self.com.open()

    def MuteDSA(self):
        self.hidden_buffer['Mute'] = not self.hidden_buffer['Mute']
        self.SendMessage(self.command.muteDSA(str(int(self.hidden_buffer['Mute']))))
        self.DSA_update = True
        self.Poll()

    def AdvancedFeaturesWall(self):
        if not (self.advancedcheck > 3):
            self.advancedcheck += 1
            return
        self.advancedcheck = 0

        if WindowExists(self.window_list, 'Advanced'):
            return

        self.AdvancedWindow = self.Popup("Advanced Features", 700, 600)
        self.window_list.append(('Advanced', self.AdvancedWindow))

        self.PasswordFrame = tk.Label(self.AdvancedWindow, font=self.font, bg=BG1, fg=FG1)
        self.pass_text = tk.StringVar()
        tk.Label(self.PasswordFrame, text='Enter Password:', font=self.font, bg=BG1, fg=FG1).grid(row=1, column=1,
                                                                                                  sticky='E')

        tk.Entry(self.PasswordFrame, textvariable=self.pass_text, state='normal').grid(row=1, column=2)
        self.PasswordButton = tk.Button(self.PasswordFrame, font=self.font, bg=BG2, fg=FG1, activebackground=BG2,
                                        activeforeground=FG1, disabledforeground=FG1, text='Enter',
                                        command=self.passcheck, width=BUTTONWIDTH)
        self.PasswordButton.grid(row=1, column=3, padx=5)
        self.PasswordFrame.pack(ipadx=5, ipady=5, padx=5, pady=5)

    def passcheck(self):
        if self.pass_text.get() != item():  # This is not super secure... Can be targeted with a timing attack
            return
        self.PasswordFrame.pack_forget()
        self.AdvancedFeatures()

    def AdvancedFeatures(self):
        state = 'disabled'
        bg = BG1
        if self.com:
            state = 'normal'
            bg = BG2

        # Macro Frame
        CommandProfileFrame = tk.LabelFrame(self.AdvancedWindow, text="Custom Command Macro", font=self.font, bg=BG1,
                                            fg=FG1)  # Select table from .csv
        path_text = tk.StringVar()
        tk.Button(CommandProfileFrame, text='Load', command=lambda: self.LoadCommandProfile(path_text),
                  width=BUTTONWIDTH, bd=2, font=self.font, bg=BG2, fg=FG1, activebackground=BG2, activeforeground=FG1,
                  disabledforeground=FG1, state='normal').pack(side=tk.LEFT, padx=2)
        tk.Entry(CommandProfileFrame, textvariable=path_text, width=40, font=self.font).pack(fill=tk.X, expand=True,
                                                                                             side=tk.LEFT)
        run_profile_button = tk.Button(CommandProfileFrame, text='Run',
                                       command=lambda: self.RunCommandProfile(path_text.get()), width=BUTTONWIDTH, bd=2,
                                       font=self.font, bg=bg, fg=FG1, activebackground=BG2, activeforeground=FG1,
                                       disabledforeground=FG1, state=state)
        self.button_list.append(run_profile_button)
        run_profile_button.pack(side=tk.RIGHT, padx=4)
        CommandProfileFrame.pack(fill=tk.X, side=tk.BOTTOM)

        # Command Frame
        self.txlist = ['$build,*']
        CommandFrame = tk.LabelFrame(self.AdvancedWindow, text="Command Window", font=self.font, bg=BG1, fg=FG1)

        # Send items
        TxFrame = tk.Label(CommandFrame, font=self.font, bg=BG1, fg=FG1)
        frame_left = tk.Label(TxFrame, font=self.font, bg=BG1, fg=FG1)
        frame_middle = tk.Label(TxFrame, font=self.font, bg=BG1, fg=FG1)
        frame_right = tk.Label(TxFrame, font=self.font, bg=BG1, fg=FG1)
        tk.Label(frame_left, text="TX:", font=self.font, bg=BG1, fg=FG1).pack()
        self.Command = ttk.Combobox(frame_middle, value=self.txlist)  ##Makes the menu
        self.Command.pack(expand=tk.YES, fill=tk.BOTH)
        SendButton = tk.Button(frame_right, text='Send', font=self.font, bg=bg, fg=FG1, activebackground=BG2,
                               activeforeground=FG1, disabledforeground=FG1, command=self.CommandLineSend,
                               width=BUTTONWIDTH, state=state)
        self.button_list.append(SendButton)
        SendButton.pack()
        frame_left.pack(side=tk.LEFT, fill=tk.Y)
        frame_right.pack(side=tk.RIGHT, fill=tk.Y)
        frame_middle.pack(fill='both')
        TxFrame.pack(fill=tk.X)

        # Receive items
        RxFrame = tk.Label(CommandFrame, font=self.font, bg=BG1, fg=FG1)
        frame_left = tk.Label(RxFrame, font=self.font, bg=BG1, fg=FG1)
        frame_middle = tk.Label(RxFrame, font=self.font, bg=BG1, fg=FG1)
        frame_right = tk.Label(RxFrame, font=self.font, bg=BG1, fg=FG1)
        tk.Label(frame_left, text="RX:", font=self.font, bg=BG1, fg=FG1).pack(side=tk.TOP)
        self.rxText = tk.Text(frame_middle, font=self.font, state='normal', undo=False, wrap=tk.WORD)
        self.rxText.pack(side=tk.LEFT, fill='both', expand=True)
        ClearButton = tk.Button(frame_right, text='Clear Rx', font=self.font, bg=BG2, fg=FG1, activebackground=BG2,
                                activeforeground=FG1, command=self.ClearRx, width=BUTTONWIDTH, state='normal')
        SaveButton = tk.Button(frame_right, text='Save Rx', font=self.font, bg=BG2, fg=FG1, activebackground=BG2,
                               activeforeground=FG1, command=self.SaveRx, width=BUTTONWIDTH, state='normal')
        ClearButton.pack(side=tk.TOP)
        SaveButton.pack(side=tk.TOP)
        frame_left.pack(side=tk.LEFT, fill=tk.Y)
        frame_right.pack(side=tk.RIGHT, fill=tk.Y)
        frame_middle.pack(fill='both')
        RxFrame.pack(fill='both', expand=True)
        CommandFrame.pack(fill='both')

        TLVButton = tk.Button(self.AdvancedWindow,
                              text='TVL Dump',
                              font=self.font,
                              bg=BG2,
                              fg=FG1,
                              activebackground=BG2,
                              activeforeground=FG1,
                              command=self.TLVtoTxt,
                              width=BUTTONWIDTH,
                              state='normal')
        TLVButton.pack(padx=5, pady=5, side=tk.LEFT)

        # Add function to save TxFrame to .txt file

    def SaveRx(self):
        try:
            path = tk.filedialog.asksaveasfile(initialfile=f"{filenametime()}_GUI_RX_Capture.txt",
                                               filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name
            with open(path, 'w') as f:
                f.write(self.rxText.get(1.0, tk.END))  # Write to file *exactly* how it is displayed in rxText

        except AttributeError:
            # No selection
            pass

    def TLVtoTxt(self):
        if not self.search:
            tk.messagebox.showerror('Error', 'No found devices')
            return
        try:
            path = tk.filedialog.asksaveasfile(initialfile=f"{filenametime()}_TVL_Data.txt",
                                               filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name
            self.search.saveTVL(path)
        except AttributeError:
            # No selection
            pass

    def RunCommandProfile(self, filepath):
        if filepath == '':
            tk.messagebox.showwarning("Error", "No file selected")
            return

        with open(filepath, 'r') as f:
            command_list = f.readlines()

        for command in command_list:
            if command.count('#') or command.count('//'):
                continue
            self.Command.set(command.strip('\n'))
            self.CommandLineSend()
            self.Update()
            self.AdvancedWindow.focus_set()

    def LoadCommandProfile(self, path_text):
        if self.com:
            self.com.close()
        path = tk.filedialog.askopenfile(filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name
        path_text.set(path)
        self.AdvancedWindow.focus_set()
        if self.com:
            self.com.open()

    def SaveCommandProfile(self):
        if self.com:
            self.com.close()
        path = tk.filedialog.asksaveasfile(initialfile='Untitled.txt',
                                           filetypes=(("Text files", "*.txt"), ("All files", "*.*"))).name
        self.AdvancedWindow.focus_set()

        global txlist
        txt = ''
        for command in txlist:
            txt += F"{command}\n"

        with open(path, 'w') as f:
            f.write(txt)
        if self.com:
            self.com.open()

    def updatePeriod(self, event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)

    def Reset(self):
        self.clear()
        self.SendMessage(self.command.reset())
        self.Disconnect()

    def updateDSAfromSlider(self, event):
        if not self.com:
            return
        value = float(self.DSAScale.get())
        # self.DSAScale.set(value)
        self.master.update()
        atten = Communication.dB_to_Hex(self.model['DSA'], value)
        if checkdict(self.hidden_buffer, 'Band'):
            self.SendMessage(
                self.command.setDSA(str(self.hidden_buffer['Band'] - int(self.model['band_DSA_offset'])), str(atten)))
        self.DSA_update = True
        self.Poll()

    def updateDSA(self):
        if not self.com:
            return
        value = float(self.DSAScale.get())
        # self.DSAScale.set(value)
        self.master.update()
        atten = Communication.dB_to_Hex(self.model['DSA'], value)
        if checkdict(self.hidden_buffer, 'Band'):
            self.SendMessage(
                self.command.setDSA(str(self.hidden_buffer['Band'] - int(self.model['band_DSA_offset'])), str(atten)))
        self.DSA_update = True
        self.Poll()

    def DSA_Change(self, direction):
        if direction:
            value = float(self.DSAScale.get()) + self.DSA_resolution
        else:
            value = float(self.DSAScale.get()) - self.DSA_resolution
        if (value > self.DSA_range[1]) or (value < self.DSA_range[0]):
            return
        self.DSAScale.set(value)
        self.updateDSA()

    def UpdateCommunication(self, method: int) -> None:
        if method == 1:
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            # self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.IP_EN.set(not self.IP_EN.get())
            self.IP['state'] = 'disabled'
            # self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'
        self.Disconnect()

    def Connect(self) -> None:
        if self.com:
            self.Disconnect()
            return
        if self.announce_capture_flag:
            self.StatusLightFlash('red', True)
            self.statusLightBackground.itemconfig(self.statusLight, fill='red')
            return

        self.StatusLightFlash('orange', False)
        # Ideally, I pass the serial.tools variable and callm port.device to get the COMX connection, but the class
        # is built to accept a str or int already
        ip = self.IP.get()
        serial_port = self.SerialPort.get()
        if ip.count('IP:'):
            ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', ip)[0]  # Get IP address from string
        if serial_port.count('Port:'):
            cut = serial_port[serial_port.index('Port:') + 5:]
            serial_port = cut[:cut.index(',')]  # Cut port number from string
        else:
            serial_port = filterPort(serial_port)

        # Append new entry to serial device list
        if self.SerialPort.get() not in self.serial_devices and self.SerialPort.get() != '':
            self.serial_devices.append(self.SerialPort.get())
            self.SerialPort['values'] = self.serial_devices

        # Append new entry to ip device list
        if self.IP.get() not in self.ip_devices and self.IP.get() != '':
            self.ip_devices.append(self.IP.get())
            self.IP['values'] = self.ip_devices

        #   Open connection into self.com
        error = ''
        try:
            if self.VLNB:
                # Virtual connection to VLNB
                self.com = True
            elif self.IP_EN.get() and ip and self.model['IP']:
                # Connect via TCP
                self.com = Communication.SocketConnection(ip, SOCKET_PORT)
            elif self.Serial_EN.get() and serial and self.model['serial']:
                # Connect via Serial
                if self.model['serial'] == 'RS232':
                    self.com = Communication.RS232Connection(serial_port)
                else:
                    self.com = Communication.RS485Connection(serial_port)

        except socket.gaierror as e:
            self.buffer['Error'] = Catch_Exception(e, "Invalid IP address")
        except ConnectionRefusedError as e:
            self.buffer['Error'] = Catch_Exception(e, "Connection refused")
        except TimeoutError as e:
            self.buffer['Error'] = Catch_Exception(e, "Connection timed out")
        except serial.serialutil.SerialException as e:
            self.buffer['Error'] = Catch_Exception(e, "Serial port not found")
        except Exception as e:
            self.buffer['Error'] = Catch_Exception(e)

        self.UpdateStatusWindow()

        #   If com passes, run initial conditions
        if self.com:
            self.connectButton['text'] = 'Disconnect'
            self.statusLightBackground.itemconfig(self.statusLight, fill='light green')
            self.EnableButtons('state', 'normal')
            self.EnableButtons('bg', BG2)
            self.DSAScale['state'] = 'normal'
            self.first_loop = True
            self.DSA_update = True
            self.Poll()
            return

        self.StatusLightFlash('red', True)
        self.statusLightBackground.itemconfig(self.statusLight, fill='red')

    def Disconnect(self):
        self.connectButton['text'] = 'Connect'
        self.statusLightBackground.itemconfig(self.statusLight, fill='red')
        self.clear()
        self.EnableButtons('state', 'disabled')
        self.EnableButtons('bg', BG1)
        self.DSAScale['state'] = 'disabled'
        self.master.update()
        self.Close_Communication()

    def Quit(self):
        self.quit = True
        self.Close_Communication()

    def Close_Communication(self):
        if type(self.com) == bool or not self.com:
            self.com = None
            return
        self.com.close()
        self.com = None

    def CommandLineSend(self):
        message = self.Command.get()
        if self.txlist.count(message) > 0:
            self.txlist.remove(message)
        self.txlist.insert(0, message)
        self.Command['values'] = self.txlist
        temp = self.com.delay
        self.com.delay = 0.5
        reply = self.SendMessage(message)
        self.com.delay = temp

        self.rxText['state'] = 'normal'
        reply_string = ''
        for items in reply:
            reply_string += items
        self.rxText.insert(tk.END, reply_string + '\n')
        self.rxText.see(tk.END)
        self.rxText['state'] = 'disabled'

    def ClearRx(self):
        self.rxText['state'] = 'normal'
        self.rxText.delete('1.0', tk.END)
        self.rxText['state'] = 'disabled'

    def clear(self):
        self.clearbuffer()
        self.UpdateStatusWindow()
        self.DSAScale.set('0.5')
        self.first_loop = True

    def clear_band_colour(self):
        for button in self.band['button']:
            button['bg'] = BG2

    def UpdateStatusWindow(self):
        self.StatusWindow['state'] = 'normal'
        self.StatusWindow.delete('1.0', tk.END)
        line = 1
        for key, value in self.buffer.items():
            self.StatusWindow.insert(tk.END, F"{key}:  {value}\n")
            self.StatusWindow.tag_add(F"{line}", F"{line}.0", F"{line}.{len(key) + 1}")
            self.StatusWindow.tag_config(F"{line}", font=self.fontLargeBold)
            self.StatusWindow.tag_add(F"{line}.1", F"{line}.{len(key) + 2}", F"{line}.end")
            self.StatusWindow.tag_config(F"{line}.1", font=self.fontLarge)
            if key == 'Error':
                self.StatusWindow.tag_config(F"{line}.1", font=self.fontLargeBold, foreground='red')
            line += 1
        self.StatusWindow['state'] = 'disabled'

    def EnableButtons(self, option, newstate):
        for button in self.button_list:
            try:
                button[option] = newstate
            except:
                pass

    def StatusLightFlash(self, colour, flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight, 'fill')
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight, fill=colour1)
        self.master.update()
        if (flash == True):
            colour2 = 'dark ' + colour
            while (i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight, fill=colour1)
                self.master.update()
                time.sleep(delay / 2)
                self.statusLightBackground.itemconfig(self.statusLight, fill=colour2)
                self.master.update()
                time.sleep(delay / 2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight, fill=colour1)
            self.master.update()

    def Send(self, message: str) -> list:

        #print(f'[SEND]: {message}')
        reply = []
        try:
            #   self.VLNB priority if both enabled. TBD
            if self.VLNB:
                reply = self.VLNB.query(message)

            elif self.com:
                reply = self.com.query(message)

        except Exception as e:
            self.Handle_Exception(e)
        #print(f'[REPLY]: {reply}')
        return reply

    def SendMessage(self, message: str) -> list:
        reply = self.Send(message)

        for item in reply:
            if item.count('$'):
                try:
                    newbuffer = self.command.decode(item)
                    self.UpdateBuffer(newbuffer)
                except Exception as e:
                    self.Handle_Exception(e)
                    continue

        #print(self.buffer)
        #print(self.hidden_buffer)

        return reply

    def SendSettings(self, message: str) -> list:
        reply = self.Send(message)

        for item in reply:
            if item.count('$'):
                try:
                    newbuffer = self.command.decode(item)
                    self.UpdateSettings(newbuffer)
                except Exception as e:
                    self.Handle_Exception(e)
                    continue
        return reply

    def UpdateSettings(self, buffer) -> None:
        #print(buffer)
        n = list(buffer.keys())
        k = list(self.settings_buffer.keys())
        for key in n:
            if not k.count(key):
                buffer.pop(key)
        self.settings_buffer.update(buffer)

    def UpdateBuffer(self, newbuffer: dict) -> None:
        # Special cases + units
        for key, value in newbuffer.items():
            if key == 'Band':
                self.hidden_buffer['Band'] = int(value)
                newbuffer[key] = str(self.hidden_buffer['Band'] + self.model['band_display_offset'])
            elif key == 'Voltage':
                if self.command.version > 0:
                    newbuffer[key] = str(round(float(newbuffer[key]) * 0.1, 1))
                newbuffer[key] += ' V'
            elif key == 'DSA':
                self.hidden_buffer['DSA_hex'] = value
                self.hidden_buffer['DSA_f'] = Communication.Hex_to_dB(self.model['DSA'], str(value))
                if (self.hidden_buffer['DSA_f'] > self.DSA_range[1]):
                    newbuffer[key] = 'MUTED'
                else:
                    newbuffer[key] = str(self.hidden_buffer['DSA_f']) + ' dB'
            elif key == 'Current':
                newbuffer[key] += ' mA'
            elif key == 'Temperature':
                newbuffer[key] += ' C'
            elif key == 'Uptime':
                newbuffer[key] += ' seconds'
            elif key == 'Fault':
                newbuffer[key] = CommandManager.DecodeFault(0, newbuffer[key])
            elif key == 'Fault_Status':
                newbuffer[key] = CommandManager.DecodeFault(1, newbuffer[key])
            elif key == 'System_Status':
                newbuffer[key] = CommandManager.DecodeFault(2, newbuffer[key])
            elif key == 'CPU' or key.count('WM'):
                newbuffer[key] += ' %'

        try:
            newbuffer['Serial_Number'] = newbuffer['CID'] + '-' + newbuffer['UID']
            newbuffer.pop('CID')
            newbuffer.pop('UID')
        except KeyError:
            pass

        self.buffer.update(newbuffer)

    def Poll(self):
        #   Get status
        self.SendMessage(self.command.getst())

        #   Update Band button colour
        if self.model['bands']:
            index = self.hidden_buffer['Band'] - self.model['band_DSA_offset']
            self.clear_band_colour()
            try:
                self.band['button'][index]['bg'] = 'green'
            except IndexError:
                self.buffer['Error'] = 'Wrong device detected. GUI functionality limited.'

        #   Update DSA and slider if applicable
        if self.DSA_update:
            self.SendMessage(self.command.getDSA(str(self.hidden_buffer['Band'] - int(self.model['band_DSA_offset']))))
            if self.buffer['DSA']:
                self.DSA_update = False
                self.DSAScale.set(self.hidden_buffer['DSA_f'])

        #   Get unit information only if change has been made
        if self.first_loop:
            self.first_loop = False
            # self.SendMessage(self.command.getDSA(str(self.hidden_buffer['Band']-int(self.model['band_DSA_offset']))))
            if self.buffer['DSA']:
                self.DSA_update = False
                self.DSAScale.set(self.hidden_buffer['DSA_f'])
            self.SendMessage(self.command.build())
            if self.model['Info']:
                self.SendMessage(self.command.getmm())
            if self.model['Log']:
                self.SendMessage(self.command.getlog())

        #   Update mute status (if applicable)
        if self.model['Mute']:
            if self.hidden_buffer['DSA_hex'] == '0x7F':
                self.hidden_buffer['Mute'] = True
                self.MuteDSAButton['bg'] = 'red'
            else:
                self.hidden_buffer['Mute'] = False
                self.MuteDSAButton['bg'] = BG2

        #   Transfer new information from buffer to display
        self.UpdateStatusWindow()

    def Handle_Exception(self, exception):
        self.buffer['Error'] = Catch_Exception(exception)
        self.UpdateStatusWindow()

    def Popup(self, name, width, height):
        self.window = tk.Toplevel(self.master)
        self.window.title(F"{name}")
        self.window.geometry(F"{width}x{height}")
        self.window.minsize(width, height)
        self.window.configure(bg=BG1)
        self.window.resizable(height=1, width=1)

        return self.window

    def Help(self):
        if WindowExists(self.window_list, 'Help'):
            return
        self.helpwindow = self.Popup("About", 250, 200)
        self.window_list.append(('Help', self.helpwindow))
        self.helpwindow.resizable(height=0, width=0)
        tk.Label(self.helpwindow, image=self.logo, bg=BG1).pack()
        tk.Label(self.helpwindow, text=F"Copyright © 2023 Orbital Research Ltd", font=self.font, bg=BG1, fg=FG1).pack()
        # tk.Label(self.helpwindow,text=F"Build: {VERSION}",font=self.font,bg=BG1,fg=FG1).pack()
        tk.Button(self.helpwindow, font=self.font, bg=BG1, fg=FG1, activebackground=BG1, activeforeground=FG1,
                  disabledforeground=FG1, text=F"Build: {VERSION}", command=self.AdvancedFeaturesWall, bd=0,
                  state='active').pack()
        tk.Button(self.helpwindow, font=self.font, bg=BG1, fg=FG1, activebackground=BG1, activeforeground=FG1,
                  disabledforeground=FG1, text='https://orbitalresearch.net',
                  command=lambda url='https://orbitalresearch.net/': webbrowser.open(url), bd=0, state='active').pack()

    def ModelSelected(self):
        if (self.model_selected):
            return True
        tk.messagebox.showwarning("Warning", "Please select model")
        return False

    def clearbuffer(self) -> None:
        self.buffer.update(dict.fromkeys(self.buffer, ''))

    def ModelSupport(self, key):
        if (key == 'filter'):
            if (self.model['filter'] != 0):
                return True
        if (key == 'IP'):
            if (self.model['IP']):
                return True
        tk.messagebox.showwarning("Warning", "Model does not support functionality")
        return False

    def FactoryReset(self):
        if not self.ModelSelected():
            return
        if not self.ModelSupport('filter'):
            return
        if not self.com:
            tk.messagebox.showwarning("Warning", "No connection found")
            return
        answer = tk.messagebox.askokcancel("Factory Reset", "Do you wish to reset this unit?")
        if answer:
            self.SendMessage(self.command.factory())
        self.first_loop = True
        self.DSA_update = True

    def donothing(self):
        if not self.ModelSelected():
            return
        tk.messagebox.showwarning("Warning", "Functionality under construction")

    def Testing(self, item):
        self.buffer['Error'] = str(item)
        self.UpdateStatusWindow()

    def Update(self):
        # self.UpdateStatusWindow()
        for name, window in self.window_list:
            window.update()
        self.master.update()
        self.master.update_idletasks()

def item() -> str:
    # Super secure item
    # Obscure item
    num1 = 400
    num2 = 1100
    return str(((num1 * 8 + 6)) * 2 + 1) + 'c' + str(int(num2 / 2)) + 'b2'

def get_path(filename):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, filename)
    else:
        return filename

def load_cfgs() -> list[Profile.Model]:
    profiles = []
    # Load included/default models
    for default_model in DEFAULT_MODELS:
        new_model = Profile.Model(get_path(default_model))
        if new_model.is_valid:
            profiles.append(new_model)

    # Search cwd for other .cfg or .json
    files = os.listdir(os.getcwd())
    for file in files:
        if file.endswith('.cfg') or file.endswith('.json'):
            new_model = Profile.Model(file)
            if not new_model.is_valid:
                continue
            profile_names = [model.name for model in profiles]
            if new_model.name not in profile_names:
                profiles.append(new_model)

    return profiles

def WindowExists(window_list, name):
    for n, window in window_list:
        if (n == name):
            if (tk.Toplevel.winfo_exists(window)):
                window.deiconify()
                return True
            else:
                window_list.remove((n, window))
                return False
    return False


def Catch_Exception(exception, custom_message=''):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    #error = [exc_type, fname, exc_tb.tb_lineno, exception]
    error = exception

    #Log exception

    if (exc_type == IndexError):
        error = ''

    if custom_message:
        return custom_message.upper()

    return error


def updatetime():
    timer = datetime.datetime.now()
    return timer.strftime("%Y-%b-%d  %H:%M:%S")


def filenametime():
    timer = datetime.datetime.now()
    return timer.strftime("%Y%b%d%H%M%S")


def LoadSettings(key, identifier, settings_list):
    for settings in settings_list:
        if (settings[key] == identifier):
            return settings
    return None


def FormatText(text, max_characters):
    temp = text.zfill(max_characters)
    spaced_text = temp.replace('0', ' ', max_characters - len(text))

    return spaced_text


def filterPort(text: any) -> str:
    # filter port number from string
    if type(text) == tuple:
        text = text[1]
    stripped_port = text.split('COM')[-1]
    filtered_port = ''
    for item in stripped_port:
        if (item.isdigit()):
            filtered_port += item

    return filtered_port


def checkdict(dictionary: dict, key: str) -> bool:
    if (list(dictionary.keys()).count(key)):
        return True
    return False


def Load_All_Settings(file: str) -> list[dict[any]]:
    # Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)

    # Generate list of dictionaries from matrix, grouping bands together if model name left blank
    key_list = matrix[0]
    output_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            if value == key_list[index]:  # Skips all keys
                continue
            if value == 'TRUE':
                value = True
            if value == 'FALSE':
                value = False
            if (key_list[index] == 'band_name' or key_list[index] == 'name' or key_list[
                index] == 'serial'):  # Only columns preserved as string
                temp_dict[key_list[index]] = value
            else:
                temp_dict[key_list[index]] = int(value)
        output_list.append(temp_dict)
    output_list.reverse()

    output_list.pop(0)

    return output_list


def SearchNetwork(search, duration):
    global FOUND_DEVICES, THREAD_COMPLETE
    search.Network(solicitation=False, duration=duration) # Stopping function. Returns devices when completed
    THREAD_COMPLETE = True
    FOUND_DEVICES = search.network_devices


if __name__ == '__main__':
    root = tk.Tk()
    myapp = GUI(root)
    myapp.master.protocol("WM_DELETE_WINDOW", myapp.Quit)

    poll_timer = time.time()
    error_timer = time.time()
    VLNB_timer = time.time()
    network_search_timer = time.time()
    error_trigger = False
    search_time_remaining = 35
    search_duration = search_time_remaining + 2
    listener_thread = threading.Thread(target=lambda: SearchNetwork(myapp.search, search_duration))

    while True:
        timer = time.time()

        #   Clock Task
        myapp.clock_text.set(updatetime())

        #   VLNB Timer Task
        if myapp.VLNB and timer - VLNB_timer > 1:
            VLNB_timer = timer
            myapp.VLNB.update()

        #   Poll Task
        if timer - poll_timer > myapp.PERIOD:
            poll_timer = timer
            if myapp.com:
                if myapp.settings_check:
                    # Skips Poll if checking settings to avoid settings being handled by the buffer manager
                    myapp.settings_check = False
                else:
                    myapp.Poll()

        #   Search unit Task
        if myapp.announce_capture_flag:
            myapp.IP.set(f'Searching for network devices... (Approx time remaining: {search_time_remaining} seconds)')
            if THREAD_COMPLETE:
                listener_thread.join()
                listener_thread = threading.Thread(
                    target=lambda: SearchNetwork(myapp.search, search_duration))  # Overwrite with new thread
                myapp.announce_capture_flag = False
                THREAD_COMPLETE = False
                #print(FOUND_DEVICES)
                myapp.ip_devices = myapp.ConvertFoundDevices(FOUND_DEVICES)
                myapp.IP['values'] = myapp.ip_devices
                if len(myapp.ip_devices) > 0:
                    myapp.IP.set('NEW DEVICES ------------>')
                else:
                    myapp.IP.set('')
                message = f"Network search complete.\n{len(FOUND_DEVICES)} devices found."
                tk.messagebox.showinfo(title='Search', message=message)
                search_time_remaining = search_duration
                #myapp.search.TLV_Readable_Dump()

            elif not listener_thread.is_alive():
                network_search_timer = time.time()
                myapp.search = Communication.Search()
                listener_thread.start()

            if timer - network_search_timer > 1:
                search_time_remaining -= int(timer - network_search_timer)
                if search_time_remaining <= 0:
                    search_time_remaining = 0
                network_search_timer = time.time()

        #   Error Task
        if myapp.buffer['Error'] and not error_trigger:
            error_timer = timer
            error_trigger = True
        elif error_trigger:
            if timer - error_timer > 10:
                error_trigger = False
                myapp.buffer['Error'] = ''
                myapp.UpdateStatusWindow()

        if myapp.quit:
            if listener_thread.is_alive():
                listener_thread.join()
            break

        #   Update GUI Task
        myapp.Update()

        time.sleep(0.05)

    #   Close App
    myapp.master.destroy()
    sys.exit()
