#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#   ------Version Log-------
#   R1 - 2023-03-21 -   Remove command management from GUI and store here
#                       Create class Command to manage command structures and set/get functions

import csv

class Command:
    #   ------USE-------
    #   Initialize Command class with filter version (int)(0 = TriKa/DKa, 1 = Horizon WBKaLNB, 2 = Standard WBKaLNB)
    #   Access class methods to format commands based on unit filter version.
    #   Ex.
    #   version = 0                 # Set unit version
    #   command = Command(version)  # Create instance of Command structure with unit filter type
    #   command.build()                 # Use instance to call commands. This method will return string '$build,*'
    #   command.setDSA(band, DSA)       # provide variables. This method returns f'$setda,{band},{DSA},*'
    #   command.setth(voltHigh=320)     # set command parameters with keywords. This method returns f'$setth,,,,,{voltHigh},,,,*'

    def __init__(self, version: int, command_filter_path: str, fault_codes_path: str):
        self.version = version
        self.response_syntax = self.LoadCommandFilter(command_filter_path)
        self.all_fault_codes = self.LoadFaultCodes(fault_codes_path)

    @staticmethod
    def build() -> str:
        return '$build,*'

    @staticmethod
    def clral() -> str:
        return '$clral,*'

    @staticmethod
    def factory() -> str:
        return '$factory,*'

    @staticmethod
    def getip() -> str:
        return '$getip,*'

    @staticmethod
    def getlog() -> str:
        return '$getlog,*'

    @staticmethod
    def getmm() -> str:
        return '$getmm,*'

    @staticmethod
    def getst() -> str:
        return '$getst,*'

    @staticmethod
    def setst(band: str) -> str:
        return f'$setst,{band},*'

    @staticmethod
    def reset() -> str:
        return '$reset,*'

    @staticmethod
    def sblon(timeout: str = '') -> str:
        return f'$sblon,{timeout},*'

    @staticmethod
    def setan(lldp: str = '', timer: str = '') -> str:
        return f'$setan,{lldp},{timer},*'

    @staticmethod
    def getan() -> str:
        return f'$setan,*'

    @staticmethod
    def setDSA(band: str, value: str = '') -> str:
        return f'$setda,{band},{value},*'

    @staticmethod
    def getDSA(band: str) -> str:
        return f'$setda,{band},*'

    @staticmethod
    def muteDSA(enable: str) -> str:
        return f'$muteda,0,{enable},*'

    @staticmethod
    def sethost(hname: str) -> str:
        return f'$sethost,{hname},*'

    @staticmethod
    def gethost() -> str:
        return f'$sethost,*'

    @staticmethod
    def setStaticIP(DHCPretries: str = '', sIPaddr: str = '', sGateway: str = '', sSubnet: str = '') -> str:
        return f'$setip,{DHCPretries},{sIPaddr},{sGateway},{sSubnet},*'

    @staticmethod
    def getStaticIP() -> str:
        return f'$setip,*'

    @staticmethod
    def getth():
        return '$setth,*'

    def setth(self, currentHigh: str = '', currentLow: str = '0', tempHigh: str = '', tempLow: str = '',
              voltHigh: str = '', voltLow: str = '', stackLow: int = '', cpuHigh: int = '') -> str:
        if self.version == 1:
            return f'$setth,{currentHigh},{currentLow},{tempHigh},{tempLow},{voltHigh},{voltLow},{stackLow},{cpuHigh},*'
        elif self.version == 2:
            return f'$setth,{currentHigh},{currentLow},{tempHigh},{tempLow},{voltHigh},{voltLow},*'

    @staticmethod
    def gettime() -> str:
        return f'$wrutc,*'

    @staticmethod
    def setwrutc(unixts: str) -> str:
        return f'$wrutc,{unixts},*'

    @staticmethod
    def setwrrtc(yyyymmdd: str, hhmmss: str = '', utcoffset: str = '') -> str:
        return f'$wrrtc,{yyyymmdd},{hhmmss},{utcoffset},*'

    def ReplyFilter(self, message: str) -> dict:
        newbuffer = {}
        reply = message.split(',')
        reply.pop(-1)  # remove '*'

        #   Determine which message was received
        command_keys = ()
        for command in self.response_syntax:
            if command[0] == reply[0].lower():  # find which command to be filtered
                command_keys = command

        #   Return if reply unrecognized
        if not command_keys:
            return newbuffer

        for index, value in enumerate(reply):
            if command_keys[index] != '-':
                flag = False
                temp_key = ''
                temp_value = ''
            if index == 0:  # skip identifier
                continue
            elif command_keys[index] == '-' or flag:  # grouping items to leftmost key with "-" character
                if not flag:
                    flag = True
                    temp_key = command_keys[index - 1]
                    temp_value = reply[index - 1]
                temp_value += ', ' + value
                newbuffer.update({temp_key: temp_value})
            elif command_keys[index].isdigit():  # skip numerical placeholders
                continue
            elif command_keys[index] == '':  # if key is blank
                continue
            else:
                newbuffer[command_keys[index]] = reply[index]

        return newbuffer

    def DecodeFault(self, version: int, fault: str) -> str:

        if not self.all_fault_codes:
            return fault

        fault_codes = self.all_fault_codes[version]

        #   Determine bit length, default 16 bit
        bits = 16
        if version > 0:
            bits = 32

        # Format string to list of bits, little endian
        number, pad, rjust, size, kind = int(fault, 16), '0', '>', bits, 'b'
        t = F'{number:{pad}{rjust}{size}{kind}}'
        bit_list = list(t)
        bit_list.reverse()

        # Decode error from list of bits
        error = ''
        for index, bit in enumerate(bit_list):
            if (bit == '1'):
                if (fault_codes[index] == ''):  # skip unlabelled/ignored fault bits
                    continue
                error += f'{fault_codes[index]}, '
        if error:
            error = error[:-2]  # pop last ', ' off list

        return error

    def LoadFaultCodes(self, file: str) -> tuple:
        # Open .csv as matrix
        with open(file) as f:
            file = csv.reader(f)
            matrix = list(file)

        fault_matrix = []
        i = 0
        while (i < len(matrix[0])):
            fault_list = []
            for index, row in enumerate(matrix):
                if (index == 0):
                    continue
                if (index > 32):
                    break
                fault_list.append(row[i])
            fault_matrix.append(tuple(fault_list))
            i += 1

        fault_matrix.pop(0)

        return tuple(fault_matrix)

    def LoadCommandFilter(self, file: str) -> tuple:
        # Open .csv as matrix
        with open(file) as f:
            file = csv.reader(f)
            matrix = list(file)

        matrix.pop(0)
        filter_list = []
        command_list = []
        for index, row in enumerate(matrix):
            if (row[0] != '' and index != 0):
                filter_list.append(command_list)
                command_list = []
            row.pop(0)
            command_list.append(tuple(row))
            if (row == matrix[-1]):
                filter_list.append(tuple(command_list))

        return tuple(filter_list[self.version])
