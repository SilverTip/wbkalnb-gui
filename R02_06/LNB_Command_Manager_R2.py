#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#   ------Version Log-------
#   R1 - 2023-03-21 -   Remove command management from GUI and store here
#                       Create class Command to manage command structures and set/get functions
#   R2 - 2023-06-23 -   Remove .csv files for fault + command manager
#                       Hard code command syntax and fault codes, add match function to select syntax
#                       Fix commas

class Command:
    # ------USE-------
    # Initialize Command class with filter version (int)(0 = TriKa/DKa, 1 = Special WBKaLNB, 2 = Standard WBKaLNB)
    # Access class methods to format commands based on unit filter version.
    # Ex.
    # version = 0                     # Set unit version
    # command = Command(version)      # Create instance of Command structure with unit filter type
    # command.build()                 # Use instance to call commands. This method will return string '$build,*'
    # command.setDSA(band, DSA)       # provide variables. This method returns f'$setda,{band},{DSA},*'
    # command.setth(voltHigh=320)     # set individual parameters with keywords. This method returns f'$setth,,,,,{voltHigh},,,,*'
    # command.decode('$getst...')     # Decode reply messages into corresponding dict: key:value pairs

    command0 = [
        ['$build', 'CID', 'UID', '', 'Firmware_Ver', '-'],
        ['$statr', 'CID', 'UID', '', 'Band', 'Voltage', 'Temperature', '', 'Fault'],
        ['$setdar', 'CID', 'UID', '', '', 'DSA']
    ]
    command1 = [
        ['$buildr', 'CID', 'UID', '', 'Firmware_Ver', '-', '-', '-', ''],
        ['$statr', 'CID', 'UID', '', 'Uptime', '', 'System_Status', 'Fault_Status', '', '', '', 'Voltage', '', 'Temperature', ''],
        ['$mutedar', 'CID', 'UID', '', '', 'DSA'],
        ['$setdar', 'CID', 'UID', '', '', 'DSA'],
        ['$setipr', 'CID', 'UID', '', 'DHCP_Retries', 'Static_IP', 'Static_Gateway', 'Static_Subnet'],
        ['$ipcf', 'CID', 'UID', 'IP', 'Subnet', 'Gateway', 'MAC'],
        ['$ack', '', 'CID', 'UID'],
        ['$time', 'CID', 'UID', 'UNIXTS', 'YYYYMMDD', 'HHMMSS', 'UTCOFFSET', ''],
        ['$setthr', 'CID', 'UID', '', 'Current_High', 'Current_Low', 'Temperature_High', 'Temperature_Low', 'Voltage_High', 'Voltage_Low', 'Stack_L', 'CPU_H'],
        ['$setanr', 'CID', 'UID', '', 'LLDP_Enable', 'LLDP_Timer', '', ''],
        ['$hostr', 'CID', 'UID', '', 'Hostname'],
        ['$mmr', '', 'Mfg', 'Model', 'Hardware_Ver', 'Hostname'],
        ['$getlogr', 'CID', 'UID', '', '', '', '', '', '', 'Stack_WM', 'CPU', 'CPU_WM']
    ]
    command2 = [
        ['$buildr', 'CID', 'UID', '', 'Firmware_Ver', '-', '-', '-', ''],
        ['$statr', 'CID', 'UID', '', 'Uptime', '', 'System_Status', 'Fault_Status', '', '', '', 'Voltage', '', 'Temperature', 'Band'],
        ['$setdar', 'CID', 'UID', '', '', 'DSA'],
        ['$setipr', 'CID', 'UID', '', 'DHCP_Retries', 'Static_IP', 'Static_Gateway', 'Static_Subnet'],
        ['$ipcf', 'CID', 'UID', 'IP', 'Subnet', 'Gateway', 'MAC'],
        ['$ack', '', 'CID', 'UID'],
        ['$time', 'CID', 'UID', 'UNIXTS', 'YYYYMMDD', 'HHMMSS', 'UTCOFFSET', ''],
        ['$setthr', 'CID', 'UID', '', 'Current_High', 'Current_Low', 'Temperature_High', 'Temperature_Low', 'Voltage_High', 'Voltage_Low', '', ''],
    ]

    def __init__(self, version: int):
        self.version = version
        self.response_syntax = self.select_syntax()

    def select_syntax(self) -> list:
        match self.version:
            case 0:
                return self.command0
            case 1:
                return self.command1
            case 2:
                return self.command2
            case _:
                return []

    def decode(self, message: str) -> dict:
        newbuffer = {}
        reply = message.split(',')
        reply.pop(-1)  # remove '*'

        if not self.response_syntax:
            raise SyntaxError('No command syntax found')

        #   Determine which message was received
        command_keys = ()
        for command in self.response_syntax:
            if command[0] == reply[0].lower():  # find which command to be filtered
                command_keys = command
                break

        #   Return if reply unrecognized
        if not command_keys:
            return newbuffer

        for index, value in enumerate(reply):
            if command_keys[index] != '-':
                flag = False
                temp_key = ''
                temp_value = ''
            if index == 0:  # skip identifier
                continue
            elif command_keys[index] == '-' or flag:  # grouping items to leftmost key with "-" character
                if not flag:
                    flag = True
                    temp_key = command_keys[index - 1]
                    temp_value = reply[index - 1]
                temp_value += ', ' + value
                newbuffer.update({temp_key: temp_value})
            elif command_keys[index].isdigit():  # skip numerical placeholders
                continue
            elif command_keys[index] == '':  # if key is blank
                continue
            else:
                newbuffer[command_keys[index]] = reply[index]

        return newbuffer

    @staticmethod
    def build() -> str:
        return '$build,*'

    @staticmethod
    def clral() -> str:
        return '$clral,*'

    @staticmethod
    def factory() -> str:
        return '$factory,*'

    @staticmethod
    def getip() -> str:
        return '$getip,*'

    @staticmethod
    def getlog() -> str:
        return '$getlog,*'

    @staticmethod
    def getmm() -> str:
        return '$getmm,*'

    @staticmethod
    def getst() -> str:
        return '$getst,*'

    @staticmethod
    def setst(band: str) -> str:
        return f'$setst,{band},*'

    @staticmethod
    def reset() -> str:
        return '$reset,*'

    @staticmethod
    def sblon(timeout: str = '') -> str:
        return f'$sblon,{timeout},*'

    @staticmethod
    def setan(lldp: str = '', timer: str = '') -> str:
        return f'$setan,{lldp},{timer},*'

    @staticmethod
    def getan() -> str:
        return f'$setan,*'

    @staticmethod
    def setDSA(band: str, value: str = '') -> str:
        return f'$setda,{band},{value},*'

    @staticmethod
    def getDSA(band: str) -> str:
        return f'$setda,{band},*'

    @staticmethod
    def muteDSA(enable: str) -> str:
        return f'$muteda,0,{enable},*'

    @staticmethod
    def sethost(hname: str) -> str:
        return f'$sethost,{hname},*'

    @staticmethod
    def gethost() -> str:
        return f'$sethost,*'

    @staticmethod
    def setStaticIP(DHCPretries: str = '', sIPaddr: str = '', sGateway: str = '', sSubnet: str = '') -> str:
        return f'$setip,{DHCPretries},{sIPaddr},{sGateway},{sSubnet},*'

    @staticmethod
    def getStaticIP() -> str:
        return f'$setip,*'

    @staticmethod
    def getth():
        return '$setth,*'

    def setth(self, currentHigh: str = '', currentLow: str = '0', tempHigh: str = '', tempLow: str = '',
              voltHigh: str = '', voltLow: str = '', stackLow: str = '', cpuHigh: str = '') -> str:
        if self.version == 1:
            return f'$setth,{currentHigh},{currentLow},{tempHigh},{tempLow},{voltHigh},{voltLow},{stackLow},{cpuHigh},*'
        elif self.version == 2:
            return f'$setth,{currentHigh},{currentLow},{tempHigh},{tempLow},{voltHigh},{voltLow},*'

    @staticmethod
    def gettime() -> str:
        return f'$wrutc,*'

    @staticmethod
    def setwrutc(unixts: str) -> str:
        return f'$wrutc,{unixts},*'

    @staticmethod
    def setwrrtc(yyyymmdd: str, hhmmss: str = '', utcoffset: str = '') -> str:
        return f'$wrrtc,{yyyymmdd},{hhmmss},{utcoffset},*'

def DecodeFault(version: int, fault: str):
    fault0 = [
        'Invalid Command',
        'Invalid Argument',
        'Under Temperature',
        'Over Temperature',
        'Under Voltage',
        'Over Voltage',
        'Under Current',
        "Over Current",
        'UART Comm Error',
        'UART Overrun Error',
        'UART Frame Error',
        'BOR or POR Event',
        'Bad EEEPROM',
        'EEPROM Not Restored',
        'Loss of Lock Alarm (LOLA)',
        'LO Board Comm Fault'
    ]
    fault1 = [
        '',
        'C_LNB - High Current Fault',
        '',
        '',
        '',
        '',
        'VIN - Low Voltage Fault',
        'VIN - High Voltage Fault',
        '',
        '',
        '',
        '',
        'High Temperature Fault',
        'Low Temperature Fault',
        '',
        '',
        'PLL Alarm (ALM) – Loss Of Lock Alarm (LOLA)',
        'PS Power Fault',
        'LNA/LNB Power Fault',
        'LO Power Fault',
        'DSA(S) Are MUTED',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        ''
    ]
    fault2 = [
        '',
        '',
        '',
        '',
        'C_LNB - High Current Fault',
        '',
        '',
        '',
        'Vin - Low Voltage Fault',
        'Vin - High Voltage Fault',
        '',
        'CPU Usage High Fault',
        'STACK low Fault',
        'High Temperature Fault',
        'Low Temperature Fault',
        'Watchdog Timer Fault',
        'System Status Flash Memory Fault',
        'Default Config Flash Fault',
        'Base Config Flash Fault',
        'UART Oerr Fault',
        'Rx Buff Overflow Fault',
        'POST Fail - Checksum Failure',
        'DSA Setpoint Unsuccessful',
        'DSA Setpoint Updated',
        'PLL Alarm - Loss Of Lock Alarm (LOLA)',
        'PS Power Fault',
        'LNA / LNB Power Fault',
        'LO Power Fault',
        'Network Configuration Updated',
        'LLDP Configuration Updated',
        'Device Hostname Updated',
        'Firmware Updated'
    ]
    fault_codes = None
    match version:
        case 0:
            fault_codes = fault0
        case 1:
            fault_codes = fault1
        case 2:
            fault_codes = fault2

    if not fault_codes:
        return fault

    #   Determine bit length, default 16 bit
    bits = 16
    if version > 0:
        bits = 32

    # Format string to list of bits, little endian
    number, pad, rjust, size, kind = int(fault, 16), '0', '>', bits, 'b'
    t = F'{number:{pad}{rjust}{size}{kind}}'
    bit_list = list(t)
    bit_list.reverse()

    # Decode error from list of bits
    error = ''
    for index, bit in enumerate(bit_list):
        if (bit == '1'):
            if (fault_codes[index] == ''):  # skip unlabelled/ignored fault bits
                continue
            error += f'{fault_codes[index]}, '
    if error:
        error = error[:-2]  # pop last ', ' off list

    return error

if __name__ == '__main__':
    print(DecodeFault(2,'0x11000000'))
    #print(decode.ReplyFilter('$ipcf,9999,001,10.0.10.101,10.0.10.1,255.255.255.1,eb:ec:ed:ef:eg:eh'))


