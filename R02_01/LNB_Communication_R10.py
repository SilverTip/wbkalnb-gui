#LNB_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-03-16 - Import code from KaLNB_TCP_Serial_Communication_R6.py. Rename module to include all LNBs. 
#R2 - 2022-03-23 - Filter responses based on type of connection - class methods to return interested values
#R3 - 2022-04-04 - filter, attenuation, and faults not dependent on comm type. 
#R4 - 2022-04-12 - updated faults
#R5 - 2022-05-04 - RS232 and Socket updated with /r/n
#R6 - 2022-05-05 - Manage decode exceptions, lengthen read delay for longest command ("cfg", ~150ms)
#R7 - 2022-07-27 - Minor timing and filter changes
#R8 - 2022-07-27 - Minor filter changes
#R9 - 2022-09-22 - Filter changes
#R10 - 20221108 - Move Filter to external database. RS485 dump everything after ACK into item in list. Breakup send/read/query

import socket
import serial
import time

class RS485_Connection:

    STX = b'\x02'
    EXT = b'\x03'
    ACK = b'\x06'
    
    #TriKa and DKa/DKu communications
    def __init__(self, port):
        self.ADDR = b'\x39\x32'
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
        port = 'COM'+str(self.port),
        baudrate = 9600,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        timeout = 2.0
        ) 
        self.connection = True  

    def close(self):
        self.ser.close()
        self.connection = False      

    def send(self, message):
        CMD = message.encode('utf-8')
        message = self.STX + self.ADDR + CMD + self.EXT    
        checksum = 0
        for num in message:
            checksum ^= num
        message += (checksum).to_bytes(1, byteorder='big')
        if(self.connection):
            self.ser.write(message)

    def read(self):
        character = ''
        word = ''
        reply = []
        if(self.connection):
            while((self.ser.in_waiting > 0) and (self.ser.isOpen())):
                character = self.ser.read(1)
                if(character == self.EXT):
                    chksum = self.ser.read(1) #Next byte is checksum ignore for now
                    if(word):
                        reply.append(word)
                        word = ''
                elif(character == self.ACK):
                    if(word):
                        reply.append(word)
                        word = ''
                    address = self.ser.read(2) #next 2 bytes are address so we can remove and ignore them (for now)
                else:
                    character = character.decode('utf-8')
                    word += character
        if(word):
            reply.append(word)

        return reply

    def query(self, message):
        if(self.connection):
            self.send(message)
            time.sleep(self.delay) #unstable <= 0.12.
            reply = self.read()

            return reply

class RS232_Connection:

    #WBKaLNB serial connection
    def __init__(self, port):
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
        port = 'COM'+str(self.port),
        baudrate = 38400,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )
        self.connection = True

    def close(self):
        self.connection = False
        self.ser.close()

    def send(self, message):
        if(self.connection):
            tx = bytes(str(message)+'\r\n','utf-8') 
            self.ser.write(tx)

    def read(self):
        character = ''
        word = ''
        reply = []
        if(self.connection):
            while(self.ser.in_waiting > 0):
                character = self.ser.read(1)
                try: 
                    character = character.decode('utf-8')     
                except:
                    #print("Failed")
                    continue #decode of character failed
                word += character
            
            reply = word.split('\n\r')
        
        return reply

    def query(self, message):
        if(self.connection):
            self.send(message)
            time.sleep(self.delay) #unstable <= 0.12.
            reply = self.read()

            return reply

class Socket_Connection:
    #WBKaLNB ethernet connection
    def __init__(self, IP, Port):
        self.timeout = 5
        self.delay = 0.15
        self.bytes = 1024
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.connection = False
        self.open()

    def open(self):
        self.sock.connect(self.server_address)
        self.connection = True

    def close(self):
        self.sock.close()
        self.connection = False

    def send(self, message):
        if(self.connection):
            self.sock.sendall(bytes(str(message)+'\r\n','utf-8'))
        #r,w,e = select.select([self.sock],[],[],0)

    def read(self):
        reply = []
        word = ''
        if(self.connection):
            received = self.sock.recv(self.bytes)
            for item in received:
                try:
                    character = chr(item)
                
                except:
                    continue #binary decode failed due to garbage
                word += character 
        
            reply = word.split('\r\n')
        
        return reply

    def query(self, message):
        if(self.connection):
            self.send(message)
            time.sleep(self.delay)
            reply = self.read()

            return reply

def attenuation(version,value,direction):
    new = 0.5
    if(version==1):
        if direction:
            new = hex(int(63-(value*2)))
        else:
            new = abs(int(value,16)-63)/2

    elif(version==2):
        if direction:
            new = hex(int(float(value)/0.25))
        else:
            new = int(value,16)*0.25
    
    return new 


if __name__ == '__main__':
    COM_PORT = 10
    #com = Socket_Connection('10.0.10.192',32019)
    #com = RS232_Connection(COM_PORT)
    com = RS485_Connection(COM_PORT)
    while(1):
        try:
            message = input('Command:')
            com.delay = 3.1
            if not com.connection:
                print("Open communications Method")
                com.open()
            reply = com.query(message)
            print(reply)
            #for item in reply: print(item)
        except KeyboardInterrupt:
            com.close()
            print('Closed communications method')
       

