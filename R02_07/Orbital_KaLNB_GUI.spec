# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['GUI_R02_07_00.py'],
             pathex=[],
             binaries=[],
             datas=[
			 ('Orbital STANDARD - RS485.cfg', '.'),
			 ('Orbital WIDEBAND - Ethernet.cfg', '.'),
			 ('Orbital WIDEBAND - RS485.cfg', '.'),
			 ('DEMO2.json', '.')
			 ],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
			 
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='Orbital KaLNB GUI 2.7.0',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None, 
		  icon='OrbIcon_Round.png',
		  version='version.rc')
