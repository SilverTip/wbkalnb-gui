# LNB_Communication
# Written by Benjamin Stadnik
# Orbital Research Ltd.

import socket
import serial
import time
import re
import serial.tools.list_ports
import datetime
import os.path

ENCODE_FORMAT = 'utf-8'


class RS485Connection:  # TriKa/DKa/DKu communications
    STX = b'\x02'
    EXT = b'\x03'
    ACK = b'\x06'

    def __init__(self, port):
        self.addr = b'\x39\x32'
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1.0
        )
        self.connection = True

    def close(self):
        self.ser.close()
        self.connection = False

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        command = self.STX + self.addr + message.encode(ENCODE_FORMAT) + self.EXT
        checksum = 0
        for num in command:
            checksum ^= num
        command += checksum.to_bytes(1, byteorder='big')
        # if self.connection:
        self.ser.write(command)

    def read(self) -> list:
        character = ''
        word = ''
        reply = []
        if not self.connection:
            return reply
        while (self.ser.in_waiting > 0) and self.ser.isOpen():
            character = self.ser.read(1)
            if character == self.EXT:
                checksum = self.ser.read(1)  # Next byte is checksum ignore for now
                if word:
                    reply.append(word)
                    word = ''
            elif character == self.ACK:
                if word:
                    reply.append(word)
                    word = ''
                address = self.ser.read(2)  # next 2 bytes are address, so we can remove and ignore them (for now)
            else:
                try:
                    character = character.decode(ENCODE_FORMAT)
                except UnicodeDecodeError:
                    continue
                word += character
        if word:
            reply.append(word)

        return reply

    def query(self, message: str) -> list:
        # if self.connection:
        self.send(message)
        time.sleep(self.delay)  # unstable <= 0.12.
        reply = self.read()

        return reply


class RS232Connection:  # WBKaLNB serial connection
    def __init__(self, port: int):
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=38400,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
        )
        self.connection = True

    def close(self):
        self.connection = False
        self.ser.close()

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        # if self.connection:
        tx = bytes(str(message) + '\r\n', ENCODE_FORMAT)
        self.ser.write(tx)

    def read(self) -> list:
        character = ''
        word = ''
        reply = []
        if self.connection:
            while self.ser.in_waiting > 0:
                character = self.ser.read(1)
                try:
                    character = character.decode(ENCODE_FORMAT)
                except UnicodeDecodeError:
                    continue  # decode of character failed
                word += character

            reply = word.split('*\r\n')

            if (reply[-1]) == '':
                reply.pop(-1)

        return reply

    def query(self, message: str) -> list:
        # if self.connection:
        self.send(message)
        time.sleep(self.delay)  # unstable <= 0.12.
        reply = self.read()

        return reply


class SocketConnection:  # WBKaLNB network connection
    def __init__(self, host: str, port: int):
        self.timeout = 5
        self.delay = 0.15
        self.bytes = 1024
        self.address = (host, port)
        self.connection = False
        self.open()

    def open(self) -> None:
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.sock.connect(self.address)
        self.connection = True

    def close(self) -> None:
        self.sock.close()
        self.connection = False

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        if self.connection:
            self.sock.sendall(bytes(str(message) + '\r\n', ENCODE_FORMAT))

    def read(self) -> list:
        reply = []
        word = ''
        if self.connection:
            received = self.sock.recv(self.bytes)
            for item in received:
                try:
                    character = chr(item)
                except:
                    # binary decode failed due to garbage non-ASCII characters
                    continue
                word += character

            reply = word.split('*\r\n')

            if (reply[-1]) == '':
                reply.pop(-1)

        return reply

    def query(self, message: str) -> list:
        if self.connection:
            self.send(message)
            time.sleep(self.delay)
            reply = self.read()

            return reply


def dB_to_Hex(version: int, dB: float) -> str:
    hex_value = '0x02'
    if dB > 31.5:  # Maximum value
        dB = 31.5
    if dB < 0:  # Minimum value
        dB = 0
    if version == 1:
        decimal = dB % 1
        if decimal == 0.25 or decimal == 0.75:
            dB += 0.25  # Round up
        hex_value = hex(int(63 - (dB * 2)))
    elif version == 2:
        hex_value = hex(int(dB / 0.25))

    return hex_value


def Hex_to_dB(version: int, hex_value: str) -> float:
    dB = 0.5
    if version == 1:
        dB = abs(int(hex_value, 16) - 63) / 2
    elif version == 2:
        dB = int(hex_value, 16) * 0.25

    return dB


class Search:
    # Class to manage searching for Orbital devices on a network or local serial devices
    # Main methods:
    #   Serial()
    #           Input:      None
    #           Output:     None
    #           Function:   Searches every serial port on local machine and returns items into self.serial_devices
    #           Info:       Compatible devices are stored as class instance rather than return function
    #                       Device Port, name, type are stored as dict in format of serial_device_formatting
    #
    #   Network(solicitation, verbose)
    #           Input:      Solicitation; True = discovery feature on port 30304, False = announce feature on port 30303
    #                       Verbose; default=False, True = print annoucements to console as they are obtained
    #           Output:     None
    #           Function:   Searches network devices according to solicitation.
    #                       Saves raw announcements into self.announcements list
    #                       Saves decoded announcements into self.network_devices
    #           Info:       Compatible devices are stored as class instance rather than return function
    #                       Device IP, name, type, etc. are stored as dict according to TLV_to_Useful
    #   TLV_Readable_Dump()
    #           Input:      None
    #           Output:     None
    #           Function:   Convert contents of self.announcements into corresponding TLV contents and print to console

    target = '255.255.255.255'
    port_solicited = 30304
    port_unsolicited = 30303
    solicited_search_delay = 0.2
    unsolicited_search_delay = 35
    serial_device_formatting = {"ID": 'Unknown', "Port": 'Unknown', "Type": 'Unknown', "Firmware": 'Unknown'}

    def __init__(self):
        self.serial_devices = []
        self.network_devices = []
        self.announcements = []

    def Serial(self, com_type='RS485') -> list:
        # Check each device connected COM channels
        devices = []
        for port in self.COMList():
            device = self.serial_device_formatting.copy()
            build = []
            if com_type != 'RS485':
                try:
                    com1 = RS232Connection(port)
                    build = com1.test()
                    com1.close()
                except OSError:
                    pass
            else:
                try:
                    com2 = RS485Connection(port)
                    build = com2.test()
                    com2.close()
                except OSError:
                    pass
            if build:
                device.update(self.Decode_Build(build))
                device['Type'] = com_type
                device['Port'] = port
                devices.append(device)

        self.serial_devices = devices
        return devices

    @staticmethod
    def Decode_Build(message: list) -> dict:
        build = {'ID': 'Unknown', 'Firmware': 'Unknown'}
        if not message:
            return build
        for item in message:
            if item.count('$BUILD'):
                build_string = item.split(',')
                if len(build_string) < 2:  # Item is echo: ['$build','*']
                    continue
                build['ID'] = build_string[1] + '-' + build_string[2]
                build['Firmware'] = build_string[4]
                return build

        return build

    @staticmethod
    def COMList() -> list:
        return [int(item.device.strip('COM')) for item in list(serial.tools.list_ports.comports())]

    @staticmethod
    def TLV_Decode(message: bytes) -> list:
        #   Input announcement, output list of dict of TLV fields
        #   Using Regular Expressions module and Horizon ICD_R7 (20220228)
        tlv_dict = {'field': None,
                    'description': None,
                    'address': None,
                    'sub': None,
                    'len': None,
                    'contents': None,
                    'value': None
                    }

        TLV = [
            (1, 'Chassis_ID', b'\x02', None, hex, '^([0-9A-Fa-f]{12})$'),
            (2, 'Port_ID', b'\x04', None, int, None),
            (3, 'Time-to-Live', b'\x06', None, int, None),
            (4, 'Port_Description', b'\x08', None, str, '^(ETH0)$'),
            (5, 'System_Name', b'\x0a', None, str, '^((ORBLNB-){1}[0-9A-Fa-f]{8})$'),
            (6, 'System_Description', b'\x0c', None, str,
             '^((LNB858-900XWS60,){1}[0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
            (7, 'System_Capability', b'\x0e', None, str, '^(LNB)$'),
            (8, 'Management_Address', b'\x10', None, str,
             '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'),
            (9, 'LLDP_MED_Capabilities', b'\xfe', b'\x01', int, None),
            (10, 'Hardware_Revision', b'\xfe', b'\x05', str, '^(r[0-9]{3})$'),
            (11, 'Firmware_Revision', b'\xfe', b'\x06', str, '^([0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
            (12, 'Serial_Number', b'\xfe', b'\x08', str, '^([0-9]{4,5})(-)[1-9]{1}[0-9]{0,4})$'),
            (13, 'Manufacturer_Name', b'\xfe', b'\x09', str, '^(ORBITAL RESEARCH)$'),
            (14, 'Model_Name', b'\xfe', b'\x0a', str, '^(LNB858-900XWS60)$'),
            (15, 'Asset_ID', b'\xfe', b'\x0b', str,
             '^((LNB858-900XWS60-00-){1}([0-9A-Fa-f]{2}[.]){5}[0-9A-Fa-f]{2})$')
        ]

        bytes_list = [x.to_bytes(1, 'big') for x in message]
        tlv_list = []
        for field, description, address, sub, character_format, decoder in TLV:
            if address == bytes_list[0]:
                index = 2
                tlv = tlv_dict.copy()
                if sub:
                    if sub == bytes_list[5]:
                        tlv['sub'] = '0x{:02x}'.format(int.from_bytes(sub, 'big'))
                        index = 6
                    else:
                        continue
                tlv['field'] = field
                tlv['description'] = description
                tlv['address'] = '0x{:02x}'.format(int.from_bytes(address, 'big'))
                tlv['len'] = int.from_bytes(bytes_list[1], "big")
                tlv['contents'] = ''.join('/0x{:02x}'.format(x) for x in [int.from_bytes(y, "big") for y in
                                                                          bytes_list[:index + tlv['len']]])
                trimmed_contents = b''.join(bytes_list[index:index + tlv['len']])
                if character_format == int:
                    tlv['value'] = int.from_bytes(trimmed_contents, 'big')
                elif character_format == hex:
                    tlv['value'] = trimmed_contents.hex()
                else:
                    tlv['value'] = trimmed_contents.decode('ascii')

                tlv_list.append(tlv)
                bytes_list = bytes_list[index + tlv['len']:]

        return tlv_list

    def TLVtoReadable(self, announcement: tuple, hexdump: bool) -> list:
        lines = []
        space = 38
        tlv_list = self.TLV_Decode(announcement[0])
        bytes_list = ['{:02x}'.format(x) for x in [int.from_bytes(y, 'big') for y in
                                                   [z.to_bytes(1, 'big') for z in announcement[0]]]]
        lines.append(F'Announcement Time: {announcement[2].strftime("%Y%b%d_%H%M%S_%f")}')
        lines.append(F'Address:Port: {announcement[1]}')
        # Hex dump start
        if hexdump:
            i = 0
            lines.append('----------------------------------------------------------------------------------------')
            lines.append('HEX DUMP:')
            lines.append('----------------------------------------------------------------------------------------')
            dump_len = 16
            while True:
                if i + dump_len > len(bytes_list):
                    r = len(bytes_list) - dump_len
                    lines.append(' '.join(bytes_list[i:i + r]))
                    break
                lines.append(' '.join(bytes_list[i:i + dump_len]))
                i += dump_len
        lines.append(F'-------------------------------------------------------------------------------------------')
        lines.append(F"{'DESCRIPTION'.ljust(21)} | {'DECODED VALUE'.ljust(space)} | {'PAYLOAD CONTENTS'.ljust(space)}")
        lines.append(F'-------------------------------------------------------------------------------------------')
        for item in tlv_list:
            description = item['description']
            contents = item['contents'].replace('/0x', ' ')
            value = str(item['value'])
            lines.append(F'{description.ljust(21)} | {value.ljust(space)} | {contents.ljust(space)}')

        return lines

    def SaveAnnouncementsToTxt(self, hexdump=False):
        for announcement in self.announcements:
            tlv_list = self.TLV_Decode(announcement[0])
            is_valid_device = False
            unitSN = 'Unknown-Device'               # Don't want ???? in filename
            for item in tlv_list:
                if item['description'] == 'Serial_Number':
                    CIDUID = str(item['value']).split('-', 1)
                    unitSN = '-'.join([CIDUID[0],CIDUID[1].zfill(3)])
                elif item['value'] == 'ORBITAL RESEARCH':
                    is_valid_device = True

            if is_valid_device:
                lines = self.TLVtoReadable(announcement, hexdump)
                lines.insert(0, F'Serial Number: {unitSN}')
                timestamp = announcement[2].strftime("%Y%b%d_%H%M%S_%f")
                filename = F"{unitSN}_{timestamp}_LLPD_Data.txt"
                if not os.path.isfile(filename):
                    self.saveTLV(filename,lines)

    def PrintAnnouncementsToConsole(self, hexdump=False):
        for announcement in self.announcements:
            lines = self.TLVtoReadable(announcement,hexdump)
            for line in lines:
                print(line)

    @staticmethod
    def saveTLV(filename: str, lines: list):
        with open(filename, 'w') as f:
            for line in lines:
                f.write(f"{line}\n")

    @staticmethod
    def TLV_to_Useful(tlv_list: list) -> dict:
        device = {}
        interested_categories = [
            ('Chassis_ID', 'MAC'),
            ('Management_Address', 'IP'),
            ('Serial_Number', 'S/N'),
            ('Firmware_Revision', 'F/W'),
            ('Hardware_Revision', 'H/W')
        ]
        for item in tlv_list:
            for category, keyword in interested_categories:
                if item['description'] == category:
                    device[keyword] = item['value']

        keys = list(device.keys())
        if not (keys.count('IP') and keys.count('MAC')):
            return {}

        return device

    @staticmethod
    def Listen(sock: socket.socket, timeout: float, verbose=False) -> list:
        response_list = []
        sock.settimeout(timeout)
        begin = datetime.datetime.now()
        try:
            while True:
                response = sock.recvfrom(2048)  # Blocking function. Timeout will cause exception
                now = datetime.datetime.now()
                formatted_response = (response[0],response[1], now)
                response_list.append(formatted_response)
                if verbose:
                    print(f'Device found:{formatted_response[1]}, {formatted_response[2].strftime("%Y%b%d_%H%M%S_%f")}')
                if now - begin > datetime.timedelta(seconds=timeout):
                    raise socket.timeout
        except socket.timeout:
            return response_list

    def add_network_devices(self, announce_list: list) -> list:
        # announce_list is list of tuple (bytes, (address, port), datetime)

        #   Append new announcements to class instance self.announcements
        for new_announcement in announce_list:
            if new_announcement[1] not in [old_announcement[1] for old_announcement in self.announcements]:
                self.announcements.append(new_announcement)

        new_devices = [self.TLV_to_Useful(self.TLV_Decode(new_announcement[0])) for new_announcement in announce_list]

        #   Append new devices to class instance self.network_devices
        for new_device in new_devices:
            if not new_device:
                continue
            if new_device not in self.network_devices:
                self.network_devices.append(new_device)

        return new_devices

    def Network(self, solicitation: bool, duration=0, verbose=False) -> list:
        announce_list = []
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        if solicitation:
            # Solicited a broadcast on port 30304
            if not duration:
                duration = self.solicited_search_delay
            sock.sendto(b'D\r\n', (self.target, self.port_solicited))
            announce_list += self.Listen(sock, duration, verbose)  # Measured worst case ~120ms
        else:
            # Wait for unsolicited announcements on port 30303
            if not duration:
                duration = self.unsolicited_search_delay
            sock.bind((socket.gethostbyname(socket.gethostname()), self.port_unsolicited))
            announce_list += self.Listen(sock, duration, verbose)

        return self.add_network_devices(announce_list)


def CommandLineTesting():
    COM_PORT = 5
    #com = RS232Connection(COM_PORT)
    #com = RS485Connection(COM_PORT)
    com = SocketConnection('10.0.10.67', 32019)

    while True:
        try:
            message = input('Command:')
            #com.delay = 0.15
            if not com.connection:
                print("Open communications Method")
                com.open()
            reply = com.query(message)
            print(reply)
            for item in reply:
                print(item)
        except KeyboardInterrupt:
            com.close()
            print('\nClosed communications method')
        except socket.timeout:
            continue


def SearchTesting():
    # Testing the Search class
    search = Search()  # Create class instance

    #print('Searching for Serial Devices...')
    # begin = time.time()
    # search.Serial()
    # end = time.time()
    # print(f'Serial search duration: {end - begin}\n')
    # print(f'Serial devices found:')
    # for device in search.serial_devices:
    #     print(device)

    print('Searching for Network Devices...')
    begin = time.time()
    readable = True
    hexdump = True
    filename = 'TVL_Data.txt'
    search.Network(solicitation=False, verbose=True)
    end = time.time()
    print(f'Network search duration: {end - begin}\n')
    print(f'Network devices found:')
    for device in search.network_devices:
        print(device)
    if readable:
        search.PrintAnnouncementsToConsole(hexdump)
        #search.SaveAnnouncementsToTxt(hexdump)


if __name__ == '__main__':
    print(dB_to_Hex(2, 31.5))

# Version Log
# ---------------------------------------
# R1 - 2022-03-16 - Import code from KaLNB_TCP_Serial_Communication_R6.py. Rename module to include all LNBs.
# R2 - 2022-03-23 - Filter responses based on type of connection - class methods to return interested values
# R3 - 2022-04-04 - filter, attenuation, and faults not dependent on comm type.
# R4 - 2022-04-12 - updated faults
# R5 - 2022-05-04 - RS232 and Socket updated with /r/n
# R6 - 2022-05-05 - Manage decode exceptions, lengthen read delay for longest command ("cfg", ~150ms)
# R7 - 2022-07-27 - Minor timing and filter changes
# R8 - 2022-07-27 - Minor filter changes
# R9 - 2022-09-22 - Filter changes
# R10 - 20221108 -  Move Filter to external database.
#                   RS485 dump everything after ACK into item in list.
#                   Breakup send/read/query. Attenuation function round up 0.25 and 0.75 for TriKa units
# R11 - 20230216 -  Add Search class to find serial and network devices
#                   Proper pythonic annotation
#                   Rename/split attenuation functions for ease of use
# R12 -2023-04-15 - Remove Serial Testing function, renamed testing functions
#                   Refactored TVL function to return list of readable lines for saveTVL() and TLV_Readable_Dump()
#                   Socket connection returns value with \r\n to preserve reply structures
#                   Socket class open() method now reopen socket properly
# R12c-2023-04-19 - Renamed variables in Search.add_network_devices() for clarity
#                   Add datetime to announcement tuple
#                   Refactored TLV_to_Readable to accept decode one announcement at a time,
#                       rather than loop through all announcements
#                   Added Search class methods PrintAnnouncementsToConsole() and SaveAnnouncementsToTxt()
