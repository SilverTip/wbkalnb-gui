#KALNB_TCP_Serial_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-25 Import code form Attenuation Sweep LNB ATE2 R2 - Serial and TCP Connection. Rewrite to class structure
#R2 - 2022-02-15 Consoliodate read/write methods to simple 'send' method which reads and writes. Carriage Return ('\r\n') on messages
#R3 - 2022-03-04 Minor bug fixes - added .decode() to socket reply. 


import socket
import serial
import sys
import time,sys,os

class SerialConn:
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 38400,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        message = bytes(str(message)+'\r\n','ascii')
        self.ser.write(message)
        time.sleep(0.1)
        character = ''
        reply = ''
        while(self.ser.in_waiting >= 1):
            character = self.ser.read(1)
            character = character.decode('ascii')
            if character == '\n':
                character = ' '
            reply += character   
        return reply

    def close(self):
        self.ser.close()


class SocketConn:
    def __init__(self, IP, Port):
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.server_address)

    def send(self,message):
        message = bytes(str(message)+'\r\n','ascii')
        self.sock.sendall(message)
        time.sleep(0.1)
        reply = self.sock.recv(1024).decode('ascii')
        return reply

    def close(self):
        self.sock.close()

if __name__ == '__main__':
    #Use this 'function' to troubleshoot connection and message parameters
    pass
    #print('Main not enabled')
