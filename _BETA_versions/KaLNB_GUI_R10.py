#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-31 - Version 1. "As Benjamin see's it"
#R2 - 2022-02-02 - Changing GUI to class structure to remove global variables
#R3 - 2022-03-01 - Added colour. Minor reformatting of variables
#R4 - 2022-03-04 - Properly formatting variables depending on unit type
#R5 - 2022-03-07 - Beta Release. Minor bug fixes
#R6 - 2022-03-14 - DKa and TriKa temporary solution - disabled IP, TriKa specific commands. Updated communication module to LNB_Communications_R1.py that includes TriKa command encoding/decoding
#R7 - 2022-04-08 - Generate GUI based on model number. New communication module LNB_Communication R2 with built in encoding/decoding and filtering of data.
#R8 - 2022-04-11 - New polling algorythm increase speed of GUI
#R9 - 2022-05-24 - Advanced features overhaul - scrollable rx commands and saved and ordered tx commands. Update to LNB_Communication_R6 with fault decoding. Offset TriKa band settings 
#R10 - 2022-07-27 - Minor improvements. Improved reply filtering. LNB_Comm_R8

import os, time, csv, sys
import tkinter as tk
from tkinter import ttk
import LNB_Communication_R8 as COMM
import tkinter.font as tkFont
from PIL import ImageTk, Image
import datetime
import base64

WIDTH = 475
HEIGHT = 900
BUTTONWIDTH = 10

BG1 = '#042B60'
BG2 = '#36A9E1'
FG1 = 'white'

PASSWORD = 'password' #Super secure password

buffer = {'CID':'', 'UID':'', 'firmware':'', 
        'DSA':'', 'band':'',
        'voltage':'', 'current': '', 'temperature':'', 'uptime':'',
        'IP_unit':'','IP_server':'','TCP_port':'', 'serial_port':'', 'serial_EN':False, 'TCP_EN':False, 'conn_EN':False,
        'rx':'', 'tx':'', 'error':'', 'first_loop': True}

MODEL_SETTINGS_LIST =  [
                        {'name': 'LNBKA2G-WS-44548','IP':False,'serial':1, 'DSA':1, 'fault':None, 
                        'bands':3, 'filter':1, 'band_offset': True,                     
                        'voltage':True, 'current': False, 'temperature':True, 'uptime':False},
                        {'name': 'LNBKAMS-XIWN60A','IP':False,'serial':1,'DSA':1, 'fault':None, 
                        'bands':2, 'filter':1, 'band_offset': True,                        
                        'voltage':True, 'current': False, 'temperature':True, 'uptime':False},
                        {'name': 'LNB855-900XWS60','IP':True,'serial':2, 'DSA':2, 'fault':None, 
                        'bands':False, 'filter':2, 'band_offset': False,                       
                        'voltage':True, 'current': True, 'temperature':True, 'uptime':True},
                        {'name': 'LNBKAWS-XWS60A', 'IP':False,'serial':1, 'DSA':2, 'fault':None, 
                        'bands':4, 'filter':3, 'band_offset': False,                      
                        'voltage':True, 'current': True, 'temperature':True, 'uptime':True}
                        ]
MODELS = []
for item in MODEL_SETTINGS_LIST:
    MODELS.append(item['name'])

DEFAULT_MODEL = None #"LNBKA2G-WS-44548"

class GUI(tk.Frame):

    def __init__(self, master):
        super().__init__(master)
        self.master.geometry(str(WIDTH)+'x'+str(HEIGHT))
        self.master.resizable(width=0, height=0)
        self.master.title(os.path.basename(__file__))

        #Tkinter Icon
        enocdedImage = 'iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAADs4SURBVHhe7X0HgFvFtba3uGLihjHGBowNhGZaQjGEUAKhGBywQw31kVCMIZQXAo/QbCA/LaHbuJf1uuLeO+69997W3pW0klZdW3z+883ea2Qxu95y79WVNN97HwZnV3M1d853zsycOVOHFBQU0hZKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABQU0hhKABTSBseYZfwPnaUaS2JZZgBjPk9vA+3x/wvaCUoAFNICUbbAwuJjdDBURrsCpbS5qJTWeEpoaWEJzXeW0Kz8Ypp1hHk4SrMOxvGAjJGf/z32Zw8x8RlHi2muo5iWuEpEOzv8pZQXLiMvP0PZMfvIgBIAhaQHzCneq8ezsLiM1ntLaOrRKOUcDNM3u8PUc1uQXt0QpKdWB6nrUj91XVBEXecwp3tO5NSTMPZnZzHxGQuL6JGVfnppfZA+2BqkgfvDNLsgStt9JRQuLfvF8+kRgtVQAqCQ1Aix9TgiZbQ3UEZbfKW0mr3tSva6K9n7rmQvvJK9Ov6ceShCfXaG6P/WB+jZVT7qtthHN833UseZHmo92UMNfyykhiNc1HCokxoOdFDDATHsJ2PBz/+u/xx+bzBzGH/GKBc1m+imC1kUbprnpSeX++j9jQEasjtEi49Ejj/XSkeJeOY9HJUURo9ZLgJKABRsDxgFqHv5WB7msHqhq5iGcUj+6Y4wvcYevTt73u6LfNR9dhF1n8WcW0RPz/FSl5le+v1UN/1mspsuYuM8e7ybThvLhj+mkOqMdFGdXGaOk+oMc1CdoTEcIiP/nP7v+s/h90B8Bn9W3dEsAvz5aOdybvPWaW7qNstLz831lj8bRwrdOfLAMw/az6LgLqEoTw+Ofz/te5sJJQAKtkaUrQDz5qNs6PDyO3juvsPLxLyaDWZmXpQ+3x6iv64M0B9+KqIO7HFbjyuk1sOd1Jq9c+u+TPbOrdgzN2ejbcKG2pg9dKMcF9UfzkbKhprJnr8OCBEA2XBPThYN2d+z5xefwZ+XwczmNtDOKdwe2j6Nn+GMQfxMeLb+TP5ZPPMTK/zUe1eYNruLxffC9zwQKiMfVhJNhBIABdsAQz2eCO8RIo/PK6avd0eo58Yg9VzDZIPpucxPPTiU78Le/Tc8Fz9rgpsasDeH4WWwsWWwgWX0d1LGQOZgJof3GWz8GWyMwtuP0Dw/OAoGzUQ0wF67xsTvg/g8TQjQTgaLgGibBUA8ywCQn4//Ds98BT//A/OK6F2eKvRczt9tfZD674uIxcrY/jAaSgAUEg4M7AjHvEXs7ZyRY1TAnq8gWM4VPJ/vtzdMf18XpFt/8tFFkzh8H8PkMPsiNuj2bMxnsLdvygbdUPfmulHrhq0bt07dSMe65fyxlpR9JtqLfYYYgcAzN8ktpDb8PS5kQbgIZDG7f5mPRh6IlvcF94mHIyGjAwIlAAoJRynPe3fzIF/gKKahPBfuuy1MfdkD9t0UpA/WBOjxRUV0Pc/fT+e5dCaMnL1oJnv3TA6jMwdpXlX36jAq3ch/ZI5j49MpM9ZE8Pjz8PPhWfmZ8fz4HuJ7gSxw7Se76dklPurLUU/fzSGxg+FmETASSgAULAOGLra84MUwtxfkvwiUlNH0gmJ6f0uQ7lzko04TPdSJDaITz6cvZ0/ZfqSTWrKR1BfhtGbo+p8j2YB0Dx/v2WXGZzfGRgd6VIDvxWzM3/F8/u9O/Gcn/pke6wIinwB9Vsx9Z4QUKAFQsAwI8/cGSmlZYQlN4jn9pP1RmrQnQj/uDtEb7Olv4zlws/GFlMVz5KzvCyjrBybPkzMHl8+VhWHASHTvHm9MdvLyNaUeGfD3xLpB5kAHZfXhfuhXQFfN8NB3W0Oiz35yFov+rC2UACiYAgxNjE94fJ1Odvnj8yL05mb29Dyfv3Oal+7kAf9HNvoreNC3YS9YDx6Q58J1YPDadtrxubzw7hplxpMK1EUM3xFRASId9AP3SQv+79/x/34nT4UQDbg4DEC/8v/XGEoAFEyBn+N8ePsV7O0XHSmmRYejNPFAhF5d46dr53gpmw06m+fv2ezps3VPP5QHOwwe22nCw8cZeip4+OowVgzGlq8TZPV1UDZHBBdP99CkA2FadLSY8iPIGKgZlAAo1BrwQCJxJYYw/hEHI/TK+gA9xaH9UzO99BCHsNdOclNrGLbu2YZo3p7DXTGfh9dLZQ9fY3KfIArCYudQjga4jx6a7qWnFvjEdAB9zv9fbSgBUKg1kKyCQzabikppk6uENjlLaNz+MHVf7acL2FM1H+qg5v0c1JS9fCM2+LowdggAkmbiF+2SZfEukeQ+Q4JRs/7crywIn24Jin7HNmF1oQRAodrAMBOE12Fu9ZVS7sEovbkpRG8u9dObPL9/er6Xrp3moVMRug5xUMYg5mD29PBg+sq97unTLbSvDfW+4j7MwBYoRwOdZ3pEv6/1lIr3UR0oAVCoFrCFB09ziD3+QX8pHWTjH82h/t9WBaj9NC+1Z8/UfqCT2nBY34T/PQueXizigWzw8R5fsWZE/6FPuX9bcl+j3wfuidDBQPXWA5QAKFQLMP4FPOf8eneYvlwfpC/XBunZxT76DRt/Js/fM9nTI0FHpLsKb8+saNtOsZYsF1SRQDTQQY9z5PXllpD2pqoGJQAKlQIJJ8HSY+Rjw/dFj9E2nud/vC1E18zz0lVs8FfluqgDs1kuD0Z4+xEc5scn5yhvbx5FJMB9nOukdhwFXDXJI94TkqyqAiUACpViX6CUZhcU08i9ERrJhv/VpiB1nl9E9Xn+jq27rL4FwvtkDNO8Peb1ak5vPVloRdIQvwe8J6zLVAVKABROALaTMM/X69tNPxql7jiIM81Dt7KBX8cGfhZCfXh79joiaQeGr3t7hKVKAKwnhBfvhKOAWzkiyDkYEUlCJ4MSAIXjQGopcs3nsMefcyhKcw5E6c21frp0hpeyhzop+9t8ysIBHOzd4yjtWB5wyT63h1idIFiaiMUSxlUZf/E72mclQgh5+pX9XT79bblf1CI8GZQApDng8WNTdQfsj9ADy/z0ABv9A1M8dM2EQmoBzw5PzyJwQsJO7GBPBsoM8gQB4O8jM3B8f5343jr1v4v/eV0IEiEAeB5+Tx35Od7nqQDeb2VQApDGgMc/ECyj1YUltDq/mGYciojadY154Dbm+WTjHwqo/mCeV2I1H1tOybiYh2cGZQaMxTOdiGj0RczKiFRlMf2p7Ge1n9E/+xeCEfN8ZpCNv94AB904zysiusqgBCDNAIcArwA62OOP4lD/pdUBemluEf0PE7XrxJweNe6QuCPm+NrAxeBKhFerDnWPLp5Te24YH74TG604d88eUpC/H+oJYPFMnMHHeQQQ/43tTJAF8DiHgPzzw5j4M/Z/w8/i9/TPwOfxf59QiQjRE4QBz4NI4fhzmkBury23ge3ayoIAJQBpBD3M381eYbenlBbyXP/FNQFqw2F+Gx6wZ/BgbYz8fDFIYwaqnT1/vIfXDR5E1ALDZ2LRsgELwK/YCFvwd2zFwtaG/zwbSTTMDvzdO7Chdhjmog4sEh345wT5dzrw7wvy53Xgz+3AfSL+1P8eP4Ofxe/h9/E5/Hnn8p9t+bNP57aacVuN+Wey0bf8O4J4Rv2Z9e8h+441IX92fRapexb5xMGsiqAEIA2A1w8GWAFmstF/vDVIH68I0P8u9dN10z3lNfRYAES6Ljw+wlYYvmxgJZqxHhPPCCNChALvDi+reXZRBxDfB7UB+fvVZyNrO95NV0310C0zPNRltpcem1dEz//ko38u8dFbbChv4U/uE8FlGpczV2hcCQboLY6YxJ/63+Nn8LP672qf99piHz05v4ju5bZumOahiyZ5qAk/s6gPyFGDeEY20gwhuvwdIASx37VW5M+CsHF/bfZVvBioBCDFgSQeV5QZKqNdvlJ6Z0uQLpnupkvYU53PHqs5G87xOSu8ku6NpIMqQdSfR/eSutdElMLPjYMxDXiwn8pG1ZSNqgWLWYsfmPgzh8lG15YN//dsjP8DI94QoP9uD1PO3jDNPBylda4obXaV/MzCCuiOo+xnYj5nhSNKo/eH6Qtu6+8caXVb4qeLZnjFuf4WPD3AMzbnP0/hqQW+w/H+HxPznWtDbucUFsORh8LaaPgllACkONZ6S2ngvggN3ByiLzcE6ba5XsrkgSbq6fFcVSTwwOPrK9eygZQI6p4eBoHn07y8fhxWEDX4WQCa889cPMFNt0zxUFc2sKdnMqdrfy4ooqfZO7+wLkAfbgvRkAMRmnI0Kq7sQrIMyo2fUIvfQPpZfHEN2SJuaywLzbd7IvT6hhA9zVHC07P5ufgZH5npoU6T3XQ6ohmIALZYURcBgozvHt8v1SJHGywAzy31aaPhl1ACkGLgcSfm+noiT182/hs41L2BjegaZmvMi2FIoL6dB48jHUAWMza8F/+O5yt/VhQLyWJvma0VxBDsx+TvhNt3HuKw+6NNQRrBXn1xXpQWs8GJP48W02JHsShDtoHFcDcbJA4yOSLHxH0DYe4s9JkZwHvAUekCbmt/sIy2seCscpfSYp6GLT7C5GeccjBC/7s2QJezEGSzGGf3RoEUFmcIgdh5qaUI8GdcwIKPZ5FBCUCKARdgLuHBPpU93dS9EfoLq38WG7moK8fGgzmymG/CuGINLtHU5/Pw9hAneEPMjXOd1IAHMMqFdfzRRTfxPP5u9vaCkz10N8/hX2Tv/j0b/k/OEtrPxo3BXsriJ/6Mo/DO3E/8h2mGHwu9HT0qOOF5+EFwZ+GkI1F6lacld88porv5+93CPAveH1EAT22EUNdYCArFjsQaj3w7UAlACiB2YG30logLLztP81BnNvDzMXAwkGBMMCoYmB5a2kIA8CxMfi5RBBPbazw1EYKFUmFYRZ/oods5ivn7Kj/14zB+CgubIEc3U/KKaSEbPi7QOMLhPBY6kwmYfiA6wJRkyiGOCPh7DdoRort56pLFQpjFEQG2Ko9P02ryzoY66KPt8lOCSgBSAGIAsREsYWPozS/6Mp771h1QQHW/x+ApnycLD1LbcNII6ot48PaIRHRvz8/YiNmWvX1HDluv5//teh7010/1UJclfvpga4jGs6fcFywVJxRjiV0u2D3/qyVe3WhAwPEd9O9zOFxK/+b3eP0Mr+iHc7E4iHeIvqrJusBIJ/2BBUUGJQBJCAxyPaTEXfOjD0XomRUBemaWl+5hz98Sho4FMswjMWhgbLKBYSWPz+mZPJix+CiSZ3hakok5L4vAWRPddA97+rfY0/ffFCrnzrCoNoS6d9v9peL2oFQHFg+xcNh/V5j6bwzRowuLyi9EwRQO71NETXH9WxlHu6gd97+s55QAJCECbAS7cR02z/W3uIrppTV+askvuiXP9ZqyUYktJX0BSd86ix8UVlBvG0YPap4MyTAteCC3H+6ki4cyWagunuSmzot89DF7+vmOKDmwUAdyWI9rs7GYhgW7JIvwawQIO5J3cC8i+qDPnhBdPNkt+qoRBAD9WJ2FWx4HSArCVerxUAKQJMCrE+R/7GRPiBTPt5f66e0lPurEXj8jl+fQSD3VF/kQKiZqjv8Lb4/kHCS+lD9fE/7fbpzpoR6Li+jD5X76cBlzY5B+4PnvPEeJWKU//n1jmE6I/d64NvzDTUHRV+04SkI/isiuqpEARHiwU5z7iIcSgCQBEnry2Bvm+cto4uEo/ZG95XlsWOexQTXDYMAcEV4fRpcojw+KqEMzfvZUuH77V/ycZ7AAnMkD90z+78tmeei19QGakRehXZ5S2uVm+krFAEXSUhguUOE4UIYN/YO+umOhj87kKK8eojz0sewdyDjURct5WhEPJQBJgi08AL7bE6Zv1gWpx1IftWVPoB88EbX39FVi2cu3gjB6PANSiRGFYOWaB+lp/Ew3TPXQ03O91IPn9z2W+On/Ngdp3JEoe/rSmLWMcirT/yXQJ3r//GdnWPTjmehz9upVXhTkMYJkpHgoAbApYl96kD0AFvqu5Rf/W/agF7C3bwiPD+rJPIn2+qM51GeDr8eevlG/Amr0HZP/GzsSSHSZwYNvVX4xreIQf723PBlHNidVqBwimYj78bo5XmrUp6A86pO9k3jyuPmWHUg8lADYFLELfWN3hKn7Ch/VZUPD3rg4ZoqwH4Zn9Txfb0+E+EyEokhWyXHSKRwBXD6+kO6f4qZuEzzUbb6PXue5/YS86PF77HQm65ZdogGHgP57e3OIuk3ylEdciLxONg7YWWBrMR5KAGwGGEX8Qt9tHOZdjAs2sPqLFw7Pr4d+VgmA3g7+lIT42YMK6KwpHnpxlZ+m7Q/T7H1Rmn2kPAUXeQpG3GSr8DNWuUto9v4oZWOrFwJcBQHotS2o/fbPUAJgM8gW+nCB5s8v02LDF+Q2ITjaNp4sxMfhIvzsaH7uUGlZ+VkEzVsp2zce6Ff08c0svqhzcNIFQX5v721RAmBLwD70+b5soU/6Qq2ixNvLQnyFxCB3d4TOmOAu3/6VvT+dLABYfI2HEgAbIHa+L13ok71Qiyjz9rIQXyExKCo+Rnct9tMpEIDKogAeR+9vVWsAtgG8viD/I3a+L13ok71QC4iwXubtaxPiu91uWrp0KeXm5tI333xD7733Hr344ov00EMP0R/+8Ae64oor6KKLLqILLriAOnToIIh/v/jii6ljx4504403Urdu3ah79+70/vvv0/fff08//vgjLVq0iPLz87VW0gfo+v77ItSOowCxIFvRbhCPpe94jMVDCUCCUFFij3ShzypKwv2aevtAIEBz586lL7/8kp5//nm66aabqFWrVlSnTh1TiTbuuOMO+uc//0kjRoygrVu3Umlp1W7JSVZgHHVe7Ct/fxUJAL/T8XkR7Td+hhKABKGixJ7yF6YZvqULffJwv6re3uv10tSpU+mNN96g6667jrKzs6UGmgiecsopdN9999GAAQNSMkrAK8EefyZuaqrojAA7l9Xu4vJfiIESAAuBo55BtqRKE3tkL88MGrC4t23bNurZsyddddVVlJGRITU+O/Kaa66hXr160YYNG7RvkvxA6bHmmAYgN0P2voc4KS/0y0hICYCFQA26sQcjlSf2yF6eCazp4p5u9JiPy4wr2QgxyMnJoWg0uXcyEJk9uSrAnh5OJG4cYVrAEQCcTzyUAJgMdLm+xTf0QEQU5aw0scciVmdxz+fziQW7VDF6Gc844wz64IMPknqKgMpIcCbldzbGvG8eW7gXQWL/SgDMRuwW31+X+8VZ+IQk9khC/pN5+507d9LLL79Mp556qtRoUpH16tWjZ555ho4cOaL1QvIAot0GTmVMnACMdlHbyR7tp06EEgATAKEV5H/EbvGJUtxaxpzVlIX8Mm9/jB96xowZdNddd0kNJF0I0fv000+Tbmpw+zyvdjI05v2PctGdC+WlwZUAmICKtvhOeCkWsyoLfPPnz6dOnTpJDSJdiRyEadOmaT1kf7y/LchTyjgB4Gkm7kSQQQmACah8i88iViPkX7Nmjdg7lxmAYjnvvffepFgfmO8sLr88Rc8HwMLyEIeoqSiDEgCDYKstPmZVQv4dO3bQAw88IB3wir/kmWeeKTIO7YwifsnidKCeDzCGxx//dxEGqARKAAyEXbb4wMpC/kgkIlJw69atKx3oihUzKyuLPv/8c7FWYkfgniNRL1Afb6NddPY4VASWP68SgFoAXhQeFZ4VSNgWXzVSeOHBLrzwQungVqw6u3btKrIf7YgsXKembwXy+Lttgbob0FTMPxhN6BZfVVN4n3vuOelgVqwZcWjJjtuFjcfzuNO3Agc76VOOSiuCEoBaAJdLHvKX0ktscB2GOOkUNsDyevy/NFIzebIVfuTot27dWjqIFWtH7BLk5eVpPW0PtJrM40LUiSykuv0ctKWo4sNQSgBqAdzn9tW6IHWa4RF3uKEo5i/SMM1gFVf4Mdd/9dVXpQNX0Tief/75dPjwYdHndkDbaZ5yAeBp6JU8PvQcDxmUANQACKv97P3/uzMsDB+0Mp23Kiv8e/bsEYd0ZANW0Xied955dOjQIW2EJBZtIAAYixyVvrbar/2tHEoAaoDDoTIaxcbfdYGv3PPDI1vh+TXGh/zxmDJlCjVt2lQ6UBXNI4qXHD16VHsLicNpU7QIgOf/k45WnsmoBKCagJdFUgUMv52+0h+femkGZWE/h/yxQOGLd955Rzo4Fa0hMikx9UokGmnHgnEACMeEK4MSgGpgxL4wNR3vpqZ9Y1f6raEs7I9FcXExPfLII9JBqWgtn3zyyYTmCWTC++fwOJnlpWhlCwAMJQCVILbr/rHcT4/N9VIGK2sG7tyXGKmZrCzsD4VC1LlzZ+lgtDNRNahNmzb029/+lm6//Xa6++676Z577hEHkfT6gKeffrr0d+1O1CpMBDBmEZUiAa2y7T8dSgCqgHc2hzjcd1JrhOFY7Kus+qqRrELYD6DenmwQ2oX169cXhTdeeOEF6tevH61evZocDgeVlWkZVCcBTuQdPHiQZs+eTR9++KHIy2/ZsqW0LbsQx4pxxsJq4Br1Ouygzs5x0j7JEe94KAGQINbzf7HST7fP9lLmEAdl4DJGK+b7Gk8W9judTuE9ZQMw0Tz33HPplVdeoXnz5onpidFAiI2SXm+99ZZoS/YMiWb79u0pGPzlIq2ZWOstoTrf59NrS3yVbv/pUAIQByzy6XiQw/4r2PBbIbd6BBs/PH9FRRcNpqwkdyyw74xS2bKBlyjCK8MgN2/ebOkcGG0tW7ZMrIHYrTbhv/71L+0prcHAfWFq9G0+ravg9F88lABUgOHbwtSeDTCrd375Nh/2VRNUtSce2G+2k9dDFeBhw4YlfPUbQBnwRx99lDIzM6XPajUxFdi1a5f2dObjqYVFdNPYQpEPUhUoAdAQO1u6eZ6XbmaDPzV2mw+nqxKQ0x8LhHSoz4fFMdlgs5q/+93vaPHixdrT2QuYHlx55ZXS57aaWKC1AhgfF/Dc//9tPHHcVAYlAHH4dFOQske5KLuPRam93EbGAMfxjMLKEnxKSkpssdp/ySWX0OTJk217JFYH1h5Q6NMOdxRMmjRJeyrzsDdQRu04ilwqqf9fEZQAxKDbTA/diCQKrPSjqILZq/0cZdTnF9bih3IB0I/wVoSXXnpJOrisYuPGjal3795Jd9MOVuOx3Sj7TlYRUzZs15qJsXlR6jLHS0cjVdtdAZQAaHhvQ4AaDXJQ/f6O8muwzS7ggQsc4ub8eyrJ2vrqq6+kA8sq3nbbbbR//37taZIPWDe57LLLpN/NKvbt21d7GnPw0fYQfcqs6vwfSHsBmHi0mF5c4KNbprJRDmXjN9vzYx2BxSVj6In37MP4KxKAiRMnJmx1G16/T58+tg/3q4KioiKRcCT7nlYQUyez+hGf+9amAC1yVW/LNe0F4I0NQTpziJOawvhxlh/Gb+Z5fiwossicye3o23yVYd26ddSoUSPpgDKbOOuOVfVUQjgcFjcMy76vFZwzZ472JMaitOwYfbQtKK4Lrw7SWgB6LffTzTO9IrU3QxRShOc3yfuLHQT2/MNcYr5/y7wiMeevDJgzJqp8FxYb7VryqrbAFeWJyqFAFqMZCJeW0dQj0Qoq/1WMtBQAR7CMXlkfoIvY6E/DSj/m/BYs+KFYI4y/U24hvbP15AtCf//736WDyGwieaWqabrJCqQWJ2JhEFM5M/ICAsVldDRc/XeWdgIwhef8/TaE6Hr2/Mi2E0k+Zi74xW3z4e8qW+zTgVBRNoDMJCreDh48WHuC1Mfy5csTskUIYTcaoZKyant/IG0EACm+xTxP6r4uQJ2GF1JLrMIjxdfM3P6Ybb7nVvlpk6ukSsbv8Xiobdu20sFjFpGxNmHCBO0J0geffPKJtD/MJO4XMHoxsLSGn5c2ArCYjW/ynghdgfp93xeIubipST4QmCFOOn9cIT013Us5B6ueJvv4449LB45ZbNCggbgPMB2BnIYbbrhB2i9mcu3atdoTJBYpLwBIjyzlqdGrG4N01zg3tYDHz+Gw3yzPH7PNl80hf5dFPlp0OFolzw+MHTtWOmDMIkLgmTNnaq2nJ3BDktVTgV69emmtJxYpLwBbfaXCAC/nOT9q9sMwTfX8Mdt8d4130392hYUAQYhOBpyRb9GihXTAmMVBgwZprac3rL4zAQeo7ICUFQDYG4zuSzbAp6Z76HQs9HFILkJzmeHWlpJtPkw51nur5vmBHj16SAeLWbT6qKqdgV0BrIPI+skMYjcAgp9opKwAoHIvFt26LPJTix/yKRs3pop9/jjDNYraNl+zEa7j23xYdKxqWqbVYSjOzqdCdp+RePnll6V9ZRaHDh2qtZw4pJwAYEhjXI/lsP+txT66YKKHMgagbr+Jxs/EliKIf68J7r//fukgMYO4LwAZcQonAtd8YStU1mdm8Omnn9ZaThxSTgAsv65L2+e/eopbCE5NgAs7ZQPEDDZs2JC2bdumtawQDxQmlfWbGUQ5t0Qj5QQg9rquzP4Oc8/0S/b5qwuE4VgQkg0QM5ioarXJgtzcXGm/mUGIcaIzLlNGAJDoo1/XdRXPw1vkOs29rkuyz1+Vlf54jB49Wjo4zCC8m5r3Vw4U8cQJSFn/mUEry4XJkDICcMJ1XX1NvK6rlvv8sUC5a1wnJRsYRhPbi3a4tioZYGUi1rhx47RWE4OUEAD9uq5bx7vNv66rFvv88UCNfNmgMIOo5KNQNQwZMkTah2awZ8+eWquJQUoIwNyjxdRzfZCy+xVQpri1xyTPX8t9/lggFL/00kulg8JoduzYUdQTVKgadu/eLe1HM/jAAw9orSYGSS0A8Lo4BPHgQh/dgFp+ONOPeb/MgGvLWu7zx8PK035z587VWlWoCiDOrVq1kval0cSNSYlEUgvApqJSWpMfpcZslA0GsOcfbYLxo04Af36zEYV0VY6LHlzhF4uNtQUKQ8gGhNG87777tBYVqoOuXbtK+9NonnfeeVqLiUFSCgDMD97/A/bCL8/3ir14UWDT8HP9CPmdlMnicuU0L321JkjDD1Z+33pVgJVfK2r8Iakl0avMyQqrjgljcTaRSEoBKIweo9089756ThG1HcieH2G/0RV9EPKPdFGTHKdIKPqf1QE65Cul/GqUXK4IVqWcPvbYY1qLCtWFVacycYNRInMBkk4A4P0XOIrp45UBajHOLbLwyo3fYAGIy/DLYc+PLfTaBv+oTHvqqadKB4PR3Lhxo9aqQnWxatUqaZ+aQRSASRSSSgBgfC72wJ9sC9KlPB+vh4o+SMgxOs1XkuF3MGSMSn/55ZfSQWA0cc++Qs2Bm5dl/WoG9+zZo7VqPZJKALDhNmhHmLrMQz0/HPAxYdGvggy/muzzxwOryyi1LRsERtOud/YlC/CurCrHvnr1aq1V65E0AoDttgD/43eTPHQWDB/e38g0XwMz/CoCavzLBoDRvP7667UWFWoDq0qyz58/X2vReiSNACwtLKEJe8OUNdhJmbhOC3N+kZxjEA3M8KsIuDtfNgCMJq7qVqg9rr76amn/Gs1ERmtJIQDw/qKm33g2UqT5Gnm23+AMv4qAkNKKvH8sMOJAi0LtYdUNQitXrtRatB62F4Awu+ClR4vFffl1e+cbn+xjcIZfRcANtbKXbzT/+te/ai0q1BZW3SOIqWGiYGsBQPiNq47/yl65pajp5zB23o9pBBt+Zl8HXTHNIy4Mmees3uWKVcWbb74pfflGc8mSJVqLCrWFVdmaW7Zs0Vq0HrYWgL2BMpqVF6WWPO+va3RNP2wdjnJRNnv/i4Y46ZlVfnFlWBGOFhoMhP/t27eXvnwj+etf/1q0pWAMHnzwQWk/G81EZmvaVgAwjr/nuXj3n4ooYzCy/WD8RgkAfw6LScZAJ53KkUWvZX4aeShSXk+wvHlDgW0e2Ys3mh9++KHWooIReOihh6T9bDT379+vtWg9bCsAR9gbd17ko3YI+3HG38hkH0QS7P3PYAG4dKaHdrpLKa8GFytWFW+88Yb0xRvNzZs3ay0qGAEkU8n62WiiGGmiYEsBgCl+uylI50xwi4M4hh7ywWfh2PBwJ/1tThG9ye1grcHMwBmhuezFG8lzzz1Xhf8G4+abb5b2tdEMBAJai9bDdgIQZev3l5TR1RymN0JdP3h/mSHXlGNcVK+/gxqxACzIi9I6j7mFMqDuspduNHHASMFY/OY3v5H2tZFs0qSJ1lpiYDsBWMsGOWZniLIGOigDd/jJjLimhJiw9792nJu6zisif2mZ4dt98RgxYoT0xRvN2bNnay0qGAUrIreLLrpIay0xsJUAYAH+v7tCdBvm6Ej4MfSIbyFlDHGIsmHvrg+K3YUSC0JmK+6c+9WvfiUKjCoYizZt2kj720jedtttWmuJga0EYH5BMd23sIiyv9cSfoxK9dUE5QyOAG7mz5zrKBZiYwWs8CJ//vOftdYUjALWU1C3X9bfRvKJJ57QWkwMbCMACMUfXean87FIZ3R1n1GFlNWngG6a46XhO8J0wKCjvSeDVfP/b7/9VmtRwSi4XOyAJH1tNHE+JJGwhQDA+NexV24y1k31cNDHYONHqu8VLCpvbg6SN3qMDCjpVyVYdcvM+vXrtRYVjMLatWulfW00Ey3eCRcA2GKw7Bi9ttBHGbkI1Y1c9ce830mZ/JlfrArQdJ5iWGT7As8++6z0pRtJzP9LS409uKRANGHCBGl/G820vxjEz5PxTe4SOpsNVZzxN8r7a6m+zfhz2/G8/0BRKbnY+1sJK4p/3HnnnVprCkbim2++kfa30UTpsUQioQIAc9xYVEI9V/rLE34Qrhu18MeflTHQQTdN9dA/1gRMT/aJh1Xzf5X+aw7+8Y9/SPvbaPr9fq3FxCChAgDvP/xAhC7Bfr/w/hJDrgmx6j/CRU1/cNCb6wK0gSMMqzF58mTpCzeaCxYs0FpUMBJWHARq166d1lrikFABmMVz8meX+Mrr+8FojfL+EBQWgMeneWnykajpyT4y9OrVS/rSjSRKSqviH+bgsssuk/a5kcRtzYlGwgQA+/Avrw/QBaK+HxuszJBrxELK7F9A2TwF+OlQ1LBqvtVFt27dpC/dSKJmnYLxwD2K9erVk/a5kXz99de1FhOHhAnAbPb+V83wsLHy3B/Hc6XGXE1iAZG9/6WjC+mOn3yG1/SrDqwo//Xwww9rrSkYie3bt0v722jaoXZjQgQAIfkDK/x0Gry/kdt+SPj5oYBeXhmg8YcTlxqLyz9kL9xo/vvf/9ZaVDASY8aMkfa30dy0aZPWYuKQEAFY7iqhX43nUB2FPoza9sO5gVwXXcuiMj6vmMKJmPhrWLZsmfSFG00sNCoYj7ffflva30ayfv36triy3XIBQEj+/Eo/ZYu5v1Hev1CcHMwc6qTeG4K03Z+Yeb+O/v37S1+60dy7d6/WooKRsKIQyG9/+1uttcTCcgHY7i2lM4TBsvEbte03ykWNWAB+zdHE0WAZhRLo/YHXXntN+tKNJG6tSeSlkqkKHAI688wzpX1uJO1SvdlSAYBZvr8uWG78htX2ZzEZ7KRzJxTSe2sCopZgonHHHXdIX7qRvPzyy7XWFIwE6vPJ+ttofvfdd1qLiYWlApAfKqMLx2Gl3kDvP6aQmg900J0Li2ibwZd51BRI8JC9dCPZpUsXrTUFIzF8+HBpfxvNpUuXai0mFpYKQJ/tYTFPN8z7YwFxsIO6TPfQFzvDCdvyiwXC8rp160pfupHs0aOH1qKCkXjhhRek/W0kMX2zSwEXywQgwvPya6d4jPP+yBoc7aL6vfOp784QbfXZw/vn5+dLX7rR/Oyzz7QWFYxEx44dpf1tJO10gMsyAZhwMEpZ8P64ftuIlF+kDuc46U8T3LSpqMQW3h+w6g6AUaNGaS0qGAWPx0MZGRnS/jaSdhJvSwQAi/J3zC4qz9E3Yt8fAsLTiKyBBTRjX4TcVlX4qAKsOkeOXAMFYzFt2jRpXxtN3BNpF1giAEtcJZSNWvyoymuE90fSD08lbmTvX1Jm7THfkwGru7KXbjQPHz6stahgFKw4AtysWTNbbd+aLgD4qs+uDZTX+UPYLjPo6nK4izKHuWjIjnB5IzYCarzJXrzRDIft992THSjRLetrI9m1a1etNXvAdAHAaby2k2G0LAAyY64u2ftn8VTiskkeckfslwjz+OOPS1+8kcQug7oFyFjs3r1b2tdG0y77/zpMF4DcQxGqj7Rfo274Ye9/ymgXfbI5ZKvQX8ctt9wiffFGsnnz5lprCkbhyy+/lPa10dy2bZvWoj1gqgAUs4P+22p/+d6/EYt/Y9xUd5iTLpnpob1+exbCtKKQxDnnnKO1pmAUcEGHrK+NJFKM7Ra5mSoAjugx+o3Y+0f4b4AAsPdvPqaQ/m9LyDbbfvGwohAo9qoVjAOOb1uRvPXYY49pLdoHpgoA7vlrBu+PW35kBl0dcgSRNRje30sbi+xbBvvss8+Wvnwjef3112utKRgB5FTI+tloDho0SGvRPjBVAD7ZEKQMXPRhhPcf6aKmI1z02sby67ztitNPP1368o3kjTfeqLWmYASsOP4LHjhwQGvRPjBNAJD8gyu+jVn848/hSOKCqR5anoAKv9UBLuqQvXwjec0112itKdQWR48epaysLGk/G8lLLrlEa9FeME0AfnIVU5ZR9f54ClEvxynyCRJ81P+ksKKY5BVXXKG1plBbfP7559I+NpqffPKJ1qK9YJoAPKMn/8gMujpE5mCui5qPKxSFRO0MZHjJXr7RTPSd8qkCrMhfeuml0j42kjhfYNfMTVMEANt/IvkHN/3IjLq6HOakPy4qSmidv6ogFApJB4DRRMVhhdoDOfmy/jWat99+u9ai/WCKAHiKj1Ej1PsbY8D8f3QhZQ530oD9Ee3T7Qtc8yQbAEazTZs2WosKtcHzzz8v7V+jaYfy3xXBFAFYXlhCmeKabwMEgIXkvEluOhq2X9pvPBBSWnGcFHvW6kbg2qGgoIAaNGgg7V8jecopp1AgENBatR9MEYB/bgiWz/9rm/3Hv496f2/z59l87e84mjRpIh0IRjMvL09rUaEmeOedd6T9ajSffPJJrUV7wnABwB79Bcj+Q+EPmVFXhxz+t+T5/xab1PqrCqxIBAKXL1+utahQXWCqhmO5sn41mnPnztVatScMFwDc+JuJsl9GHP1l439mgc/WiT/xsGJVGRw9erTWokJ1YdXBn7POOsv2pdsNF4C8YCnV4bC99nX/CsWtwUvy7b31F48bbrhBOhiMJvavFaoPFOO0KkpDbQi7w3ABmJgXpTpDOAKorQCMdtGZw522T/yJx1133SUdDEbzqaee0lpUqA6++OILaX+awa1bt2qt2heGC0B3JAAZceXXCBfdMserfWry4KGHHpIOBqOprgavPpxOp2WLtIgEkwGGC8AVs7zGJADx/P/drSHtU5MHVtSV01lYWKi1qlAVvPjii9J+NIOzZ8/WWrU3DBeAVhPYeGu7ADiW5/9DnbTAmVzzf2DAgAHSAWEGp0+frrWqcDJs2bLFkkM/IE5rJkvJNkMFoLjsWPmtv7Xd/x9dSG1ZRHwlSbYAwFiyZIl0UJhB7GUrnBwwxj/+8Y/SPjSD8+fP11q2PwwVAHe0rPzmn9qkAGPxkEXk4UW+pEn+iQXCctmgMIMoP6ZwcvTt21faf2YQNSGTCYYKAOr0lZf/roUAjGEOd9IPe+yf+18RrCgKonPz5s1aqwoyoNov0nFlfWcGFy5cqLWcHDBUAFa4isXiXa2mAPhd/ozVHnsX/qgMv//976WDwwy+/fbbWqsK8SgpKaFOnTpJ+80MorBossFQAZhyJCoKd0oNu6oc66K6OU4qtNF1X9XFs88+Kx0gZrB9+/ZJs+BkNT766CNpn5lFrP8kGwwVgMH7wrXPAeDpQ/sJ7qSc/+uwcicAnDVrltaygg5sw2VnZ0v7ywza6cbf6sBQAfh6FwsAdgFkhl1Vjiqkexb6tE9MThw6dEg6SMwiqgSrKOBnIAPPqoQfnStWrNBaTy4YKgD/3RmqvQDw7/9rc1D7xOTFxRdfLB0oZnHmzJlay+kNh8NB5557rrSPzGIyp2UbKgB99hgwBeDfzzmQvDsAOl555RXpYDGL1113XdpHAbgwFdGQrH/MIm77cbvd2hMkHwwVgJz9BgjAcCetKky+DMB4WHXXfCxzc3O11tMPkUjEsvr+sZwyZYr2BMkJQwVg/OFI7XcBhrkoL5T85a6CwaAlJcJj2aJFC8rPz9eeIH0Az9+5c2dpn5jJJ554QnuC5IWhArDAES3PBJQZdlU51EWRZKoAUgmsTD/ViePIdi9CYSR8Ph/deuut0r4wk6gpkMyhvw5DBWCnv6Q8Eaiml4EgCYgFhJJ6E/BnoGqPbPCYzc8++0x7gtQGbvW5+uqrpX1gJjMzM2nx4sXaUyQ3DBUAf0kZZeIm4JpWA0YKcS4EIDVQXFxMrVu3lg4iM4nKxD/++KP2FKmJVatWifLosu9vNnv27Kk9RfLDUAE4xv/XCDcB11gAOALIdWqflhp47733pIPIbNavX58WLVqkPUVqYejQoeL7yb632bz55ptTqiS7oQIAtJqkGbLMwE9G7SBQKgFXQll1Dj2eqHy7adMm7UmSH1jpf/XVV6Xf1Qqec845dOTIEe1pUgOGC8Cfl/u1rcAaiACOAvMUIlXWAHR069ZNOqCsIDLikul8ekXANV5WVVyWETss27dv154mdWC4AIw6FKE6Ax3lC3oyI6+MmgBgKpFKWLBggXRQWUXcJJSTk6M9TXIBVXzffffdhEVRYKNGjVL2HgbDBeBoqJTqD2YBqGlNgBwXOaOpd+3VvffeKx1cVvKNN94Qe+bJAhzoQdET2XexihCeqVOnak+UejBcAIIlx+h0hP81vRlouJOWu5M/EzAeu3btsjwxSEacUbD7wZV169YlJIdCxkGDBmlPlZowXACQw3PrT0XlCUE1uRtghJO+2ZM8Xqo6ePPNN6WDzGpiH/v1118XZbLtBBj+Y489ZskFq1Xhxx9/rD1Z6sJwAQC+3h2mDNwOXJPdgFEu6rrcr31SagFZa4nIC6iIKJWFMuaJTB9GrsTIkSMtu1GpquzRo0daHK4yRQD2BsuoNTICMQ2obhQwupBaT/FQcYpms2IPWzbgEsmGDRvS008/LY4Uo4yW2YDRz5kzh1566SVbCaJO9EW6XL9uigBgGtB9oa/8jsDqRgH88xkjXZQfTk0FQJ7+3XffLR14dmDLli3p+eefp7Fjx4rCJkYAorJx40YaOHAgPfroo9S0aVNp23Zgr1690upYtSkCAKwoKKaG/Qs4CqjmbgCyCIc6aU0KLgTqwNy7bdu20gFoN+K8+3333Scuuvz666+FMKD2HRY1Dxw4IBKdDh48SHv37qX169eLy0pQEu3DDz8UQoI6BYgwZJ9tJ2KBNlm3SmsD0wQAl3p2neYR+/qY10uNXUbtQNAnG5O/KlBlwGESK2vWKVZMZEwiVyMdYZoAALMORih7iJMyhjnYuKs6FeCfY8G4doKbkvBioGqhd+/e0gGpaB1RVTkVM/yqClMFoISn8TdN9VDjXI4CkB5c5QXBQsoa6KQFR1J3GqADYbJsYCqaT0xPUEMwnWGqAAA5uyN07iQ3ZfblKGA0e/dxMoOXcJiT/jLfJ6YSqQysiD/44IPSAapoHh955BEKhZLv9mmjYboAeKPH6M/L/HQJEoNQMRgiUJVIgKcBTYa5aL0reW8IqiqwSv7www9LB6qisTzttNNo1KhRWs8rmC4AcOBDD0TpgxV+ymSvnjFE3xo8yZoAtgP5Z19f6hPbiikeCAgRgFeSDVpFY4hTmQUFBVqPKwCmCwBwJFxG6wuLqR0b9em4PBRbg1WJBHJddE6uk/YVlVJRqq8IMiACf/nLX6SDV7HmVF6/YlgiADBdX0kZ/e/KAHWd5RW7AiISGHWSKIBFInOwgz5bFaBVnpKUjwIAZKC9/PLL0oGsWH0qr185LBEAoOTYMY4CSqj3jhA1HeqgU/uzCGBnoLJMQUQII5x0eY6LftgbJk/0WMovCuoYMWKEpddapxqV168aLBMAAMa7hj35Y/O8dM9UD2UjQQhTAggBpgTxAoAdAxaIrL4F9JdFRTRsd5i8qXpIQALccWf1FWPJzgYNGogDTi5X6hSXNROWCgDgYi++4EiUBu0KU+PxbsruXUCZgxzs6WMEIHarEFHAcBd1GOkSOQW7/CUiQShNAgHy+/0if1422BV/Jgp34Fp2pCYrVB2WCwAMFwa8K1hGXZb66Y9s5L9G/j+igBO2CjVBEGJQSJkDnZTN0ULfbUGaeaSYjqboYSEZcDhl3Lhxll96mQyE4eOGnh07dmi9pVAdWC4AOrCqP4kNeRxHAn9d5qOsURzqf5/Phu6gDFwvJqYETD0agDAMd9IfJrjp/gVF9JOzREwpUuQSoSoB5bxwyAY16mTGkE7E4Z3nnntOHEJSqDkSJgCw2zBbcJiFYNiBCF0zu4iuGeakCznU/xW2CRERwOixU6BHBKNcVL+fgxrw3/97c4hWHC2mTUWlFEqXlUENOKabrtOCVq1aiTm+CvWNQcIEIBYb2Ii/3x2h79cF6JXlfrqQ5/qZQ5yU2d9BmYOclIGpARYMQRQaYQG4fbqXXphbRB9tD1EeTwcQCaRDwlAs1q5dK+6mT9QlGVYR3w/p0ijOaUXBknSCLQQgwB4cc/qjgTKaeiRKD7EIXDCmkC5gY2/HbMbRQBaiAZ08FWg6yEFncDRw4zwvzTwcpR3uEtrpLyV38TFKn9WBcuBAC+rXJUuNgaqyU6dO1KdPn5S4hNOusIUAwGsL8j/2BEpp0P4Ivbs6QO8u89MLC4uo0zQPNURqcA5HA1gjGMCRQZ8Cyvwun87kqKDH/CL6YKmfPtwWpmUsBLhdGJ8lqH12OgDeccyYMeKe/GQowiFju3bt6O2331aLehbBFgIQiyBHAweCZbTNU0rbCktoWl6EeqzxU4fpHjqdjf10nhaAzVgIGjObMDsMKKALOSLoOMNL3+4Mi9ThAo4GCjiiKODIwstRAdYJ0mnBECfdEDK/8MILdNZZZ0mNzQ48//zz6ZlnnqEhQ4bQvn37tKdXsAq2EwDYqD6fBw+FS8VtQ69sCNKzS3z07KwienZ2EXWZ4aFrp7ip3UgXNerH0cD3+VRvqJMemualr5b46QeOHn5YE6QfdodpdkExbfeXpN1ioQ5sI6ImH64Nx1kDXLGVqGpEHTt2pBdffFFk6aXaPXvJCNsJQDxgtPs5IljFof0yRzEtO1LOvjtD9DJHBn+YWyQuIsFtRA1659MF3+bT774toJt4evD7IU66cY6X3tkcoklHIhwNlIpqw8hDgBZAYNJTEsov2sQiIi6+eOWVV6hz5850zTXXiBC8NinIzZs3pyuvvJL+9Kc/iTMNn3/+uZiW4DKSwsJCrXUFu8D2AgADhaHCYGO5qYjnu3lRMe9/bGWAusznqGCCm+7LcdH9g5yCfxpdSDfN9VL3tQH6YmeQph2N0gIWEaQj7/CV0uFQGRWxIpRhsUDhBAQCARGSw3BR6HPy5Mk0YcIEURQUnDhxIk2bNk1c34V6elu2bBFZiwrJBdsLQEXAav/eYCmt9ZTSXDbq6SwG0/dFaPrOME1nUZi+PUyTdoXoPzvC1GtrkN7YGKTXeRrx0roAfcZ/N+JglBY6S2gvUouhMAoKaYikFQCYrB4ZIKQXRHgfw1BpGa1ngRhzKEKfsiC8xgLwt9UBjhpCNHR/hOYVlNBunxIAhfRF0gpAVVDKoX1B5Bht43B/iauEZuYX06Qj5Z5/g7eU9gXKyB1VUwCF9EVKCwCAyCDMHj7A/+JjFvHUAf+OxUW2fRFBKCikK1JeAGIBW9epoKCQZgKgoKBwIpQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikMZQAKCikLYj+Pwk7SiFpR3rfAAAAAElFTkSuQmCC'
        decodedImage = base64.b64decode(enocdedImage)
        img = tk.PhotoImage(data=decodedImage)
        self.master.wm_iconphoto(True, img)

        self.com = None
        self.PERIOD = 1 #seconds
        self.font = tkFont.Font(family="Helvetica",size=10)

        self.master.configure(bg=BG1)

        #Variables
        self.TCP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.Advanced_EN = tk.IntVar()
        self.Band_text = tk.StringVar()
        self.voltage_text = tk.StringVar()
        self.current_text = tk.StringVar()
        self.temperature_text = tk.StringVar()
        self.uptime_text = tk.StringVar()
        self.Error_text = tk.StringVar()
        self.DSA_text = tk.StringVar()
        self.CID_text = tk.StringVar()
        self.UID_text = tk.StringVar()
        self.Firmware_text = tk.StringVar()
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        self.clock_text = tk.StringVar()
        self.clock_text.set(updatetime())

        #DiplayFrame
        self.DisplayFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
        self.logo = tk.PhotoImage(file=get_path('orblogob2.png'))
        self.logoFrame = tk.Label(self.DisplayFrame, image=self.logo,bg=BG1).pack(side='left',anchor='nw')#grid(row=1,column=1,sticky='W')
        self.clock = tk.Label(self.DisplayFrame,font=self.font,textvariable=self.clock_text,bg=BG1,fg=FG1).pack(side='top', anchor='ne')#grid(row=1,column=2,sticky='E')
        #self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text,font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2,sticky='W',columnspan=2)
        self.statusLightBackground = tk.Canvas(self.DisplayFrame,bg=BG1,width=30,height=30,highlightbackground=BG1)
        self.statusLightBackground.pack(side='right',anchor='e')#grid(row=1,column=4,sticky='E')
        self.statusLight = self.statusLightBackground.create_oval(3,3,30,30,fill='red')
        tk.Label(self.DisplayFrame,font=self.font,text='Connection Status:',bg=BG1,fg=FG1).pack(side='right',anchor='e')#grid(row=1,column=3,sticky='E')
        self.DisplayFrame.pack(side='top',anchor='e',fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
        
        #ModelFrame
        self.ModelFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.ModelFrame,text="Select Model:",font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='W')
        self.Model = ttk.Combobox(self.ModelFrame,value=MODELS) ##Makes the menu
        self.Model.grid(row=1,column=2,sticky='W',ipadx=50) ## Positions the Menu
        self.ModelButton = tk.Button(self.ModelFrame,text='Enter',font=self.font,bg=BG1,fg=FG1,activebackground=BG1,activeforeground=FG1,disabledforeground=FG1,command=self.PackGUI,width=BUTTONWIDTH,state='normal')
        self.ModelButton.grid(row=1,column=3,padx=5)
        
        #Check if model has been pre-selected
        if(DEFAULT_MODEL):    
            self.PackGUI()
        else:
            self.ModelFrame.pack() 

    def PackGUI(self):
        #Used to pack GUI based on feature list
        if(DEFAULT_MODEL):
            model_name = DEFAULT_MODEL
        else:
            model_name = self.Model.get()
            self.ModelFrame.pack_forget()
        
        global model 
        model = None
        for settings in MODEL_SETTINGS_LIST:
            if(settings['name'] == model_name):
                model = settings
        if(model == None):
            return

        #Connection Frame
        self.Serial_EN.set(True)
        self.ConnectionFrame = tk.LabelFrame(self.master,text="Communication Menu",width=WIDTH,font=self.font,bg=BG1,fg=FG1)
        self.spacer = tk.Label(self.ConnectionFrame,font=self.font,bg=BG1,fg=FG1)
        self.SerialPort = tk.Entry(self.ConnectionFrame,state='normal',disabledbackground=BG1)
        if(model['serial']): 
            self.SerialCheckbutton = tk.Checkbutton(self.ConnectionFrame,font=self.font,text='Serial Enable',variable=self.Serial_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method=0: self.UpdateCommunication(method))          
            tk.Label(self.ConnectionFrame,font=self.font,text='COM Port:',bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')           
            self.SerialPort.grid(row=2,column=2,sticky='W')
            self.spacer['text'] = '                                         '
        self.IP = tk.Entry(self.ConnectionFrame,state='disabled',disabledbackground=BG1)
        self.TCPPort = tk.Entry(self.ConnectionFrame,state='disabled',disabledbackground=BG1)
        if(model['IP']):
            self.IPCheckbutton = tk.Checkbutton(self.ConnectionFrame,font=self.font,text='IP Enable',variable=self.TCP_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method=1: self.UpdateCommunication(method))                
            tk.Label(self.ConnectionFrame,font=self.font,text='Unit IP:',bg=BG1,fg=FG1).grid(row=2,column=3,sticky='E')                  
            self.IP.grid(row=2,column=4,sticky='W')  
            tk.Label(self.ConnectionFrame,font=self.font,text='Port',bg=BG1,fg=FG1).grid(row=3,column=3,sticky='E')                   
            self.TCPPort.grid(row=3,column=4,sticky='W')
        if(model['IP'] and model['serial']):
            self.SerialCheckbutton.grid(row=1,column=1)
            self.IPCheckbutton.grid(row=1,column=3)
            self.spacer['text'] = '                        '
        
        self.spacer.grid(row=2,column=3,sticky='E')

        self.connectButton = tk.Button(self.ConnectionFrame,font=self.font,text='Connect',bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,command=(lambda model=model:self.Connect(model)),width=BUTTONWIDTH)
        self.connectButton.grid(row=4,columnspan=4,column=4,sticky='E')
        self.ConnectionFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)      

        #Band Select Frame

        global bands
        bands = [0, 1, 2, 3]

        if(model['band_offset']):
           bands = [x+1 for x in bands]
                
        self.BandSelectFrame = tk.Label(self.master,bg=BG1)
        self.Band0 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[0]),command=lambda band=bands[0]: self.UpdateBand(band),width=BUTTONWIDTH,bd=6,state='disabled')
        self.Band1 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[1]),command=lambda band=bands[1]: self.UpdateBand(band),width=BUTTONWIDTH,bd=6,state='disabled')
        self.Band2 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[2]),command=lambda band=bands[2]: self.UpdateBand(band),width=BUTTONWIDTH,bd=6,state='disabled')
        self.Band3 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[3]),command=lambda band=bands[3]: self.UpdateBand(band),width=BUTTONWIDTH,bd=6,state='disabled')
        bandpadx = 10              
        if(model['bands'] > 3):
            bandpadx = 9   
            self.Band3.grid(row=1,column=4,padx=bandpadx,pady=5)
        if(model['bands'] > 2):
            self.Band2.grid(row=1,column=3,padx=bandpadx,pady=5)          
        self.Band0.grid(row=1,column=1,padx=bandpadx,pady=5)
        self.Band1.grid(row=1,column=2,padx=bandpadx,pady=5)
        if(model['bands']):
            self.BandSelectFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
            #self.BandSelectFrame.place(anchor='center')
        

        #DSA Scale Frame
        if(model['DSA']):
            self.ScaleFrame = tk.LabelFrame(self.master,text='DSA Setting (dB)',font=self.font,bg=BG1,fg=FG1)
            if(model['DSA'] == 1):
                self.DSAScale = tk.Scale(self.ScaleFrame,from_='0.5',to='31.5',resolution=0.5,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')
            else:
                self.DSAScale = tk.Scale(self.ScaleFrame,digits=4,from_='0.5',to='31.5',resolution=0.25,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')      
            self.DSAScale.bind("<ButtonRelease-1>", self.updateDSA)
            self.ScaleFrame.grid_propagate(0)
            self.DSAScale.pack(fill='both',padx=5,pady=5)
            self.ScaleFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
        

        #Status Frame
        self.StatusFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        width = 50
        state = 'normal'       
        self.Band_entry = tk.Entry(self.StatusFrame,textvariable=self.Band_text,state=state,width=width)
        if(model['bands'] > 1):
            tk.Label(self.StatusFrame,text='Band:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
            self.Band_entry.grid(row=1,column=2,sticky='W')
        self.DSA_entry = tk.Entry(self.StatusFrame,textvariable=self.DSA_text,state=state,width=width)
        if(model['DSA']): 
            tk.Label(self.StatusFrame,text='DSA:',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')                
            self.DSA_entry.grid(row=2,column=2,sticky='W')
        self.voltage_entry = tk.Entry(self.StatusFrame,textvariable=self.voltage_text,state=state,width=width)
        if(model['voltage']):
            tk.Label(self.StatusFrame,text='Voltage:',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=1,sticky='E')            
            self.voltage_entry.grid(row=3,column=2,sticky='W') 
            tk.Label(self.StatusFrame,text='(V)',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=3,sticky='W')  
        self.current_entry = tk.Entry(self.StatusFrame,textvariable=self.current_text,state=state,width=width)     
        if(model['current']):
            tk.Label(self.StatusFrame,text='Current:',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=1,sticky='E')
            self.current_entry.grid(row=4,column=2,sticky='W')        
            tk.Label(self.StatusFrame,text='(mA)',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=3,sticky='W')
        self.temperature_entry = tk.Entry(self.StatusFrame,textvariable=self.temperature_text,state=state,width=width)
        if(model['temperature']):
            tk.Label(self.StatusFrame,text='Temperature:',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=1,sticky='E')    
            self.temperature_entry.grid(row=5,column=2,sticky='W')       
            tk.Label(self.StatusFrame,text='(C)',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='CID:',font=self.font,bg=BG1,fg=FG1).grid(row=6,column=1,sticky='E')
        self.CID_entry = tk.Entry(self.StatusFrame,textvariable=self.CID_text,state=state,width=width)
        self.CID_entry.grid(row=6,column=2,sticky='W')
        tk.Label(self.StatusFrame,text='UID:',font=self.font,bg=BG1,fg=FG1).grid(row=7,column=1,sticky='E')
        self.UID_entry = tk.Entry(self.StatusFrame,textvariable=self.UID_text,state=state,width=width)
        self.UID_entry.grid(row=7,column=2,sticky='W')
        tk.Label(self.StatusFrame,text='Firmware:',font=self.font,bg=BG1,fg=FG1).grid(row=8,column=1,sticky='E')
        self.Firmware_entry = tk.Entry(self.StatusFrame,textvariable=self.Firmware_text,state=state,width=width)
        self.Firmware_entry.grid(row=8,column=2,sticky='W')
        self.uptime_entry = tk.Entry(self.StatusFrame,textvariable=self.uptime_text,state=state,width=width)
        if(model['uptime']): 
            tk.Label(self.StatusFrame,text='Uptime:',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=1,sticky='E')            
            self.uptime_entry.grid(row=9,column=2,sticky='W')
            tk.Label(self.StatusFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=3,sticky='W')       
        tk.Label(self.StatusFrame,text='Error:',font=self.font,bg=BG1,fg=FG1).grid(row=10,column=1,sticky='E')
        self.Error_entry = tk.Entry(self.StatusFrame,textvariable=self.Error_text,state=state,width=width)
        self.Error_entry.grid(row=10,column=2,columnspan=4,sticky='W')     
        self.StatusFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
        
        #Refresh Frame
        self.RefreshFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.RefreshFrame,text='   Refresh:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,columnspan=1, sticky='SE') 
        tk.Label(self.RefreshFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=3, sticky='SW')   
        self.PeriodScale = tk.Scale(self.RefreshFrame,from_='1',to='30', length=300, orient='horizontal',state='normal',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1)
        self.PeriodScale.grid(row=1,column=2,columnspan=1,sticky='W')
        self.PeriodScale.set(self.PERIOD)
        self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        #self.RefreshFrame.pack()
        
        #Advanced Menu Enable
        self.advancedfeatures = tk.Checkbutton(self.master, text='Enable Advanced Features',font=self.font,bg=BG1,fg=FG1,selectcolor=BG2,activebackground=BG1,activeforeground=FG1, variable=self.Advanced_EN, command=self.AdvancedFeatures)
        self.advancedfeatures.pack()

        #Command Frame
        global rxlist, txlist
        txlist = ['$build,*']
        self.CommandFrame = tk.LabelFrame(self.master,text="",font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.CommandFrame,text="Command:",font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='W')
        self.Command = ttk.Combobox(self.CommandFrame,value=txlist) ##Makes the menu
        self.Command.grid(row=1,column=2,sticky='W',ipadx=70) ## Positions the Menu
        self.SendButton = tk.Button(self.CommandFrame,text='Send',font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.CommandLineSend,width=BUTTONWIDTH,state='disabled')
        self.SendButton.grid(row=1,column=3,padx=5)

        self.ListFrame = tk.LabelFrame(self.CommandFrame,font=self.font)
        self.scrollbar = tk.Scrollbar(self.ListFrame)
        self.scrollbar.pack(side ='right', fill ='y')        
        rxlist = tk.Listbox(self.ListFrame, yscrollcommand=self.scrollbar.set,width=70)
        rxlist.pack(fill='both')
        self.scrollbar.config(command=rxlist.yview)
        self.ListFrame.grid(row=3,column=1,columnspan=3,padx=5)

    def AdvancedFeatures(self):
        if(self.Advanced_EN.get()):
            self.PasswordFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
            self.pass_text = tk.StringVar()
            tk.Label(self.PasswordFrame,text='Enter Password:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
            tk.Entry(self.PasswordFrame,textvariable=self.pass_text,state='normal').grid(row=1,column=2)
            self.PasswordButton = tk.Button(self.PasswordFrame,font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Enter',command=self.passcheck,width=BUTTONWIDTH)
            self.PasswordButton.grid(row=1,column=3,padx=5)
            self.PasswordFrame.pack(ipadx=5)    
        else:
            self.CommandFrame.pack_forget()
            self.PasswordFrame.pack_forget()

    def updatePeriod(self,event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)

    def UpdateBand(self, band):
        #self.clear()
        self.Send(F'$setst,{band},*')
        #buffer['first_loop'] = True

    def updateDSA(self,event):
        if(buffer['conn_EN']):
            value = float(self.DSAScale.get())
            self.DSAScale.set(value)
            self.master.update()
            atten = COMM.attenuation(model['DSA'],value,True)
            self.Send('$setda,'+str(int(buffer['band']))+','+str(atten)+',*')
            buffer['first_loop'] = True

    def UpdateCommunication(self,method):
        if(method == 1):
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.TCP_EN.set(not self.TCP_EN.get())
            self.IP['state'] = 'disabled'
            self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'        
        self.Disconnect()

    def Connect(self,model):
        self.TCPPort.get()
        self.SerialPort.get()
        if not buffer['conn_EN']:
            if(self.Serial_EN.get()):
                if(self.SerialPort.get() != ''):
                    try: 
                        self.StatusLightFlash('orange', False)
                        if(model['serial'] == 2):                 
                            self.com = COMM.RS232_Connection(self.SerialPort.get())     
                        else:
                            self.com = COMM.RS485_Connection(self.SerialPort.get())                          
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)
                    
            elif(self.TCP_EN.get()):
                if(self.IP.get() != '' and self.TCPPort.get() != ''):
                    try:
                        self.StatusLightFlash('orange', False)             
                        self.com = COMM.Socket_Connection(self.IP.get(),self.TCPPort.get())
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)        
            if(buffer['conn_EN']):
                self.connectButton['text'] ='Disconnect'
                self.statusLightBackground.itemconfig(self.statusLight,fill='light green')
                self.EnableEntryBoxes(buffer['conn_EN'])
                self.EnableButtons('state', 'normal')
                self.EnableButtons('bg', BG2)
                self.DSAScale['state'] = 'normal'
                buffer['first_loop'] = True
                
                self.Poll()
        else:           
            self.Disconnect()

    def Disconnect(self):
        buffer['conn_EN'] = False
        self.connectButton['text'] ='Connect'
        self.statusLightBackground.itemconfig(self.statusLight,fill='red')
        self.clear()
        self.EnableButtons('state', 'disabled')
        self.EnableButtons('bg', BG1)
        self.DSAScale['state'] = 'disabled'
        self.master.update()
        if(self.com != None):
            self.com.close()
            self.com = None
        
    def CommandLineSend(self):
        message = self.Command.get()
        if(txlist.count(message) > 0):
            txlist.remove(message)
        txlist.insert(0, message)
        self.Command['values'] = txlist
        reply = self.Send(message)
        rxlist.insert(tk.END, reply)
        rxlist.see(tk.END)
    
    def clear(self):
        clearbuffer()
        self.UpdateEntryBoxes(buffer)
        self.DSAScale.set('0.5')

    def clear_band_colour(self):
        self.Band0['bg'] = BG2
        self.Band1['bg'] = BG2
        self.Band2['bg'] = BG2
        self.Band3['bg'] = BG2

    def UpdateEntryBoxes(self,buffer):
        self.Band_text.set(buffer['band'])
        self.voltage_text.set(buffer['voltage'])
        self.current_text.set(buffer['current'])
        self.temperature_text.set(buffer['temperature'])
        self.Error_text.set(buffer['error'])
        self.DSA_text.set(buffer['DSA'])
        self.CID_text.set(buffer['CID'])
        self.UID_text.set(buffer['UID'])
        self.Firmware_text.set(buffer['firmware'])
        self.uptime_text.set(buffer['uptime'])

    def EnableEntryBoxes(self,var):
        state = 'disabled'
        if(var):
            state = 'normal'
        self.Band_entry['state'] = state
        self.voltage_entry['state'] = state
        self.current_entry['state'] = state
        self.temperature_entry['state'] = state
        self.Error_entry['state'] = state
        self.DSA_entry['state'] = state
        self.CID_entry['state'] = state
        self.UID_entry['state'] = state
        self.Firmware_entry['state'] = state
        self.uptime_entry['state'] = state
            
    def EnableButtons(self, option, newstate):   
        self.Band0[option] = newstate
        self.Band1[option] = newstate
        self.Band2[option] = newstate 
        self.Band3[option] = newstate
        self.SendButton[option] = newstate
        self.master.update()

    def passcheck(self):
        if(self.pass_text.get() == PASSWORD):
            self.PasswordFrame.pack_forget()
            self.CommandFrame.pack(expand='True',fill='both',padx=5,pady=5)
        else:
            tk.Label(self.PasswordFrame,text='Password Incorrect',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2)
            #self.PasswordFrame.refresh()

    def StatusLightFlash(self,colour,flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight,'fill')  
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
        self.master.update()
        if(flash == True):
            colour2 = 'dark ' + colour
            while(i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
                self.master.update()
                time.sleep(delay/2)
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour2)
                self.master.update()
                time.sleep(delay/2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
            self.master.update()    

    def Send(self, message):
        reply = [None]
        try:
            if(self.com): 
                reply = self.com.send(message)               
            
            #Filter all item in reply    
            for item in reply:      
                if(item.count('$')):
                    newbuffer = COMM.filter(model['filter'],item)
                    buffer.update(newbuffer)
                        
        except Exception as e:
            self.Handle_Exception(e)

        return reply

    def Polling(self):
        GUI_DELAY = 0.1
        prev_time1 = time.time()
        prev_time2 = time.time()
        while(1):
            POLL_DELAY = self.PERIOD
            timer = time.time()
            delay1 = timer - prev_time1
            delay2 = timer - prev_time2
            if(delay1 > GUI_DELAY):   
                self.clock_text.set(updatetime())
                prev_time1 = timer
            if(delay2 > POLL_DELAY):
                self.Poll()
                prev_time2 = timer
            self.master.update()

    def Poll(self):
        if(buffer['conn_EN']):
            self.Send('$getst,*')
            if(buffer['first_loop']):
                self.Send('$build,*')
                buffer['first_loop'] = False
            if(buffer['band'] != ''):               
                self.Send('$setda,'+str(buffer['band'])+',*') 

            self.UpdateGUI(buffer)

    def UpdateGUI(self, buffer):
        if buffer['band']:
            band = int(buffer['band'])
            self.clear_band_colour()
            if band == bands[0]:
                self.Band0['bg'] = 'green'       
            elif band == bands[1]:    
                self.Band1['bg'] = 'green'
            elif band == bands[2]:
                self.Band2['bg'] = 'green'
            elif band == bands[3]:
                self.Band3['bg'] = 'green'
        if buffer['DSA']:
            self.DSAScale.set(COMM.attenuation(model['DSA'],buffer['DSA'],False))
        
        self.UpdateEntryBoxes(buffer)

    def Handle_Exception(self,exception):
        buffer['error']  = Catch_Exception(exception)
        self.Error_text.set(buffer['error'])

def Catch_Exception(exception):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    error = [exc_type ,fname,exc_tb.tb_lineno,exception]

    return error

def updatetime():
    timer = datetime.datetime.now()
    clock = timer.strftime("%Y-%b-%d  %H:%M:%S")
    return str(clock)

def clearbuffer():
    for key in buffer:
        if(type(buffer[key]) != bool):
            buffer[key] = ''
        elif(key == 'first_loop'):
            buffer[key] = True

def get_path(filename): #Something to do with converting to an .exe file and still retaining the thumbnail.
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, filename)
    else:
        return filename


if __name__ == '__main__': 
    master = tk.Tk()
    myapp = GUI(master)
    myapp.after(100,myapp.Polling) 
    myapp.mainloop()

