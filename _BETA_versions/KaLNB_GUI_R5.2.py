#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-31 - Version 1. "As Benjamin see's it"
#R2 - 2022-02-02 - Changing GUI to class structure to remove global variables
#R3 - 2022-03-01 - Added colour. Minor reformatting of variables
#R4 - 2022-03-04 - Properly formatting variables depending on unit type
#R5 - BETA version demo
#R5.1 - 2022-03-31 - modified temporarily for Melbourne 4 band unit. 
#R5.2 - 2022-04-01 - Updated band naming convention, Updated DSA slider to 0.25 increments and hex conversion

import os, time, csv, sys
import tkinter as tk
from tkinter import ttk
import KaLNB_TCP_Serial_Communication_R3 as COMM
import tkinter.font as tkFont
from PIL import ImageTk, Image
import datetime

version = 'R5.2 BETA'

WIDTH = 475
HEIGHT = 900
BUTTONWIDTH = 10

BG1 = '#042B60'
BG2 = '#36A9E1'
FG1 = 'white'

PASSWORD = 'password' #Super secure password

temp_commands = ['$build,*', '$getst,*', '$setda,0,*','$getsv,*', '$getip,*', '$reset,*' ]

buffer = {'CID':None, 'UID':None, 'firmware':None, 
        'DSA':None, 'band': None,
        'voltage':None, 'current': None, 'temperature':None, 'uptime':None,
        'IP_unit':None,'IP_server':None,'TCP_port':None, 'serial_port':None, 'serial_EN':False, 'TCP_EN':False, 'conn_EN':False,
        'rx':None, 'tx':None, 'error':None}

class GUI(tk.Frame):

    def __init__(self, master):
        super().__init__(master)
        self.master.geometry(str(WIDTH)+'x'+str(HEIGHT))
        self.master.resizable(width=0, height=0)
        self.master.title('Orbital Communicator ' + str(version))
        #self.master.iconbitmap(get_path('orbicon.ico'))

        self.com = None
        self.PERIOD = 5 #seconds
        self.flag = False
        self.font = tkFont.Font(family="Helvetica",size=10)

        self.master.configure(bg=BG1)

        #Variables
        self.TCP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.Advanced_EN = tk.IntVar()
        self.Band_text = tk.StringVar()
        self.voltage_text = tk.StringVar()
        self.current_text = tk.StringVar()
        self.temperature_text = tk.StringVar()
        self.uptime_text = tk.StringVar()
        self.Error_text = tk.StringVar()
        self.DSA_text = tk.StringVar()
        self.CID_text = tk.StringVar()
        self.UID_text = tk.StringVar()
        self.Firmware_text = tk.StringVar()
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        self.clock_text = tk.StringVar()
        self.clock_text.set(updatetime())

        #DiplayFrame
        self.DisplayFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
        self.logo = tk.PhotoImage(file=get_path('orblogob2.png'))
        self.logoFrame = tk.Label(self.DisplayFrame, image=self.logo,bg=BG1).pack(side='left',anchor='nw')#grid(row=1,column=1,sticky='W')
        self.clock = tk.Label(self.DisplayFrame,font=self.font,textvariable=self.clock_text,bg=BG1,fg=FG1).pack(side='top', anchor='ne')#grid(row=1,column=2,sticky='E')
        #self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text,font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2,sticky='W',columnspan=2)
        self.statusLightBackground = tk.Canvas(self.DisplayFrame,bg=BG1,width=30,height=30,highlightbackground=BG1)
        self.statusLightBackground.pack(side='right',anchor='e')#grid(row=1,column=4,sticky='E')

        self.statusLight = self.statusLightBackground.create_oval(3,3,30,30,fill='red')
        tk.Label(self.DisplayFrame,font=self.font,text='Connection Status:',bg=BG1,fg=FG1).pack(side='right',anchor='e')#grid(row=1,column=3,sticky='E')
        
        #Connection Frame
        self.ConnectionFrame = tk.LabelFrame(self.master,text="Communication Menu",width=WIDTH,font=self.font,bg=BG1,fg=FG1)
        self.Serial_EN.set(True)
        tk.Checkbutton(self.ConnectionFrame,font=self.font,text='Serial Enable',variable=self.Serial_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method='Serial': self.UpdateCommunication(method)).grid(row=1,column=1)
        tk.Checkbutton(self.ConnectionFrame,font=self.font,text='IP Enable',variable=self.TCP_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method='TCP': self.UpdateCommunication(method)).grid(row=1,column=3)        
        tk.Label(self.ConnectionFrame,font=self.font,text='Unit IP:',bg=BG1,fg=FG1).grid(row=2,column=3,sticky='E')
        self.IP = tk.Entry(self.ConnectionFrame,state='disabled')
        self.IP.grid(row=2,column=4,sticky='W')
        tk.Label(self.ConnectionFrame,font=self.font,text='Port:',bg=BG1,fg=FG1).grid(row=3,column=3,sticky='E')
        self.TCPPort = tk.Entry(self.ConnectionFrame,state='disabled')
        self.TCPPort.grid(row=3,column=4,sticky='W')
        tk.Label(self.ConnectionFrame,font=self.font,text='COM Port:',bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')
        self.SerialPort = tk.Entry(self.ConnectionFrame,state='normal')
        self.SerialPort.grid(row=2,column=2,sticky='W')        
        self.connectButton = tk.Button(self.ConnectionFrame,font=self.font,text='Connect',bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,command=self.Connect,width=BUTTONWIDTH)
        self.connectButton.grid(row=4,column=4,sticky='E')
        

        #Band Select Frame
        self.BandSelectFrame = tk.Label(self.master,bg=BG1)
        self.Band1 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band0',command=lambda band=1: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
        self.Band2 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band1',command=lambda band=2: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')   
        self.Band3 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band2',command=lambda band=3: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
        self.Band4 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band3',command=lambda band=4: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
        padx = 10
        self.Band1.grid(row=1,column=1,padx=padx,pady=10)
        self.Band2.grid(row=1,column=2,padx=padx,pady=10)
        self.Band3.grid(row=1,column=3,padx=padx,pady=10)
        self.Band4.grid(row=1,column=4,padx=padx,pady=10)
        

        #DSA Scale Frame
        self.ScaleFrame = tk.LabelFrame(self.master,text='Band DSA Setting (dB)',font=self.font,bg=BG1,fg=FG1)
        self.DSAScale = tk.Scale(self.ScaleFrame,digits=4,from_='0.5',to='31.5',resolution=0.25,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')
        self.DSAScale.pack(fill='both',padx=5,pady=5)
        self.DSAScale.bind("<ButtonRelease-1>", self.updateDSA)
        self.ScaleFrame.grid_propagate(0)
        

        #Status Frame
        self.StatusFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)       
        tk.Label(self.StatusFrame,text='Band:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='DSA:',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Voltage:',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Current:',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Temperature:',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='CID:',font=self.font,bg=BG1,fg=FG1).grid(row=6,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='UID:',font=self.font,bg=BG1,fg=FG1).grid(row=7,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Firmware:',font=self.font,bg=BG1,fg=FG1).grid(row=8,column=1,sticky='E') 
        tk.Label(self.StatusFrame,text='Uptime:',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Error:',font=self.font,bg=BG1,fg=FG1).grid(row=10,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='(V)',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='(mA)',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='(C)',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=3,sticky='W')
        width = 50
        state = 'normal'
        self.Band_entry = tk.Entry(self.StatusFrame,textvariable=self.Band_text,state=state,width=width)
        self.voltage_entry = tk.Entry(self.StatusFrame,textvariable=self.voltage_text,state=state,width=width)
        self.current_entry = tk.Entry(self.StatusFrame,textvariable=self.current_text,state=state,width=width)
        self.temperature_entry = tk.Entry(self.StatusFrame,textvariable=self.temperature_text,state=state,width=width)
        self.Error_entry = tk.Entry(self.StatusFrame,textvariable=self.Error_text,state=state,width=width)
        self.DSA_entry = tk.Entry(self.StatusFrame,textvariable=self.DSA_text,state=state,width=width)
        self.CID_entry = tk.Entry(self.StatusFrame,textvariable=self.CID_text,state=state,width=width)
        self.UID_entry = tk.Entry(self.StatusFrame,textvariable=self.UID_text,state=state,width=width)
        self.Firmware_entry = tk.Entry(self.StatusFrame,textvariable=self.Firmware_text,state=state,width=width)
        self.uptime_entry = tk.Entry(self.StatusFrame,textvariable=self.uptime_text,state=state,width=width)
        self.Band_entry.grid(row=1,column=2,sticky='W')
        self.DSA_entry.grid(row=2,column=2,sticky='W')
        self.voltage_entry.grid(row=3,column=2,sticky='W')
        self.current_entry.grid(row=4,column=2,sticky='W')
        self.temperature_entry.grid(row=5,column=2,sticky='W')
        self.CID_entry.grid(row=6,column=2,sticky='W')
        self.UID_entry.grid(row=7,column=2,sticky='W')
        self.Firmware_entry.grid(row=8,column=2,sticky='W')
        self.uptime_entry.grid(row=9,column=2,sticky='W')
        self.Error_entry.grid(row=10,column=2,columnspan=4,sticky='W')

        tk.Label(self.StatusFrame,text='Refresh Period:',font=self.font,bg=BG1,fg=FG1).grid(row=11,column=1,columnspan=1, sticky='SE') 
        tk.Label(self.StatusFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=11,column=3, sticky='SW')   
        self.PeriodScale = tk.Scale(self.StatusFrame,from_='1',to='30', length=300, orient='horizontal',state='normal',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1)
        self.PeriodScale.grid(row=11,column=2,columnspan=1,sticky='W')
        self.PeriodScale.set(self.PERIOD)
        self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        

        #Advanced Menu Enable
        self.advancedfeatures = tk.Checkbutton(self.master, text='Enable Advanced Features',font=self.font,bg=BG1,fg=FG1,selectcolor=BG2,activebackground=BG1,activeforeground=FG1, variable=self.Advanced_EN, command=self.AdvancedFeatures)

        #Command Frame
        self.CommandFrame = tk.LabelFrame(self.master,text="",font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.CommandFrame,text="Command:",font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='W')
        self.Command = ttk.Combobox(self.CommandFrame,value=temp_commands) ##Makes the menu
        self.Command.set('$build,*') ## Set Default Value
        self.Command.grid(row=1,column=2,sticky='W',ipadx=70) ## Positions the Menu
        self.b1 = tk.Button(self.CommandFrame,text='Send',font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.CommandLineSend,width=BUTTONWIDTH,state='disabled')
        self.b1.grid(row=1,column=3,padx=5)
        tk.Label(self.CommandFrame,text='TX:',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')
        tk.Label(self.CommandFrame,text='RX:',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=1,sticky='E')
        self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text,font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2,sticky='W',columnspan=2)
        self.rx = tk.Label(self.CommandFrame,textvariable=self.rx_text,font=self.font,bg=BG1,fg=FG1,wraplength=350).grid(row=3,column=2,sticky='W',columnspan=2) 

        model = 'Default'
        self.PackGUI(model)

    def PackGUI(self,model):
        #Used to pack GUI based on feature list
        if(model == 'Default'):
            self.DisplayFrame.pack(side='top',anchor='e',fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
            self.ConnectionFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
            self.BandSelectFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
            self.ScaleFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
            self.StatusFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
            self.advancedfeatures.pack()
        if(model == 'Pass'):
            pass

    def updatePeriod(self,event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)   

    def updateDSA(self,event):
        value = float(self.DSAScale.get())
        atten = converttohex(value,True)
        self.Send('$setda,'+str(int(buffer['band'])-1)+','+str(atten)+',*')
        self.Polling2()

    def UpdateCommunication(self,method):
        if(method == 'TCP'):
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.TCP_EN.set(not self.TCP_EN.get())
            self.IP['state'] = 'disabled'
            self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'        
        self.Disconnect()

    def Connect(self):
        self.TCPPort.get()
        self.SerialPort.get()
        if(buffer['conn_EN'] == False):
            if(self.Serial_EN.get() == True):
                if(self.SerialPort.get() != ''):
                    try: 
                        self.StatusLightFlash('orange', False)                 
                        self.com = COMM.SerialConn(self.SerialPort.get())
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)
                    
            elif(self.TCP_EN.get() == True):
                #self.Band_text.set('TCP checkbox')
                if(self.IP.get() != '' and self.TCPPort.get() != ''):
                    try:
                        self.StatusLightFlash('orange', False)             
                        self.com = COMM.SocketConn(self.IP.get(),self.TCPPort.get())
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)        
            if(buffer['conn_EN'] == True):
                self.connectButton['text'] ='Disconnect'
                self.statusLightBackground.itemconfig(self.statusLight,fill='light green')
                self.EnableEntryBoxes(buffer['conn_EN'])
                self.EnableButtons('state', 'normal')
                self.EnableButtons('bg', BG2)
                self.DSAScale['state'] = 'normal'
        else:           
            self.Disconnect()

    def Disconnect(self):
        buffer['conn_EN'] = False
        self.connectButton['text'] ='Connect'
        self.statusLightBackground.itemconfig(self.statusLight,fill='red')
        self.clear()
        #self.EnableEntryBoxes(buffer['conn_EN'])
        self.EnableButtons('state', 'disabled')
        self.EnableButtons('bg', BG1)
        self.DSAScale['state'] = 'disabled'
        self.master.update()
        if(self.com != None):
            self.com.close()
        
    def CommandLineSend(self):
        self.flag = True
        message = self.Command.get()
        self.tx_text.set(message)
        reply = self.Send(message)
        self.rx_text.set(reply)

    def bandbutton(self, band):
        self.clear()
        self.Band1['bg'] = BG2
        self.Band2['bg'] = BG2
        self.Band3['bg'] = BG2
        self.Band4['bg'] = BG2
        reply = self.Send('$setst,' + str(band-1) + ',*')
        self.Polling2()
    
    def clear(self):
        clearbuffer()
        self.UpdateEntryBoxes(buffer)
        self.DSAScale.set('0.5')     

    def UpdateEntryBoxes(self,buffer):
        self.Band_text.set(buffer['band'])
        self.voltage_text.set(buffer['voltage'])
        self.current_text.set(buffer['current'])
        self.temperature_text.set(buffer['temperature'])
        self.Error_text.set(buffer['error'])
        self.DSA_text.set(buffer['DSA'])
        self.CID_text.set(buffer['CID'])
        self.UID_text.set(buffer['UID'])
        self.Firmware_text.set(buffer['firmware'])
        self.uptime_text.set(buffer['uptime'])
        self.master.update()

    def EnableEntryBoxes(self,var):
        state = 'disabled'
        if(var==True):
            state = 'normal'
        self.Band_entry['state'] = state
        self.voltage_entry['state'] = state
        self.current_entry['state'] = state
        self.temperature_entry['state'] = state
        self.Error_entry['state'] = state
        self.DSA_entry['state'] = state
        self.CID_entry['state'] = state
        self.UID_entry['state'] = state
        self.Firmware_entry['state'] = state
        self.uptime_entry['state'] = state
            
    
    def EnableButtons(self, option, newstate):   
        self.Band1[option] = newstate
        self.Band2[option] = newstate
        self.Band3[option] = newstate
        self.Band4[option] = newstate        
        self.b1[option] = newstate
        self.master.update()

    def AdvancedFeatures(self):
        if(self.Advanced_EN.get()):
            self.PasswordFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
            self.pass_text = tk.StringVar()
            tk.Label(self.PasswordFrame,text='Enter Password:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
            tk.Entry(self.PasswordFrame,textvariable=self.pass_text,state='normal').grid(row=1,column=2)
            self.PasswordButton = tk.Button(self.PasswordFrame,font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Enter',command=self.passcheck,width=BUTTONWIDTH)
            self.PasswordButton.grid(row=1,column=3,padx=5)
            self.PasswordFrame.pack(ipadx=5)    
        else:
            self.CommandFrame.pack_forget()
            self.PasswordFrame.pack_forget()

    def passcheck(self):
        if(self.pass_text.get() == PASSWORD):
            self.PasswordFrame.pack_forget()
            self.CommandFrame.pack(expand='True',fill='both',padx=5,pady=5)
        else:
            tk.Label(self.PasswordFrame,text='Password Incorrect').grid(row=2,column=2)

    def StatusLightFlash(self,colour,flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight,'fill')  
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
        self.master.update()
        if(flash == True):
            colour2 = 'dark ' + colour
            while(i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
                self.master.update()
                time.sleep(delay/2)
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour2)
                self.master.update()
                time.sleep(delay/2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
            self.master.update()

    def FilterReceived(self, message):

        reply = message.split(',')

        command = reply[0].lower()

        if(command == '$buildr'):
            buffer['CID'] = reply[1] 
            buffer['UID'] = reply[2] 
            buffer['firmware'] = reply[4]+' '+reply[5]+' '+reply[6]+' '+reply[7]
        elif(command == '$statr'):
            buffer['CID'] = reply[1]
            buffer['UID'] = reply[2]
            buffer['uptime'] = reply[4]
            buffer['band'] = int(reply[14])
            buffer['voltage'] = round(float(reply[10])*0.1,1)
            buffer['current'] = reply[7]
            buffer['temperature'] = reply[13]        
        elif(command == '$setdar'):
            buffer['DSA'] = reply[5]         

        self.UpdateEntryBoxes(buffer)
        self.master.update()

    def Send(self, message):
        reply = ''
        if(self.com != None):
            try:
                reply = self.com.send(message)           
            except Exception as e:
                self.Error_text.set(e)
                return
        #Filter       
        reply = str(reply)
        if reply.count('$') > 0:
            array = reply.split('$')
            reply = '$'+array[-1]
        try:
            self.FilterReceived(reply)
        except Exception as e:
            buffer['error'] = e
            pass
        
        return reply

    def Polling(self):
        if(buffer['conn_EN'] == True): 
            self.Send('$getst,*')
            self.Send('$build,*')
            try:
                self.Send('$setda,'+str(int(buffer['band'])-1)+',*')
                self.DSAScale.set(converttohex(buffer['DSA'],False))
                band = int(buffer['band'])
                if band == 1:
                    self.Band1['bg'] = 'green'       
                elif band == 2:    
                    self.Band2['bg'] = 'green'
                elif band == 3:
                    self.Band3['bg'] = 'green'
                elif band == 4:
                    self.Band4['bg'] = 'green'
                else:
                    pass
            except:
                pass
        self.clock_text.set(updatetime())
        self.master.update()
        myapp.after(self.PERIOD*1000,myapp.Polling)

    def Polling2(self):
        if(buffer['conn_EN'] == True): 
            self.Send('$getst,*')
            self.Send('$build,*')
            try:
                self.Send('$setda,'+str(int(buffer['band'])-1)+',*')
                self.DSAScale.set(converttohex(buffer['DSA'],False))
                band = int(buffer['band'])
                if band == 1:
                    self.Band1['bg'] = 'green'       
                elif band == 2:    
                    self.Band2['bg'] = 'green'
                elif band == 3:
                    self.Band3['bg'] = 'green'
                elif band == 3:
                    self.Band4['bg'] = 'green'
                else:
                    pass
            except:
                pass
        self.clock_text.set(updatetime())
        self.master.update()

def updatetime():
    timer = datetime.datetime.now()
    clock = timer.strftime("%Y-%b-%d  %H:%M")
    return str(clock)

def converttohex(value,direction):
    if direction:
        new = hex(int(float(value)/0.25))
    else:
        new = int(value,16)*0.25
    return new

def clearbuffer():
    text=''
    buffer['band'] = text
    buffer['voltage'] = text
    buffer['current'] = text
    buffer['temperature'] = text
    buffer['error'] = None
    buffer['DSA'] = text
    buffer['CID'] = text
    buffer['UID'] = text
    buffer['firmware'] = text
    buffer['uptime'] = text

def get_path(filename):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, filename)
    else:
        return filename

if __name__ == '__main__': 
    master = tk.Tk()
    myapp = GUI(master)
    myapp.after(100,myapp.Polling)
    myapp.mainloop()

