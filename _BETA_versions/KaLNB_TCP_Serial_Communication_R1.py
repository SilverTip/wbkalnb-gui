#KALNB_TCP_Serial_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - Import code form Attenuation Sweep LNB ATE2 R2 - Serial and TCP Connection. Rewrite to class structure 


import socket
import serial
import sys

class SerialConn:
    def __init__(self, port):
        self.ser = serial.Serial()
        self.ser.port = 'COM'+str(port)
        self.ser.baudrate = 38400
        self.ser.parity = serial.PARITY_NONE 
        self.ser.stopbits = serial.STOPBITS_ONE
        self.ser.bytesize = serial.EIGHTBITS

    def write(self, message):
        message = bytes(message,'utf-8')
        self.ser.write(message)

    def read(self):
        character = ''
        reply = ''
        while(self.ser.in_waiting > 1):    
            character = self.ser.read(1)
            character = character.decode('utf-8')
            if character == '\n':
                character = ' '
            reply += character
        return reply

    def close(self):
        self.ser.close()


class SocketConn:
    def __init__(self, IP, Port):
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.server_address)

    def write(self,message):
        message = bytes(message,'utf-8')
        self.sock.sendall(message)

    def read(self):
        reply = self.sock.recv(1024)
        return str(reply)

    def close(self):
        self.sock.close()



if __name__ == '__main__':
    print('Main not enabled')
