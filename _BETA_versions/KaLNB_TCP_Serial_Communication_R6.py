#KALNB_TCP_Serial_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-25 Import code form Attenuation Sweep LNB ATE2 R2 - Serial and TCP Connection. Rewrite to class structure
#R2 - 2022-02-15 Consoliodate read/write methods to simple 'send' method which reads and writes. Carriage Return ('\r\n') on messages
#R3 - 2022-03-04 Minor bug fixes - added .decode() to socket reply.
#R4 - 2022-03-10 Replies are now lists of string separated by \n, ignoring the sent command
#R5_TriKa - 2022-03-16 Can encode TriKa messages with checksum and read TriKa reply ignoring checksum and addr. 
#R6 - 2022-03-16 Combine TriKa serial connection with KaLNB Serial connection with more descriptive classes. 


import socket
import serial
import sys
import time,sys,os

class TriKa_Serial_Connection:
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 9600,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        CMD = message.encode('ascii')
        STX = b'\x02'
        ADDR = b'\x39\x32'
        EXT = b'\x03'
        ACK = b'\x06'
        message = STX + ADDR + CMD + EXT    
        checksum = 0
        for num in message:
            checksum ^= num
        message+=(checksum).to_bytes(1, byteorder='big')
        self.ser.write(message)
        time.sleep(0.3)
        character = ''
        word = ''
        reply = []
        flag = False
        while(self.ser.in_waiting > 0):
            character = self.ser.read(1)
            if(character == EXT):
                flag = False
                reply.append(word)
                word = ''
            if(flag == True):
                letter = character.decode('ascii')
                word += letter    
            if(character == ACK):
                flag = True
                address = self.ser.read(2) #next 2 bytes are address so we can remove and ignore them (for now)     
        
        return reply

    def close(self):
        self.ser.close()

class WBKaLNB_Serial_Connection:
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 38400,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        tx = bytes(message,'ascii')  
        self.ser.write(tx)
        time.sleep(0.3)
        character = ''
        word = ''
        reply = []
        while(self.ser.in_waiting > 0):
            character = self.ser.read(1)
            character = character.decode('ascii')
            word += character
        
        reply = word.split('\n')
        
        return reply

    def close(self):
        self.ser.close()


class WBKaLNB_Socket_Connection:
    def __init__(self, IP, Port):
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.server_address)

    def send(self,message):
        message = bytes(str(message)+'\r\n','ascii')
        self.sock.sendall(message)
        time.sleep(0.1)
        word = self.sock.recv(1024).decode('ascii')
        reply = word.split('\n')
        reply.pop(0)
        
        return reply

    def close(self):
        self.sock.close()

if __name__ == '__main__':
    #COM = WBKaLNB_Socket_Connection('10.0.10.84',32019)
    #COM = WBKaLNB_Serial_Connection(7)
    COM = TriKa_Serial_Connection(7)
    while(1):
        message = input('Command:')
        reply = COM.send(message)
        print(reply)

