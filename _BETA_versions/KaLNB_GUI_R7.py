#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-31 - Version 1. "As Benjamin see's it"
#R2 - 2022-02-02 - Changing GUI to class structure to remove global variables
#R3 - 2022-03-01 - Added colour. Minor reformatting of variables
#R4 - 2022-03-04 - Properly formatting variables depending on unit type
#R5 - 2022-03-07 - Beta Release. Minor bug fixes
#R6 - 2022-03-14 - DKa and TriKa temporary solution - disabled IP, TriKa specific commands. Updated communication module to LNB_Communications_R1.py that includes TriKa command encoding/decoding
#R7 - 2022-04-08 - Generate GUI based on model number. New communication module LNB_Communication R2 with built in encoding/decoding and filtering of data.

import os, time, csv, sys
import tkinter as tk
from tkinter import ttk
import LNB_Communication_R3 as COMM
import tkinter.font as tkFont
from PIL import ImageTk, Image
import datetime

version = 'R7'

WIDTH = 475
HEIGHT = 900
BUTTONWIDTH = 10

BG1 = '#042B60'
BG2 = '#36A9E1'
FG1 = 'white'

PASSWORD = 'password' #Super secure password

temp_commands = ['$build,*', '$getst,*', '$setda,0,*','$getsv,*', '$getip,*', '$reset,*' ]

buffer = {'CID':None, 'UID':None, 'firmware':None, 
        'DSA':None,'newDSA':None, 'band':None,
        'voltage':None, 'current': None, 'temperature':None, 'uptime':None,
        'IP_unit':None,'IP_server':None,'TCP_port':None, 'serial_port':None, 'serial_EN':False, 'TCP_EN':False, 'conn_EN':False,
        'rx':None, 'tx':None, 'error':None}

MODEL_SETTINGS_LIST =  [{'name': 'LNBKA2G-WS-44548','IP':False,'serial':1, 'DSA':1, 'fault':None, 'bands':3, 'filter':1, 'band_offset': True,                     
                        'voltage':True, 'current': False, 'temperature':True, 'uptime':False},
                        {'name': 'LNBKAMS-XIWN60A','IP':False,'serial':1,'DSA':1, 'fault':None, 'bands':2, 'filter':1, 'band_offset': True,                        
                        'voltage':True, 'current': False, 'temperature':True, 'uptime':False},
                        {'name': 'LNB855-900XWS60','IP':True,'serial':2, 'DSA':2, 'fault':None, 'bands':False, 'filter':2, 'band_offset': False,                       
                        'voltage':True, 'current': True, 'temperature':True, 'uptime':True},
                        {'name': 'LNBKAWS-XWS60A', 'IP':False,'serial':1, 'DSA':2, 'fault':None, 'bands':4, 'filter':2, 'band_offset': False,                      
                        'voltage':True, 'current': True, 'temperature':True, 'uptime':True}
                        ]
MODELS = []
for item in MODEL_SETTINGS_LIST:
    MODELS.append(item['name'])

DEAFULT_MODEL = None #'LNBKAWS-XWS60A'

class GUI(tk.Frame):

    def __init__(self, master):
        super().__init__(master)
        self.master.geometry(str(WIDTH)+'x'+str(HEIGHT))
        self.master.resizable(width=0, height=0)
        self.master.title('Orbital Communicator ' + str(version))
        #self.master.iconbitmap(get_path('orbicon.ico'))

        self.com = None
        self.PERIOD = 1 #seconds
        self.flag = False
        self.font = tkFont.Font(family="Helvetica",size=10)

        self.master.configure(bg=BG1)

        #Variables
        self.TCP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.Advanced_EN = tk.IntVar()
        self.Band_text = tk.StringVar()
        self.voltage_text = tk.StringVar()
        self.current_text = tk.StringVar()
        self.temperature_text = tk.StringVar()
        self.uptime_text = tk.StringVar()
        self.Error_text = tk.StringVar()
        self.DSA_text = tk.StringVar()
        self.CID_text = tk.StringVar()
        self.UID_text = tk.StringVar()
        self.Firmware_text = tk.StringVar()
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        self.clock_text = tk.StringVar()
        self.clock_text.set(updatetime())

        self.first_loop = True

        #DiplayFrame
        self.DisplayFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
        self.logo = tk.PhotoImage(file=get_path('orblogob2.png'))
        self.logoFrame = tk.Label(self.DisplayFrame, image=self.logo,bg=BG1).pack(side='left',anchor='nw')#grid(row=1,column=1,sticky='W')
        self.clock = tk.Label(self.DisplayFrame,font=self.font,textvariable=self.clock_text,bg=BG1,fg=FG1).pack(side='top', anchor='ne')#grid(row=1,column=2,sticky='E')
        #self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text,font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2,sticky='W',columnspan=2)
        self.statusLightBackground = tk.Canvas(self.DisplayFrame,bg=BG1,width=30,height=30,highlightbackground=BG1)
        self.statusLightBackground.pack(side='right',anchor='e')#grid(row=1,column=4,sticky='E')
        self.statusLight = self.statusLightBackground.create_oval(3,3,30,30,fill='red')
        tk.Label(self.DisplayFrame,font=self.font,text='Connection Status:',bg=BG1,fg=FG1).pack(side='right',anchor='e')#grid(row=1,column=3,sticky='E')
        self.DisplayFrame.pack(side='top',anchor='e',fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
        
        #ModelFrame
        self.ModelFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.ModelFrame,text="Select LNB Model:",font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='W')
        self.Model = ttk.Combobox(self.ModelFrame,value=MODELS) ##Makes the menu
        self.Model.grid(row=1,column=2,sticky='W',ipadx=50) ## Positions the Menu
        self.ModelButton = tk.Button(self.ModelFrame,text='Enter',font=self.font,bg=BG1,fg=FG1,activebackground=BG1,activeforeground=FG1,disabledforeground=FG1,command=self.PackGUI,width=BUTTONWIDTH,state='normal')
        self.ModelButton.grid(row=1,column=3,padx=5)
        
        #Check if model has been pre-selected
        if(DEAFULT_MODEL):    
            self.PackGUI()
        else:
            self.ModelFrame.pack() 


    def PackGUI(self):
        #Used to pack GUI based on feature list
        if(DEAFULT_MODEL):
            model_name = DEAFULT_MODEL
        else:
            model_name = self.Model.get()
            self.ModelFrame.pack_forget()
        
        global model 
        model = None
        for settings in MODEL_SETTINGS_LIST:
            if(settings['name'] == model_name):
                model = settings
        if(model == None):
            return

        #Connection Frame
        self.Serial_EN.set(True)
        self.ConnectionFrame = tk.LabelFrame(self.master,text="Communication Menu",width=WIDTH,font=self.font,bg=BG1,fg=FG1)
        self.spacer = tk.Label(self.ConnectionFrame,font=self.font,bg=BG1,fg=FG1)
        self.SerialPort = tk.Entry(self.ConnectionFrame,state='normal',disabledbackground=BG1)
        if(model['serial']): 
            self.SerialCheckbutton = tk.Checkbutton(self.ConnectionFrame,font=self.font,text='Serial Enable',variable=self.Serial_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method=0: self.UpdateCommunication(method))          
            tk.Label(self.ConnectionFrame,font=self.font,text='COM Port:',bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')           
            self.SerialPort.grid(row=2,column=2,sticky='W')
            self.spacer['text'] = '                                         '
        self.IP = tk.Entry(self.ConnectionFrame,state='disabled',disabledbackground=BG1)
        self.TCPPort = tk.Entry(self.ConnectionFrame,state='disabled',disabledbackground=BG1)
        if(model['IP']):
            self.IPCheckbutton = tk.Checkbutton(self.ConnectionFrame,font=self.font,text='IP Enable',variable=self.TCP_EN,bg=BG1,fg=FG1,activebackground=BG1,selectcolor=BG2,command=lambda method=1: self.UpdateCommunication(method))                
            tk.Label(self.ConnectionFrame,font=self.font,text='Unit IP:',bg=BG1,fg=FG1).grid(row=2,column=3,sticky='E')                  
            self.IP.grid(row=2,column=4,sticky='W')  
            tk.Label(self.ConnectionFrame,font=self.font,text='Port',bg=BG1,fg=FG1).grid(row=3,column=3,sticky='E')                   
            self.TCPPort.grid(row=3,column=4,sticky='W')
        if(model['IP'] and model['serial']):
            self.SerialCheckbutton.grid(row=1,column=1)
            self.IPCheckbutton.grid(row=1,column=3)
            self.spacer['text'] = '                        '
        
        self.spacer.grid(row=2,column=3,sticky='E')

        self.connectButton = tk.Button(self.ConnectionFrame,font=self.font,text='Connect',bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,command=(lambda model=model:self.Connect(model)),width=BUTTONWIDTH)
        self.connectButton.grid(row=4,columnspan=4,column=4,sticky='E')
        self.ConnectionFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)      

        #Band Select Frame
        if(model['bands']):
            global bands
            bands = [0,1,2,3]
            if(model['band_offset']):
               bands = [x+1 for x in bands]
                    
            self.BandSelectFrame = tk.Label(self.master,bg=BG1)
            self.Band0 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[0]),command=lambda band=bands[0]: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
            self.Band1 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[1]),command=lambda band=bands[1]: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
            self.Band2 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[2]),command=lambda band=bands[2]: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
            self.Band3 = tk.Button(self.BandSelectFrame,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Band' + str(bands[3]),command=lambda band=bands[3]: self.bandbutton(band),width=BUTTONWIDTH,bd=6,state='disabled')
            bandpadx = 10              
            if(model['bands'] > 3):
                bandpadx = 9   
                self.Band3.grid(row=1,column=4,padx=bandpadx,pady=5)
            if(model['bands'] > 2):
                self.Band2.grid(row=1,column=3,padx=bandpadx,pady=5)          
            self.Band0.grid(row=1,column=1,padx=bandpadx,pady=5)
            self.Band1.grid(row=1,column=2,padx=bandpadx,pady=5)
            self.BandSelectFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
            #self.BandSelectFrame.place(anchor='center')
        

        #DSA Scale Frame
        if(model['DSA']):
            self.ScaleFrame = tk.LabelFrame(self.master,text='DSA Setting (dB)',font=self.font,bg=BG1,fg=FG1)
            if(model['DSA'] == 1):
                self.DSAScale = tk.Scale(self.ScaleFrame,from_='0.5',to='31.5',resolution=0.5,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')
            else:
                self.DSAScale = tk.Scale(self.ScaleFrame,digits=4,from_='0.5',to='31.5',resolution=0.25,orient='horizontal',state='disabled',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1,bd='1')      
            self.DSAScale.bind("<ButtonRelease-1>", self.updateDSA)
            self.ScaleFrame.grid_propagate(0)
            self.DSAScale.pack(fill='both',padx=5,pady=5)
            self.ScaleFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)
        

        #Status Frame
        self.StatusFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        width = 50
        state = 'normal'       
        self.Band_entry = tk.Entry(self.StatusFrame,textvariable=self.Band_text,state=state,width=width)
        if(model['bands'] > 1):
            tk.Label(self.StatusFrame,text='Band:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
            self.Band_entry.grid(row=1,column=2,sticky='W')
        self.DSA_entry = tk.Entry(self.StatusFrame,textvariable=self.DSA_text,state=state,width=width)
        if(model['DSA']): 
            tk.Label(self.StatusFrame,text='DSA:',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')                
            self.DSA_entry.grid(row=2,column=2,sticky='W')
        self.voltage_entry = tk.Entry(self.StatusFrame,textvariable=self.voltage_text,state=state,width=width)
        if(model['voltage']):
            tk.Label(self.StatusFrame,text='Voltage:',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=1,sticky='E')            
            self.voltage_entry.grid(row=3,column=2,sticky='W') 
            tk.Label(self.StatusFrame,text='(V)',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=3,sticky='W')  
        self.current_entry = tk.Entry(self.StatusFrame,textvariable=self.current_text,state=state,width=width)     
        if(model['current']):
            tk.Label(self.StatusFrame,text='Current:',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=1,sticky='E')
            self.current_entry.grid(row=4,column=2,sticky='W')        
            tk.Label(self.StatusFrame,text='(mA)',font=self.font,bg=BG1,fg=FG1).grid(row=4,column=3,sticky='W')
        self.temperature_entry = tk.Entry(self.StatusFrame,textvariable=self.temperature_text,state=state,width=width)
        if(model['temperature']):
            tk.Label(self.StatusFrame,text='Temperature:',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=1,sticky='E')    
            self.temperature_entry.grid(row=5,column=2,sticky='W')       
            tk.Label(self.StatusFrame,text='(C)',font=self.font,bg=BG1,fg=FG1).grid(row=5,column=3,sticky='W')
        tk.Label(self.StatusFrame,text='CID:',font=self.font,bg=BG1,fg=FG1).grid(row=6,column=1,sticky='E')
        self.CID_entry = tk.Entry(self.StatusFrame,textvariable=self.CID_text,state=state,width=width)
        self.CID_entry.grid(row=6,column=2,sticky='W')
        tk.Label(self.StatusFrame,text='UID:',font=self.font,bg=BG1,fg=FG1).grid(row=7,column=1,sticky='E')
        self.UID_entry = tk.Entry(self.StatusFrame,textvariable=self.UID_text,state=state,width=width)
        self.UID_entry.grid(row=7,column=2,sticky='W')
        tk.Label(self.StatusFrame,text='Firmware:',font=self.font,bg=BG1,fg=FG1).grid(row=8,column=1,sticky='E')
        self.Firmware_entry = tk.Entry(self.StatusFrame,textvariable=self.Firmware_text,state=state,width=width)
        self.Firmware_entry.grid(row=8,column=2,sticky='W')
        self.uptime_entry = tk.Entry(self.StatusFrame,textvariable=self.uptime_text,state=state,width=width)
        if(model['uptime']): 
            tk.Label(self.StatusFrame,text='Uptime:',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=1,sticky='E')            
            self.uptime_entry.grid(row=9,column=2,sticky='W')
            tk.Label(self.StatusFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=9,column=3,sticky='W')       
        tk.Label(self.StatusFrame,text='Error:',font=self.font,bg=BG1,fg=FG1).grid(row=10,column=1,sticky='E')
        self.Error_entry = tk.Entry(self.StatusFrame,textvariable=self.Error_text,state=state,width=width)
        self.Error_entry.grid(row=10,column=2,columnspan=4,sticky='W')     
        self.StatusFrame.pack(fill='both',ipadx=5,ipady=5,padx=5,pady=5,expand=False)
        
        #Refresh Frame
        self.RefreshFrame = tk.Label(self.master,text='',font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.RefreshFrame,text='   Refresh:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,columnspan=1, sticky='SE') 
        tk.Label(self.RefreshFrame,text='(seconds)',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=3, sticky='SW')   
        self.PeriodScale = tk.Scale(self.RefreshFrame,from_='1',to='30', length=300, orient='horizontal',state='normal',troughcolor=FG1,font=self.font,bg=BG1,fg=FG1,activebackground=BG2,highlightbackground=BG1)
        self.PeriodScale.grid(row=1,column=2,columnspan=1,sticky='W')
        self.PeriodScale.set(self.PERIOD)
        self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        #self.RefreshFrame.pack()
        
        #Advanced Menu Enable
        self.advancedfeatures = tk.Checkbutton(self.master, text='Enable Advanced Features',font=self.font,bg=BG1,fg=FG1,selectcolor=BG2,activebackground=BG1,activeforeground=FG1, variable=self.Advanced_EN, command=self.AdvancedFeatures)
        self.advancedfeatures.pack()

        #Command Frame
        self.CommandFrame = tk.LabelFrame(self.master,text="",font=self.font,bg=BG1,fg=FG1)
        tk.Label(self.CommandFrame,text="Command:",font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='W')
        self.Command = ttk.Combobox(self.CommandFrame,value=temp_commands) ##Makes the menu
        self.Command.set('$build,*') ## Set Default Value
        self.Command.grid(row=1,column=2,sticky='W',ipadx=70) ## Positions the Menu
        self.SendButton = tk.Button(self.CommandFrame,text='Send',font=self.font,bg=BG1,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,command=self.CommandLineSend,width=BUTTONWIDTH,state='disabled')
        self.SendButton.grid(row=1,column=3,padx=5)
        tk.Label(self.CommandFrame,text='TX:',font=self.font,bg=BG1,fg=FG1).grid(row=2,column=1,sticky='E')
        tk.Label(self.CommandFrame,text='RX:',font=self.font,bg=BG1,fg=FG1).grid(row=3,column=1,sticky='E')
        self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text,font=self.font,bg=BG1,fg=FG1).grid(row=2,column=2,sticky='W',columnspan=2)
        self.rx = tk.Label(self.CommandFrame,textvariable=self.rx_text,font=self.font,bg=BG1,fg=FG1,wraplength=350).grid(row=3,column=2,sticky='W',columnspan=2) 
        #text_area = tk.scrolledtext.ScrolledText(self.CommandFrame, wrap = tk.WORD, width = 40, height = 10, font = self.font).grid(row=3,column=2,sticky='W',columnspan=2) 


    def AdvancedFeatures(self):
        if(self.Advanced_EN.get()):
            self.PasswordFrame = tk.Label(self.master,font=self.font,bg=BG1,fg=FG1)
            self.pass_text = tk.StringVar()
            tk.Label(self.PasswordFrame,text='Enter Password:',font=self.font,bg=BG1,fg=FG1).grid(row=1,column=1,sticky='E')
            tk.Entry(self.PasswordFrame,textvariable=self.pass_text,state='normal').grid(row=1,column=2)
            self.PasswordButton = tk.Button(self.PasswordFrame,font=self.font,bg=BG2,fg=FG1,activebackground=BG2,activeforeground=FG1,disabledforeground=FG1,text='Enter',command=self.passcheck,width=BUTTONWIDTH)
            self.PasswordButton.grid(row=1,column=3,padx=5)
            self.PasswordFrame.pack(ipadx=5)    
        else:
            self.CommandFrame.pack_forget()
            self.PasswordFrame.pack_forget()

    def updatePeriod(self,event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)   

    def updateDSA(self,event):
        if(buffer['conn_EN']):
            value = float(self.DSAScale.get())
            self.DSAScale.set(value)
            #self.master.update()
            atten = COMM.attenuation(model['DSA'],value,True)
            self.Send('$setda,'+str(int(buffer['band']))+','+str(atten)+',*')

    def UpdateCommunication(self,method):
        if(method == 1):
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.TCP_EN.set(not self.TCP_EN.get())
            self.IP['state'] = 'disabled'
            self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'        
        self.Disconnect()

    def Connect(self,model):
        self.TCPPort.get()
        self.SerialPort.get()
        if(buffer['conn_EN'] == False):
            if(self.Serial_EN.get() == True):
                if(self.SerialPort.get() != ''):
                    try: 
                        self.StatusLightFlash('orange', False)
                        if(model['serial'] == 1):                
                            self.com = COMM.RS485_Connection(self.SerialPort.get())    
                        else:
                            self.com = COMM.RS232_Connection(self.SerialPort.get())                            
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)
                    
            elif(self.TCP_EN.get() == True):
                if(self.IP.get() != '' and self.TCPPort.get() != ''):
                    try:
                        self.StatusLightFlash('orange', False)             
                        self.com = COMM.Socket_Connection(self.IP.get(),self.TCPPort.get())
                        buffer['conn_EN'] = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)        
            if(buffer['conn_EN'] == True):
                self.first_loop = True
                self.connectButton['text'] ='Disconnect'
                self.statusLightBackground.itemconfig(self.statusLight,fill='light green')
                self.EnableEntryBoxes(buffer['conn_EN'])
                self.EnableButtons('state', 'normal')
                self.EnableButtons('bg', BG2)
                self.DSAScale['state'] = 'normal'
                self.Poll()
        else:           
            self.Disconnect()

    def Disconnect(self):
        buffer['conn_EN'] = False
        self.connectButton['text'] ='Connect'
        self.statusLightBackground.itemconfig(self.statusLight,fill='red')
        self.clear()
        self.EnableButtons('state', 'disabled')
        self.EnableButtons('bg', BG1)
        self.DSAScale['state'] = 'disabled'
        self.master.update()
        if(self.com != None):
            self.com.close()
            self.com = None
        
    def CommandLineSend(self):
        self.flag = True
        message = self.Command.get()
        self.tx_text.set(message)
        reply = self.Send(message)
        self.rx_text.set(reply)

    def bandbutton(self, band):
        #self.clear()
        self.Send('$setst,' + str(band) + ',*')
    
    def clear(self):
        clearbuffer()
        self.UpdateEntryBoxes(buffer)
        self.DSAScale.set('0.5')

    def clear_band_colour(self):
        self.Band0['bg'] = BG2
        self.Band1['bg'] = BG2
        self.Band2['bg'] = BG2
        self.Band3['bg'] = BG2

    def UpdateEntryBoxes(self,buffer):
        self.Band_text.set(buffer['band'])
        self.voltage_text.set(buffer['voltage'])
        self.current_text.set(buffer['current'])
        self.temperature_text.set(buffer['temperature'])
        self.Error_text.set(buffer['error'])
        self.DSA_text.set(buffer['DSA'])
        self.CID_text.set(buffer['CID'])
        self.UID_text.set(buffer['UID'])
        self.Firmware_text.set(buffer['firmware'])
        self.uptime_text.set(buffer['uptime'])

    def EnableEntryBoxes(self,var):
        state = 'disabled'
        if(var==True):
            state = 'normal'
        self.Band_entry['state'] = state
        self.voltage_entry['state'] = state
        self.current_entry['state'] = state
        self.temperature_entry['state'] = state
        self.Error_entry['state'] = state
        self.DSA_entry['state'] = state
        self.CID_entry['state'] = state
        self.UID_entry['state'] = state
        self.Firmware_entry['state'] = state
        self.uptime_entry['state'] = state
            
    
    def EnableButtons(self, option, newstate):   
        self.Band0[option] = newstate
        self.Band1[option] = newstate
        self.Band2[option] = newstate 
        self.Band3[option] = newstate
        self.SendButton[option] = newstate
        self.master.update()

    def passcheck(self):
        if(self.pass_text.get() == PASSWORD):
            self.PasswordFrame.pack_forget()
            self.CommandFrame.pack(expand='True',fill='both',padx=5,pady=5)
        else:
            tk.Label(self.PasswordFrame,text='Password Incorrect').grid(row=2,column=2)

    def StatusLightFlash(self,colour,flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight,'fill')  
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
        self.master.update()
        if(flash == True):
            colour2 = 'dark ' + colour
            while(i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
                self.master.update()
                time.sleep(delay/2)
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour2)
                self.master.update()
                time.sleep(delay/2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
            self.master.update()

    def FilterReceived(self, message):
        newbuffer = COMM.filter(model['filter'],message)
        
        for key in newbuffer.keys():
            if(key in buffer.keys()):
                buffer[key] = newbuffer[key]

    def Send(self, message):
        reply = ['']
        if(self.com != None): 
            reply = self.com.send(message)               
              
        for item in reply:      
            if item.count('$') > 0:
                array = item.split('$')
                unfiltered = '$'+array[-1]
                self.FilterReceived(unfiltered)

        return reply[0]

    def Polling(self):
        self.master.update()
        self.Poll()
        self.clock_text.set(updatetime())
        self.master.update()
        myapp.after(self.PERIOD*1000-600,myapp.Polling)

    def Poll(self):
        if(buffer['conn_EN'] == True):
            try:
                self.Send('$getst,*')
                self.Send('$build,*')                
                self.Send('$setda,'+str(buffer['band'])+',*')    
            except Exception as e:
                self.Catch_Exception(e)
                return
            self.UpdateGUI(buffer)

    def UpdateGUI(self, buffer):
        if((buffer['band'] != None) and (buffer['DSA'] != None) and (buffer['band'] != '') and (buffer['DSA'] != '')):
            band = int(buffer['band'])

            self.DSAScale.set(COMM.attenuation(model['DSA'],buffer['DSA'],False))

            self.clear_band_colour()
            if band == bands[0]:
                self.Band0['bg'] = 'green'       
            elif band == bands[1]:    
                self.Band1['bg'] = 'green'
            elif band == bands[2]:
                self.Band2['bg'] = 'green'
            elif band == bands[3]:
                self.Band3['bg'] = 'green'
        
        self.UpdateEntryBoxes(buffer)        


    def Catch_Exception(self,exception):
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        if(exception == 'serial.serialutil.PortNotOpenError'):
            return
        buffer['error']  = [exc_type ,fname,exc_tb.tb_lineno,exception]
        self.Error_text.set(buffer['error'])

def updatetime():
    timer = datetime.datetime.now()
    clock = timer.strftime("%Y-%b-%d  %H:%M")
    return str(clock)

def clearbuffer():
    text=''
    buffer['band'] = text
    buffer['voltage'] = text
    buffer['current'] = text
    buffer['temperature'] = text
    buffer['error'] = text
    buffer['DSA'] = text
    buffer['CID'] = text
    buffer['UID'] = text
    buffer['firmware'] = text
    buffer['uptime'] = text

def get_path(filename): #Something to do with converting to an .exe file and still retaining the thumbnail.
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, filename)
    else:
        return filename


    

if __name__ == '__main__': 
    master = tk.Tk()
    myapp = GUI(master)
    myapp.after(100,myapp.Polling)
    myapp.mainloop()

