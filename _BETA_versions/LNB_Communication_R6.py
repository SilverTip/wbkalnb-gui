#LNB_Communication
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-03-16 - Import code from KaLNB_TCP_Serial_Communication_R6.py. Rename module to include all LNBs. 
#R2 - 2022-03-23 - Filter responses based on type of connection - class methods to return interested values
#R3 - 2022-04-04 - filter, attenuation, and faults not dependent on comm type. 
#R4 - 2022-04-12 - updated faults
#R5 - 2022-05-04 - RS232 and Socket updated with /r/n
#R6 - 2022-05-05 - Manage decode exceptions, lengthen read delay for longest command ("cfg", ~150ms)

import socket
import serial
import sys
import time,sys,os

class RS485_Connection:
    #TriKa and DKa/DKu communications
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 9600,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        CMD = message.encode('utf-8')
        STX = b'\x02'
        ADDR = b'\x39\x32'
        EXT = b'\x03'
        ACK = b'\x06'
        message = STX + ADDR + CMD + EXT    
        checksum = 0
        for num in message:
            checksum ^= num
        message+=(checksum).to_bytes(1, byteorder='big')
        self.ser.write(message)
        time.sleep(0.15) #unstable <= 0.12.
        character = ''
        word = ''
        reply = ['']
        flag = False
        check = False
        while((self.ser.in_waiting > 0) and (self.ser.isOpen())):
            character = self.ser.read(1)
            if(character == EXT):
                check = True
                flag = False
                reply.append(word)
                word = ''
            if(flag == True):
                letter = character.decode('utf-8')
                word += letter    
            if(character == ACK):
                flag = True
                address = self.ser.read(2) #next 2 bytes are address so we can remove and ignore them (for now)     
        if(check):
            for i,v in enumerate(reply):
                if(reply[i] == ''):
                    reply.pop(i)
        
        return reply

    def close(self):
        self.ser.close()

class RS232_Connection:
    #WBKaLNB serial connection
    def __init__(self, port):
        self.ser = serial.Serial(
        port = 'COM'+str(port),
        baudrate = 38400,
        parity = serial.PARITY_NONE, 
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        )

    def send(self, message):
        tx = bytes(str(message)+'\r\n','utf-8') 
        self.ser.write(tx)
        time.sleep(0.22)
        character = ''
        word = ''
        reply = ['']
        while(self.ser.in_waiting > 0):
            character = self.ser.read(1)
            try: 
                character = character.decode('utf-8')     
            except:
                continue #decode of character failed
            word += character
        
        reply = word.split('\r\n')
        for index, value in enumerate(reply):
            if(value == ''):
                reply.pop(index)
            elif(value == message):
                reply.pop(index)
        
        return reply

    def close(self):
        self.ser.close()


class Socket_Connection:
    #WBKaLNB ethernet connection
    def __init__(self, IP, Port):
        self.server_address = (str(IP),int(Port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        self.sock.connect(self.server_address)

    def send(self,message):
        self.sock.sendall(bytes(str(message)+'\r\n','utf-8'))
        #r,w,e = select.select([self.sock],[],[],0)
        time.sleep(0.14)
        reply = ['']
        word = ''
        received = self.sock.recv(1024)
        for item in received:
            try:
                character = chr(item)
            
            except:
                continue #binary decode failed due to garbage
            word += character 
    
        reply = word.split('\r\n')
        #reply.pop(0)
        for index, value in enumerate(reply):
            if(value == ''):
                reply.pop(index)
            elif(value == message):
                reply.pop(index)
        
        return reply

    def close(self):
        self.sock.close()


def filter(version, message): #Hardcoded for now. Need to move to database
    buffer = {}
    reply = message.split(',')
    command = reply[0].lower()
    
    if(version==1):
        if(command == '$build'):
            buffer['CID'] = reply[1] 
            buffer['UID'] = reply[2]
            buffer['UTC'] = reply[3] 
            buffer['ver_info'] = reply[4]
            buffer['build_date'] = reply[5]
            buffer['firmware'] = buffer['ver_info'] + ' ' + buffer['build_date']
        elif(command == '$statr'):
            buffer['CID'] = reply[1]
            buffer['UID'] = reply[2]
            buffer['CNT'] = reply[3]
            buffer['band'] = reply[4]
            buffer['voltage'] = reply[5]
            buffer['temperature'] = reply[6]
            buffer['current'] = reply[7]
            buffer['fault'] = fault(3, reply[8])
        elif(command == '$setdar'):
            buffer['CID'] = reply[1]
            buffer['UID'] = reply[2]
            buffer['timestamp'] = reply[3]
            buffer['index'] = reply[4]
            buffer['DSA'] = reply[5]

    elif(version == 2):
        if(command == '$buildr'):
            buffer['CID'] = reply[1] 
            buffer['UID'] = reply[2] 
            buffer['firmware'] = reply[4] + ' ' + reply[5] + ' ' + reply[6] + ' ' + reply[7]
        elif(command == '$statr'):
            buffer['CID'] = reply[1]
            buffer['UID'] = reply[2]
            buffer['uptime'] = reply[4]
            buffer['band'] = int(reply[14])
            buffer['voltage'] = round(float(reply[10])*0.1,1)
            buffer['current'] = reply[7]
            buffer['temperature'] = reply[13]
            buffer['fault'] = fault(1, reply[5])
            buffer['fault2'] = fault(2, reply[6])
        elif(command == '$setdar'):
            buffer['CID'] = reply[1] 
            buffer['UID'] = reply[2] 
            buffer['DSA'] = reply[5]

    return buffer

def attenuation(version,value,direction):
    new = 0.5
    if(version==1):
        if direction:
            new = hex(int(63-(value*2)))
        else:
            new = abs(int(value,16)-63)/2

    elif(version==2):
        if direction:
            new = hex(int(float(value)/0.25))
        else:
            new = int(value,16)*0.25
    
    return new

def fault(version, fault):
    error = ''
    error_list = []
    if(version == 1):    
        for i in range(32):
            error_list.append('bit'+str(i))
        error_list[24] = 'LOLA'
        error = DecodeFault(fault,32,error_list)

    elif(version==2):
        for i in range(32):
            error_list.append('bit'+str(i))
        error = DecodeFault(fault,32,error_list)

    return error

def DecodeFault(value, bits, error_list):
    #Format string to list of bits, little endian
    number, pad, rjust, size, kind = int(value,16), '0', '>', bits, 'b'
    t = F'{number:{pad}{rjust}{size}{kind}}'
    new = list(t)
    new.reverse()

    #Decode error from list of bits
    error = ''
    for i,v in enumerate(new):
        if(v=='1'):
            error += F'{error_list[i]} '

    return(error)

if __name__ == '__main__':
    #com = Socket_Connection('10.0.10.86',32019)
    com = RS232_Connection(5)
    #com = RS485_Connection(7)
    while(1):
        try:
            message = input('Command:')
            reply = com.send(message)
            for items in reply:
                print(items)
        except KeyboardInterrupt:
            com.close()
            print('Closed communications method')
            

