#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-31 - Version 1. "As Benjamin see's it"
#R2 - 2022-02-02 - Changing GUI to class structure to remove global variables

import os, time, csv
import tkinter as tk
from tkinter import ttk
import KaLNB_TCP_Serial_Communication_R2 as COMM

version = 2



WIDTH = 450
HEIGHT = 650
BUTTONWIDTH = 10

PASSWORD = 'password' #Super secure password

temp_commands = ['$build,*', '$getst,*, ''$getsv,*', '$getip,*', '$reset,*' ]

class GUI(tk.Frame):



    def __init__(self, master):
        super().__init__(master)
        self.master.geometry(str(WIDTH)+'x'+str(HEIGHT))
        self.master.resizable(width=0, height=0)
        self.master.title('Orbital Communicator R' + str(version))
        #self.master.configure(bg=OrbDarkBlue)
        #self.master.iconbitmap(r'OrbIcon.ico')
        #self.title = tk.Label(self.master,text="Orbtial Research Ltd.").grid(sticky='E')

        self.com = None
        self.PERIOD = 1 #seconds
        self.flag = False
        self.COUNTER = 1

        #Variables
        self.TCP_EN = tk.IntVar()
        self.Serial_EN = tk.IntVar()
        self.Advanced_EN = tk.IntVar()
        self.connectStatus = False
        self.Band_text = tk.StringVar()
        self.voltage_text = tk.StringVar()
        self.current_text = tk.StringVar()
        self.Error_text = tk.StringVar()
        self.DSA_text = tk.StringVar()
        #self.LO_text = tk.StringVar()
        self.CID_text = tk.StringVar()
        self.UID_text = tk.StringVar()
        self.FirmwareVerison_text = tk.StringVar()
        self.tx_text = tk.StringVar()
        self.rx_text = tk.StringVar()
        
        #Connection Frame
        self.ConnectionFrame = tk.LabelFrame(self.master,text="Communication Menu",width=WIDTH)
        self.statusLightBackground = tk.Canvas(self.ConnectionFrame,bg='#F0F0F0',width=17,height=17)
        self.statusLightBackground.grid(row=1,column=4,sticky='E')
        self.statusLight = self.statusLightBackground.create_oval(3,3,17,17,fill='red')
        self.Serial_EN.set(True)
        tk.Checkbutton(self.ConnectionFrame, text='Serial Enable', variable=self.Serial_EN, command=lambda method='Serial': self.UpdateCommunication(method)).grid(row=1,column=1)
        tk.Checkbutton(self.ConnectionFrame, text='TCP Enable', variable=self.TCP_EN, command=lambda method='TCP': self.UpdateCommunication(method)).grid(row=1,column=3)        
        tk.Label(self.ConnectionFrame,text='Client IP:').grid(row=2,column=3,sticky='E')
        self.IP = tk.Entry(self.ConnectionFrame,state='disabled')
        self.IP.grid(row=2,column=4,sticky='W')
        tk.Label(self.ConnectionFrame,text='Port:').grid(row=3,column=3,sticky='E')
        self.TCPPort = tk.Entry(self.ConnectionFrame,state='disabled')
        self.TCPPort.grid(row=3,column=4,sticky='W')
        tk.Label(self.ConnectionFrame,text='COM Port:').grid(row=2,column=1,sticky='E')
        self.SerialPort = tk.Entry(self.ConnectionFrame,state='normal')
        self.SerialPort.grid(row=2,column=2,sticky='W')        
        self.connectButton = tk.Button(self.ConnectionFrame,text='Connect',command=self.Connect,width=BUTTONWIDTH)
        self.connectButton.grid(row=4,column=4,sticky='E')
        self.ConnectionFrame.pack(fill='none',ipadx=5, ipady=5,padx=5,pady=5,expand=False)

        #Band Select Frame
        self.BandSelectFrame = ttk.Frame(self.master)
        self.Band1 = tk.Button(self.BandSelectFrame,text='Band1',command=lambda band=1: self.bandbutton(band),width=BUTTONWIDTH,bd=6,activebackground='green',state='disabled')
        self.Band2 = tk.Button(self.BandSelectFrame,text='Band2',command=lambda band=2: self.bandbutton(band),width=BUTTONWIDTH,bd=6,activebackground='green',state='disabled')
        self.Band3 = tk.Button(self.BandSelectFrame,text='Band3',command=lambda band=3: self.bandbutton(band),width=BUTTONWIDTH,bd=6,activebackground='green',state='disabled')
        self.Band1.grid(row=1,column=1,padx=20,pady=10)
        self.Band2.grid(row=1,column=2,padx=20,pady=10)
        self.Band3.grid(row=1,column=3,padx=20,pady=10)
        self.BandSelectFrame.pack(fill='none',ipadx=5,ipady=5,padx=5,pady=5,expand=False)

        #DSA Scale Frame
        self.ScaleFrame = tk.LabelFrame(self.master,text='Band DSA Setting')
        self.DSAScale = tk.Scale(self.ScaleFrame,from_='0.5',to='65', resolution=0.5,orient='horizontal',command=self.UpdateDSA,state='disabled')
        self.DSAScale.pack(fill='both',padx=5,pady=5)
        self.ScaleFrame.grid_propagate(0)
        self.ScaleFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)

        #Status Frame
        self.StatusFrame = tk.LabelFrame(self.master,text='Unit Status')       
        tk.Label(self.StatusFrame,text='Band:').grid(row=1,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='DSA:').grid(row=2,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Voltage:').grid(row=3,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Current:').grid(row=4,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='Error:').grid(row=5,column=1,sticky='E')     
        #tk.Label(self.StatusFrame,text='LO:').grid(row=6,column=1,sticky='E')
        tk.Label(self.StatusFrame,text='CID:').grid(row=1,column=3,sticky='E')
        tk.Label(self.StatusFrame,text='UID:').grid(row=2,column=3,sticky='E')
        tk.Label(self.StatusFrame,text='Firmware Verison:').grid(row=3,column=3,sticky='E') 


        self.FirmwareVerison_text = tk.StringVar()
        self.Band_entry = tk.Entry(self.StatusFrame,textvariable=self.Band_text,state='disabled')
        self.voltage_entry = tk.Entry(self.StatusFrame,textvariable=self.voltage_text,state='disabled')
        self.current_entry = tk.Entry(self.StatusFrame,textvariable=self.current_text,state='disabled')
        self.Error_entry = tk.Entry(self.StatusFrame,textvariable=self.Error_text,state='normal',width=58)
        self.DSA_entry = tk.Entry(self.StatusFrame,textvariable=self.DSA_text,state='disabled')
        #self.LO_entry = tk.Entry(self.StatusFrame,textvariable=self.LO_text,state='disabled')
        self.CID_entry = tk.Entry(self.StatusFrame,textvariable=self.CID_text,state='disabled')
        self.UID_entry = tk.Entry(self.StatusFrame,textvariable=self.UID_text,state='disabled')
        self.Firmware_entry = tk.Entry(self.StatusFrame,textvariable=self.FirmwareVerison_text,state='disabled')
        self.Band_entry.grid(row=1,column=2)
        self.DSA_entry.grid(row=2,column=2)
        self.voltage_entry.grid(row=3,column=2)
        self.current_entry.grid(row=4,column=2)
        self.Error_entry.grid(row=5,column=2,columnspan=4,sticky='W') 
        #self.LO_entry.grid(row=6,column=2)
        self.CID_entry.grid(row=1,column=4)
        self.UID_entry.grid(row=2,column=4)
        self.Firmware_entry.grid(row=3,column=4)

        tk.Label(self.StatusFrame,text='Refresh Period:').grid(row=6,column=1,columnspan=2, sticky='SE') 
        tk.Label(self.StatusFrame,text='(seconds)').grid(row=6,column=3, sticky='SW')   
        self.PeriodScale = tk.Scale(self.StatusFrame,from_='1',to='30', length=200, orient='horizontal',state='normal')
        self.PeriodScale.grid(row=6,column=3,columnspan=4,sticky='W')
        self.PeriodScale.set(1)
        self.PeriodScale.bind("<ButtonRelease-1>", self.updatePeriod)
        self.StatusFrame.pack(fill='both',ipadx=5, ipady=5,padx=5,pady=5,expand=False)

        #Advanced Menu Enable
        tk.Checkbutton(self.master, text='Enable Advanced Features', variable=self.Advanced_EN, command=self.AdvancedFeatures).pack()

        #Command Frame
        self.CommandFrame = tk.LabelFrame(self.master,text="Command Menu")
        tk.Label(self.CommandFrame,text="Command:").grid(row=1,column=1,sticky='W')
        self.Command = ttk.Combobox(self.CommandFrame,value=temp_commands) ##Makes the menu
        self.Command.set('$build,*') ## Set Default Value
        self.Command.grid(row=1,column=2,sticky='W',ipadx=70) ## Positions the Menu
        self.b1 = tk.Button(self.CommandFrame,text='Send',command=self.CommandLineSend,width=BUTTONWIDTH,state='disabled')
        self.b1.grid(row=1,column=3)
        tk.Label(self.CommandFrame,text='TX:').grid(row=2,column=1,sticky='E')
        tk.Label(self.CommandFrame,text='RX:').grid(row=3,column=1,sticky='E')
        self.tx = tk.Label(self.CommandFrame,textvariable=self.tx_text).grid(row=2,column=2,sticky='W',columnspan=2)
        self.rx = tk.Label(self.CommandFrame,textvariable=self.rx_text).grid(row=3,column=2,sticky='W',columnspan=2) 
    
    def updatePeriod(self,event):
        value = self.PeriodScale.get()
        self.PERIOD = int(value)   

    def UpdateDSA(self,value):
        self.Send('$setdar,' + str(value) + ',*')

        self.DSA_text.set(value) 

    def UpdateCommunication(self,method):
        if(method == 'TCP'):
            self.Serial_EN.set(not self.Serial_EN.get())
            self.IP['state'] = 'normal'
            self.TCPPort['state'] = 'normal'
            self.SerialPort['state'] = 'disabled'
        else:
            self.TCP_EN.set(not self.TCP_EN.get())
            self.IP['state'] = 'disabled'
            self.TCPPort['state'] = 'disabled'
            self.SerialPort['state'] = 'normal'        
        self.Disconnect()

    def Connect(self):
        self.TCPPort.get()
        self.SerialPort.get()
        if(self.connectStatus == False):
            if(self.Serial_EN.get() == True):
                if(self.SerialPort.get() != ''):
                    try: 
                        self.StatusLightFlash('orange', False)                 
                        self.com = COMM.SerialConn(self.SerialPort.get())
                        time.sleep(1)
                        self.connectStatus = True   
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)
                    
            elif(self.TCP_EN.get() == True):
                #self.Band_text.set('TCP checkbox')
                if(self.IP.get() != '' and self.TCPPort.get() != ''):
                    try:
                        self.StatusLightFlash('orange', False)             
                        self.com = COMM.SocketConn(self.IP.get(),self.TCPPort.get())  
                    except Exception as e:
                        self.Error_text.set(e)
                        self.StatusLightFlash('red', True)        
            if(self.connectStatus == True):
                self.connectButton['text'] ='Disconnect'
                self.statusLightBackground.itemconfig(self.statusLight,fill='light green')
                self.UpdateAllEntryBoxes('state','normal')
                self.UpdateAllButtons('state', 'normal')
                #self.Polling()
        else:           
            self.Disconnect()

    def Disconnect(self):
        self.connectStatus = False
        self.connectButton['text'] ='Connect'
        self.statusLightBackground.itemconfig(self.statusLight,fill='red')
        self.clear()
        self.UpdateAllEntryBoxes('state','disabled')
        self.UpdateAllButtons('state', 'disabled')
        self.master.update()
        if(self.com != None):
            self.com.close()
        
    def CommandLineSend(self):
        self.flag = True
        message = self.Command.get()
        self.tx_text.set(message)
        reply = self.Send(message)
        self.rx_text.set(reply)

    def bandbutton(self, band):
        self.clear()
        reply = None
        reply = self.Send('$setst,' + str(band-1) + ',*')
        if(reply != None):
            self.Band_text.set(str(band))
            if band == 1:
                self.Band1['bg'] = 'green'       
            elif band == 2:    
                self.Band2['bg'] = 'green'
            elif band == 3:
                self.Band3['bg'] = 'green'
            else:
                pass
    
    def clear(self):
        text = ''
        self.DSA_text.set(text)
        self.DSAScale.set('0.5')
        self.DSAScale.set('0.5')
        self.Band1['bg'] = '#F0F0F0' #Not quite the correct colour for some reason
        self.Band2['bg'] = '#F0F0F0'
        self.Band3['bg'] = '#F0F0F0'    
        #self.UpdateAllEntryBoxes('text', text)
        self.Band_text.set(text)
        self.voltage_text.set(text)
        self.current_text.set(text)
        self.Error_text.set(text)
        self.DSA_text.set(text)
        #self.LO_text.set(text)
        self.CID_text.set(text)
        self.UID_text.set(text)
        self.FirmwareVerison_text.set(text)
        self.tx_text.set(text)
        self.rx_text.set(text)

    def UpdateAllEntryBoxes(self, option, newstate):
        self.Band_entry[option]= newstate
        self.voltage_entry[option] = newstate
        self.current_entry[option] = newstate
        self.Error_entry[option] = newstate
        self.DSA_entry[option] = newstate
        #self.LO_entry[option] = newstate
        self.CID_entry[option] = newstate
        self.UID_entry[option] = newstate
        self.Firmware_entry[option] = newstate
        self.master.update()
    
    def UpdateAllButtons(self, option, newstate):   
        self.Band1[option] = newstate
        self.Band2[option] = newstate
        self.Band3[option] = newstate
        self.DSAScale[option] = newstate
        self.b1[option] = newstate
        self.master.update()

    def AdvancedFeatures(self):
        if(self.Advanced_EN.get()):
            self.PasswordFrame = tk.LabelFrame(self.master)
            self.pass_text = tk.StringVar()
            tk.Label(self.PasswordFrame,text='Enter Password:').grid(row=1,column=1,sticky='E')
            tk.Entry(self.PasswordFrame,textvariable=self.pass_text,state='normal').grid(row=1,column=2)
            self.PasswordButton = tk.Button(self.PasswordFrame,text='Enter',command=self.passcheck,width=BUTTONWIDTH)
            self.PasswordButton.grid(row=1,column=3)
            self.PasswordFrame.pack()    
        else:
            self.CommandFrame.pack_forget()
            self.PasswordFrame.pack_forget()

    def passcheck(self):
        if(self.pass_text.get() == PASSWORD):
            self.PasswordFrame.pack_forget()
            self.CommandFrame.pack(expand='True',fill='both',padx=5,pady=5)
        else:
            tk.Label(self.PasswordFrame,text='Password Incorrect').grid(row=2,column=2)

    def StatusLightFlash(self,colour,flash):
        startingcolour = self.statusLightBackground.itemcget(self.statusLight,'fill')  
        delay = 0.25
        cycles = 2
        i = 0
        colour1 = colour
        colour2 = colour
        self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
        self.master.update()
        if(flash == True):
            colour2 = 'dark ' + colour
            while(i <= cycles):
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
                self.master.update()
                time.sleep(delay/2)
                self.statusLightBackground.itemconfig(self.statusLight,fill=colour2)
                self.master.update()
                time.sleep(delay/2)
                i += 1
            self.statusLightBackground.itemconfig(self.statusLight,fill=colour1)
            self.master.update()

    def FilterReceived(self, message):
        message = message.lower()
        reply = message.split(',')
        for i,x in enumerate(reply):
            if(x == '0'): #if info not available
                reply[i] = 'N/A'

        command = reply[0]

        if(command == '$buildr'):
            self.CID_text.set(reply[1])  
            self.UID_text.set(reply[2]) 
            self.FirmwareVerison_text.set(reply[4]+reply[5])
        elif(command == '$statr'):
            self.CID_text.set(reply[1])
            self.UID_text.set(reply[2]) 
            self.Band_text.set(reply[4]) 
            self.voltage_text.set(reply[5])
            self.current_text.set(reply[7])
            self.Error_text.set(reply[8])
        elif(command == '$setdar'):
            self.DSAScale.set(reply[5])           
        self.master.update()

    def Send(self, message):
        reply = None
        if(self.com != None):
            try:
                reply = self.com.send(message)
                self.FilterReceived(reply)
            except Exception as e:
                self.Error_text.set(e)
        return reply

    def Polling(self):
        if(self.connectStatus == True):
            self.COUNTER+=1  
            self.Send('$getst,*')
            self.Send('$build,*')
        self.Error_text.set(self.COUNTER)
        myapp.after(self.PERIOD*1000,myapp.Polling)


if __name__ == '__main__': 
    master = tk.Tk()
    myapp = GUI(master)
    myapp.after(100,myapp.Polling)
    myapp.mainloop()
