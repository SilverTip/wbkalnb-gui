#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-231 - Version 1. "As Benjamin see's it"

import os, time, csv
import pyvisa as visa
import datetime
import tkinter as tk
from tkinter import ttk
import numpy as np

version = 1

temp_commands = ['$build,*', '$getsv,*', '$getip,*', '$reset,*' ]
rx = ''
tx = ''

def CommandGet():
    message = CommandDropdown.get()
    SendCommand(message)

def CommandLine():
    message = Cmd.get()
    SendCommand(message)

def SendCommand(message):

    rx = tk.Label(CommFrame,text='                                                       ').grid(row=1,column=2,sticky='W',columnspan=2)
    tx = tk.Label(CommFrame,text='                                                       ').grid(row=2,column=2,sticky='W',columnspan=2)
    rx = tk.Label(CommFrame,text=message).grid(row=1,column=2,sticky='W',columnspan=2)
    tx = tk.Label(CommFrame,text=message + ' + received').grid(row=2,column=2,sticky='W',columnspan=2)

    

def GUI():
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title('KaLNB GUI Interface R' + str(version))

    #GUI Variable list
    global Cmd, CommandDropdown, CommFrame, tx, rx

    #Command Frame
    CommandLineFrame = tk.LabelFrame(master,text="Command Menu")
    CommandLineFrame.grid(row=1,column=1,sticky='W')

    #Command Dropdown Menu
    tk.Label(CommandLineFrame,text="Command List:").grid(row=1,column=1,sticky='W')
    CommandDropdown = ttk.Combobox(CommandLineFrame,value=temp_commands) ##Makes the menu
    CommandDropdown.set('$build,*') ## Set Default Value
    CommandDropdown.grid(row=1,column=2,sticky='W',ipadx=25) ## Positions the Menu
    b1 = tk.Button(CommandLineFrame,text='  Send  ',command=CommandGet).grid(row=1,column=3)

    #Command Line
    tk.Label(CommandLineFrame,text='Command:').grid(row=2,column=1,sticky='E')
    Cmd = tk.Entry(CommandLineFrame)
    Cmd.grid(row=2,column=2,sticky='W',ipadx=25)
    b2 = tk.Button(CommandLineFrame,text='  Send  ',command=CommandLine).grid(row=2,column=3,sticky='E')

    #Communication Frame
    CommFrame = tk.LabelFrame(master,text='Communication')
    CommFrame.grid(row=2,column=1,sticky='W',ipadx=67)
    tk.Label(CommFrame,text='TX:').grid(row=1,column=1,sticky='E')
    tk.Label(CommFrame,text='RX:').grid(row=2,column=1,sticky='E')    

    master.mainloop()


GUI()   


