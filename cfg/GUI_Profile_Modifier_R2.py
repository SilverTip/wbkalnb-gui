#KaLNB GUI
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#   ------Version Log-------
#   R1 - 2023-06-26 -   Developed to load/save encrypted .cfg files for Orbital KaLNB GUI
#                       Model class manages path, file type (accepts .json + encrypted .cfg), and encryption
#                       Convert .csv profiles to .cfg or .json files
#                       Search cd to preload profiles into GUI for customer. GUI handles filedialog search for profiles
#   R2 - 2023-07-11 -   Obscure important information
#                       Fix Model filepath bug

import json
import csv
import os
from cryptography.fernet import Fernet

class Model:
    def __init__(self, path: str):
        self.path = path
        self.filetype = os.path.splitext(path)[1]
        self.is_valid = self.is_valid_filetype()
        self.encryption = False
        if self.is_valid:
            self.data = self.load(path)
            if type(self.data) == bytes:
                self.encryption = True
            if self.encryption:
                try:
                    self.settings = self.decryptData()
                except Exception:
                    self.is_valid = False
            else:
                self.settings = self.data
        if self.is_valid:
            self.is_valid = self.is_valid_settings()
        if self.is_valid:
            self.name = self.settings['name']

    def load(self, filepath: str) -> any:
        data = None
        if self.filetype == '.cfg':
            with open(filepath, 'rb') as file:
                data = file.read()
        elif self.filetype == '.json':
            with open(filepath, 'r') as f:
                data = json.load(f)

        return data

    def save(self, path: str):
        if self.encryption:
            encrypted_data = self.encryptSettings()
            with open(f"{path}\\{self.name}.cfg", 'wb') as f:
                f.write(encrypted_data)
            return
        with open(f"{path}\\{self.name}.json", 'w') as f:
            json.dump(self.settings, f)

    def is_valid_filetype(self) -> bool:
        if self.filetype == '.cfg':
            return True
        elif self.filetype == '.json':
            return True
        return False

    def is_valid_settings(self) -> bool:
        if 'name' not in self.settings:
            return False
        if 'serial' not in self.settings:
            return False
        return True

    @staticmethod
    def text() -> str:
        str1 = 'tIw9aa4W_vaYuNlXkCRbUyvc'
        num = 2
        str2 = 'NqjDGLZcKMCHNC'
        num2 = 5
        str3 = 'M='
        return str1 + str(num * 29) + str2 + str(num2 * 18) + str3

    def encryptSettings(self) -> bytes:
        fernet = Fernet(self.text())
        encrypted_data = fernet.encrypt(json.dumps(self.settings).encode('utf-8'))
        return encrypted_data

    def decryptData(self) -> dict:
        fernet = Fernet(self.text())
        decrypted_data = json.loads(fernet.decrypt(self.data))
        return decrypted_data

class Model2:
    def __init__(self, data: dict):
        self.settings = data
        self.is_valid = self.is_valid_settings()
        if self.is_valid:
            self.name = self.settings['name']

    def save(self, path: str, encryption=True):
        if encryption:
            encrypted_data = self.encryptSettings()
            with open(f"{path}\\{self.name}.cfg", 'wb') as f:
                f.write(encrypted_data)
            return
        with open(f"{path}\\{self.name}.json", 'w') as f:
            json.dump(self.settings, f)

    def is_valid_settings(self) -> bool:
        if 'name' not in self.settings:
            return False
        if 'serial' not in self.settings:
            return False
        return True

    @staticmethod
    def text() -> str:
        str1 = 'tIw9aa4W_vaYuNlXkCRbUyvc'
        num = 2
        str2 = 'NqjDGLZcKMCHNC'
        num2 = 5
        str3 = 'M='
        return str1 + str(num * 29) + str2 + str(num2 * 18) + str3

    def encryptSettings(self) -> bytes:
        fernet = Fernet(self.text())
        encrypted_data = fernet.encrypt(json.dumps(self.settings).encode('utf-8'))
        return encrypted_data

def GenerateEncryptiontext():
    key = Fernet.generate_key()
    name = 'filekey.key'
    if os.path.isfile(name):
        raise OSError("Key file already exists")
    with open(name, 'wb') as filekey:
        filekey.write(key)

def Load_All_Settings(file: str) -> list[dict[any]]:
    # Open .csv as matrix
    with open(file) as f:
        file = csv.reader(f)
        matrix = list(file)

    # Generate list of dictionaries from matrix, grouping bands together if model name left blank
    key_list = matrix[0]
    output_list = []
    matrix.reverse()
    for rows in matrix:
        temp_dict = {}
        for index, value in enumerate(rows):
            if value == key_list[index]:  # Skips all keys
                continue
            if value == 'TRUE':
                value = True
            if value == 'FALSE':
                value = False
            if (key_list[index] == 'band_name' or key_list[index] == 'name' or key_list[
                index] == 'serial'):  # Only columns preserved as string
                temp_dict[key_list[index]] = value
            else:
                temp_dict[key_list[index]] = int(value)
        output_list.append(temp_dict)
    output_list.reverse()

    output_list.pop(0)

    return output_list

def convertCSVtoJSON(filename: str):
    settings = Load_All_Settings(filename)
    for index,value in enumerate(settings):
        model = Model2(value)
        model.save(os.getcwd(),encryption=False)

def convertCSVtoCFG(filename: str):
    settings = Load_All_Settings(filename)
    for index,value in enumerate(settings):
        model = Model2(value)
        model.save(os.getcwd())

def dumpJSON(filename: str, data: dict):
    with open(filename, 'w') as f:
        json.dump(data, f)

def openKey(file: str) -> str:
    with open(file, 'r') as f:
        key = f.read()
    return key

convertCSVtoJSON('GUI_MODEL_REF_R02_06_03.csv')
#convertCSVtoCFG('GUI_MODEL_REF_R02_06.csv')
