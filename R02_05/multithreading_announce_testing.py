import threading
import socket
import time

BUFFER_SIZE = 1024
PORT = 30303
FOUND_DEVICES = []
IP = '0.0.0.0'
#IP = socket.gethostbyname(socket.gethostname())

def listen_for_announcements():
    print('[THREAD2] Begin\n')
    # create a UDP socket and bind it to the listening port
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((IP, PORT))
    sock.settimeout(35)
    begin = time.time()
    while time.time() - begin < 35:
        # listen for incoming UDP packets and print them to the console
        print('[THREAD2] Waiting for announce...\n')
        try:
            data, addr = sock.recvfrom(BUFFER_SIZE)
        except:
            break
        FOUND_DEVICES.append(addr)
        print(f'[THREAD2] Received announcement from {addr[0]}:{addr[1]}: {data.decode()}\n')

    print('[THREAD2] Exit\n')

def main():
    print('[THREAD1] Begin\n')
    # start the listener thread
    listener_thread = threading.Thread(target=listen_for_announcements)
    listener_thread.start()

    # sleep for 5 seconds in the main thread
    i = 0
    while i < 8:
        print('[THREAD1] Sleeping...\n')
        time.sleep(5)
        print('[THREAD1] Wake up\n')
        i += 1

    print('\n[THREAD1] Found Devices:')
    print('--------------------------')
    for item in FOUND_DEVICES:
        print(item)

    print('[THREAD1] Exit')

if __name__ == '__main__':
    main()
    print('END PROGRAM')