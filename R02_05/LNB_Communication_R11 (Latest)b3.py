# LNB_Communication
# Written by Benjamin Stadnik
# Orbital Research Ltd.

import socket
import serial
import time
import re
import serial.tools.list_ports

ENCODE_FORMAT = 'utf-8'


class RS485Connection:  # TriKa/DKa/DKu communications
    STX = b'\x02'
    EXT = b'\x03'
    ACK = b'\x06'

    def __init__(self, port):
        self.addr = b'\x39\x32'
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1.0
        )
        self.connection = True

    def close(self):
        self.ser.close()
        self.connection = False

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        command = self.STX + self.addr + message.encode(ENCODE_FORMAT) + self.EXT
        checksum = 0
        for num in command:
            checksum ^= num
        command += checksum.to_bytes(1, byteorder='big')
        # if self.connection:
        self.ser.write(command)

    def read(self) -> list:
        character = ''
        word = ''
        reply = []
        if not self.connection:
            return reply
        while (self.ser.in_waiting > 0) and self.ser.isOpen():
            character = self.ser.read(1)
            if character == self.EXT:
                checksum = self.ser.read(1)  # Next byte is checksum ignore for now
                if word:
                    reply.append(word)
                    word = ''
            elif character == self.ACK:
                if word:
                    reply.append(word)
                    word = ''
                address = self.ser.read(2)  # next 2 bytes are address, so we can remove and ignore them (for now)
            else:
                try:
                    character = character.decode(ENCODE_FORMAT)
                except UnicodeDecodeError:
                    continue
                word += character
        if word:
            reply.append(word)

        return reply

    def query(self, message: str) -> list:
        # if self.connection:
        self.send(message)
        time.sleep(self.delay)  # unstable <= 0.12.
        reply = self.read()

        return reply


class RS232Connection:  # WBKaLNB serial connection
    def __init__(self, port: int):
        self.delay = 0.3
        self.port = port
        self.connection = False
        self.open()

    def open(self):
        self.ser = serial.Serial(
            port='COM' + str(self.port),
            baudrate=38400,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
        )
        self.connection = True

    def close(self):
        self.connection = False
        self.ser.close()

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        # if self.connection:
        tx = bytes(str(message) + '\r\n', ENCODE_FORMAT)
        self.ser.write(tx)

    def read(self) -> list:
        character = ''
        word = ''
        reply = []
        if self.connection:
            while self.ser.in_waiting > 0:
                character = self.ser.read(1)
                try:
                    character = character.decode(ENCODE_FORMAT)
                except UnicodeDecodeError:
                    continue  # decode of character failed
                word += character

            reply = word.split('\r\n')

        if (reply[-1]) == '':
            reply.pop(-1)

        return reply

    def query(self, message: str) -> list:
        # if self.connection:
        self.send(message)
        time.sleep(self.delay)  # unstable <= 0.12.
        reply = self.read()

        return reply


class SocketConnection:  # WBKaLNB network connection
    def __init__(self, host: str, port: int):
        self.timeout = 5
        self.delay = 0.15
        self.bytes = 1024
        self.address = (host, port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(self.timeout)
        self.connection = False
        self.open()

    def open(self) -> None:
        self.sock.connect(self.address)
        self.connection = True

    def close(self) -> None:
        self.sock.close()
        self.connection = False

    def test(self) -> list:
        return self.query("$build,*")

    def send(self, message: str) -> None:
        if self.connection:
            self.sock.sendall(bytes(str(message) + '\r\n', ENCODE_FORMAT))

    def read(self) -> list:
        reply = []
        word = ''
        if self.connection:
            received = self.sock.recv(self.bytes)
            for item in received:
                try:
                    character = chr(item)
                except:
                    # binary decode failed due to garbage non-ASCII characters
                    continue
                word += character
            reply = word.split('\r\n')
            if (reply[-1]) == '':
                reply.pop(-1)

        return reply

    def query(self, message: str) -> list:
        if self.connection:
            self.send(message)
            time.sleep(self.delay)
            reply = self.read()

            return reply


def dB_to_Hex(version: int, dB: float) -> str:
    hex_value = '0x02'
    if dB > 31.5:  # Maximum value
        dB = 31.5
    if dB < 0:  # Minimum value
        dB = 0
    if version == 1:
        decimal = dB % 1
        if decimal == 0.25 or decimal == 0.75:
            dB += 0.25  # Round up
        hex_value = hex(int(63 - (dB * 2)))
    elif version == 2:
        hex_value = hex(int(dB / 0.25))

    return hex_value


def Hex_to_dB(version: int, hex_value: str) -> float:
    dB = 0.5
    if version == 1:
        dB = abs(int(hex_value, 16) - 63) / 2
    elif version == 2:
        dB = int(hex_value, 16) * 0.25

    return dB


class Search:
    # Class to manage searching for Orbital devices on a network or local serial devices
    # Main methods:
    #   Serial()
    #           Input:      None
    #           Output:     None
    #           Function:   Searches every serial port on local machine and returns items into self.serial_devices
    #           Info:       Compatible devices are stored as class instance rather than return function
    #                       Device Port, name, type are stored as dict in format of serial_device_formatting
    #
    #   Network(solicitation, verbose)
    #           Input:      Solicitation; True = discovery feature on port 30304, False = announce feature on port 30303
    #                       Verbose; default=False, True = print annoucmeents to console as they are obtained
    #           Output:     None
    #           Function:   Searches network devices according to solicitation.
    #                       Saves raw announcements into self.announcements list
    #                       Saves decoded accouncements into self.network_devices
    #           Info:       Compatible devices are stored as class instance rather than return function
    #                       Device IP, name, type, etc. are stored as dict according to TLV_to_Useful
    #   TLV_Readable_Dump()
    #           Input:      None
    #           Output:     None
    #           Function:   Convert contents of self.announcements into corresponding TLV contents and print to console

    target = '255.255.255.255'
    port_solicited = 30304
    port_unsolicited = 30303
    solicited_search_delay = 0.2
    unsolicited_search_delay = 35
    serial_device_formatting = {"ID": 'Unknown', "Port": 'Unknown', "Type": 'Unknown', "Firmware": 'Unknown'}

    def __init__(self):
        self.serial_devices = []
        self.network_devices = []
        self.announcements = []

    def Serial(self, com_type='RS485') -> list:
        # Check each device connected COM channels
        devices = []
        for port in self.COMList():
            device = self.serial_device_formatting.copy()
            build = []
            if com_type != 'RS485':
                try:
                    com1 = RS232Connection(port)
                    build = com1.test()
                    com1.close()
                except OSError:
                    pass
            else:
                try:
                    com2 = RS485Connection(port)
                    build = com2.test()
                    com2.close()
                except OSError:
                    pass
            if build:
                device.update(self.Decode_Build(build))
                device['Type'] = com_type
                device['Port'] = port
                devices.append(device)

        self.serial_devices = devices
        return devices

    @staticmethod
    def Decode_Build(message: list) -> dict:
        build = {'ID': 'Unknown', 'Firmware': 'Unknown'}
        if not message:
            return build
        for item in message:
            if item.count('$BUILD'):
                build_string = item.split(',')
                if len(build_string) < 2:  # Item is echo: ['$build','*']
                    continue
                build['ID'] = build_string[1] + '-' + build_string[2]
                build['Firmware'] = build_string[4]
                return build

        return build

    @staticmethod
    def COMList() -> list:
        return [int(item.device.strip('COM')) for item in list(serial.tools.list_ports.comports())]

    @staticmethod
    def TLV_Decode(message: bytes) -> list:
        #   Input announcement, output list of dict of TLV fields
        #   Using Regular Expressions module and Horizon ICD_R7 (20220228)
        tlv_dict = {'field': None,
                    'description': None,
                    'address': None,
                    'sub': None,
                    'len': None,
                    'contents': None,
                    'value': None
                    }

        TLV = [
            (1, 'Chassis_ID', b'\x02', None, hex, '^([0-9A-Fa-f]{12})$'),
            (2, 'Port_ID', b'\x04', None, int, None),
            (3, 'Time-to-Live', b'\x06', None, int, None),
            (4, 'Port_Description', b'\x08', None, str, '^(ETH0)$'),
            (5, 'System_Name', b'\x0a', None, str, '^((ORBLNB-){1}[0-9A-Fa-f]{8})$'),
            (6, 'System_Description', b'\x0c', None, str,
             '^((LNB858-900XWS60,){1}[0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
            (7, 'System_Capability', b'\x0e', None, str, '^(LNB)$'),
            (8, 'Management_Address', b'\x10', None, str,
             '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'),
            (9, 'LLDP_MED_Capabilities', b'\xfe', b'\x01', int, None),
            (10, 'Hardware_Revision', b'\xfe', b'\x05', str, '^(r[0-9]{3})$'),
            (11, 'Firmware_Revision', b'\xfe', b'\x06', str, '^([0-9]{1,3}(.)[0-9A-Za-z]{1,3}(.)[0-9A-Za-z]{1,6})$'),
            (12, 'Serial_Number', b'\xfe', b'\x08', str, '^([0-9]{4,5})(-)[1-9]{1}[0-9]{0,4})$'),
            (13, 'Manufacturer_Name', b'\xfe', b'\x09', str, '^(ORBITAL RESEARCH)$'),
            (14, 'Model_Name', b'\xfe', b'\x0a', str, '^(LNB858-900XWS60)$'),
            (15, 'Asset_ID', b'\xfe', b'\x0b', str,
             '^((LNB858-900XWS60-00-){1}([0-9A-Fa-f]{2}[.]){5}[0-9A-Fa-f]{2})$')
        ]

        bytes_list = [x.to_bytes(1, 'big') for x in message]
        tlv_list = []
        for field, description, address, sub, character_format, decoder in TLV:
            if address == bytes_list[0]:
                index = 2
                tlv = tlv_dict.copy()
                if sub:
                    if sub == bytes_list[5]:
                        tlv['sub'] = '0x{:02x}'.format(int.from_bytes(sub, 'big'))
                        index = 6
                    else:
                        continue
                tlv['field'] = field
                tlv['description'] = description
                tlv['address'] = '0x{:02x}'.format(int.from_bytes(address, 'big'))
                tlv['len'] = int.from_bytes(bytes_list[1], "big")
                tlv['contents'] = ''.join('/0x{:02x}'.format(x) for x in [int.from_bytes(y, "big") for y in
                                                                          bytes_list[:index + tlv['len']]])
                trimmed_contents = b''.join(bytes_list[index:index + tlv['len']])
                if character_format == int:
                    tlv['value'] = int.from_bytes(trimmed_contents, 'big')
                elif character_format == hex:
                    tlv['value'] = trimmed_contents.hex()
                else:
                    tlv['value'] = trimmed_contents.decode('ascii')

                tlv_list.append(tlv)
                bytes_list = bytes_list[index + tlv['len']:]

        return tlv_list

    def TLV_Readable_Dump(self, filename=None) -> None:
        for announcement in self.announcements:
            tlv_list = self.TLV_Decode(announcement[0])
            bytes_list = ['{:02x}'.format(x) for x in [int.from_bytes(y, 'big') for y in
                                                       [z.to_bytes(1, 'big') for z in announcement[0]]]]
            # Hex dump
            i = 0
            print('HEX DUMP:')
            print('---------------------------------------------------------------------------------------------------')
            dump_len = 16
            while True:
                if i + dump_len > len(bytes_list):
                    r = len(bytes_list) - dump_len
                    print(' '.join(bytes_list[i:i + r]))
                    break
                print(' '.join(bytes_list[i:i + dump_len]))
                i += dump_len
            space = 36
            print('---------------------------------------------------------------------------------------------------')
            print(F"{'DESCRIPTION'.ljust(21)} | {'DECODED VALUE'.ljust(space)} | {'BYTES CONTENTS'.ljust(space)}")
            print('---------------------------------------------------------------------------------------------------')
            for item in tlv_list:
                description = item['description']
                contents = item['contents'].replace('/0x', ' ')
                value = str(item['value'])
                print(F'{description.ljust(21)} | {value.ljust(space)} | {contents.ljust(space)}')
            print('***************************************************************************************************')

    def TLV_to_Useful(self, tlv_list: list) -> dict:
        device = {}
        interested_categories = [
            ('Chassis_ID', 'MAC'),
            ('Management_Address', 'IP'),
            ('Serial_Number', 'S/N'),
            ('Firmware_Revision', 'F/W'),
            ('Hardware_Revision', 'H/W')
        ]
        for item in tlv_list:
            for category, name in interested_categories:
                if item['description'] == category:
                    device[name] = item['value']

        keys = list(device.keys())
        if not (keys.count('IP') and keys.count('MAC')):
            return {}
        return device

    @staticmethod
    def Listen(sock: socket.socket, timeout: float, verbose=False) -> list:
        response_list = []
        sock.settimeout(timeout)
        begin = time.time()
        try:
            while True:
                response = sock.recvfrom(2048)  # Blocking function. Timeout will cause exception
                response_list.append(response)
                now = time.time()
                if verbose:
                    print(f'Device found: {response[1]}, {now}')
                if now - begin > timeout:
                    raise socket.timeout
        except socket.timeout:
            return response_list

    def add_network_devices(self, announce_list: list) -> list:
        # announce_list is list of tuple (bytes, (address, port))
        for announcement in announce_list:
            if announcement not in self.announcements:
                self.announcements.append(announcement)

        devices = [self.TLV_to_Useful(self.TLV_Decode(a[0])) for a in announce_list]

        for device in devices:
            if not device:
                continue
            if device not in self.network_devices:
                self.network_devices.append(device)

        return devices

    def Network(self, solicitation: bool, duration=0, verbose=False) -> list:
        announce_list = []

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        #   Time ~20ms, worst is 120ms
        if solicitation:
            # Get Solicited Broadcast on port 30304
            if not duration:
                duration = self.solicited_search_delay
            sock.sendto(b'D\r\n', (self.target, self.port_solicited))
            announce_list += self.Listen(sock, duration, verbose)  # Measured worst case ~120ms
##            # Get Solicited Broadcast on port 30303 (non standard)
##            sock.sendto(b'D\r\n', (self.target, self.port_unsolicited))
##            announce_list += self.Listen(sock, duration, verbose)  # Measured worst case ~120ms

        # Time depends on delay
        else:
            # Wait for unsolicited announcements on port 30303
            if not duration:
                duration = self.unsolicited_search_delay
            sock.bind((socket.gethostbyname(socket.gethostname()), self.port_unsolicited))
            announce_list += self.Listen(sock, duration, verbose)

        return self.add_network_devices(announce_list)

    # def NetworkPeek(self) -> list:
    #     announce_list = []
    #     # Non-invasive network check that can be integrated into continuous loop of an application,
    #     # but with limited search success
    #     sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    #     sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    #     # Wait for unsolicited announcements on port 30303
    #     sock.bind((socket.gethostbyname(socket.gethostname()), self.port_unsolicited))
    #     announce_list += self.Listen(sock, self.unsolicited_search_peek_delay)
    #
    #     return self.add_network_devices(announce_list)


def Testing():
    COM_PORT = 13
    # com = SocketConnection('10.0.10.144', 32019)
    com = RS232Connection(COM_PORT)
    # com = RS485Connection(COM_PORT)
    while (1):
        try:
            message = input('Command:')
            # com.delay = 3.1
            if not com.connection:
                print("Open communications Method")
                com.open()
            reply = com.query(message)
            print(reply)
            # for item in reply: print(item)
        except KeyboardInterrupt:
            com.close()
            print('Closed communications method')


def Wut():
    while True:
        com1 = RS232Connection(13)
        print(f"[RS232] | Send: $getst,* | Reply: {com1.query('$getst,*')}")
        print(f"[RS232] | Send: $build,* | Reply: {com1.query('$build,*')}")
        print(f"[RS232] | Send: $getst,* | Reply: {com1.query('$getst,*')}")
        print(f"[RS232] | Send: $build,* | Reply: {com1.query('$build,*')}")
        print(f"[RS232] | Send: $getst,* | Reply: {com1.query('$getst,*')}")
        com1.close()
        print()
        time.sleep(1)
        com2 = RS485Connection(13)
        print(f"[RS485] | Send: $getst,* | Reply: {com2.query('$getst,*')}")
        print(f"[RS485] | Send: $build,* | Reply: {com2.query('$build,*')}")
        print(f"[RS485] | Send: $getst,* | Reply: {com2.query('$getst,*')}")
        com2.close()
        print()
        time.sleep(1)


def Discover():
    # Testing the Search class
    search = Search()  # Create class instance

    print('Searching for Serial Devices...')
    begin = time.time()
    search.Serial()
    end = time.time()
    print(f'Serial search duration: {end - begin}\n')
    print(f'Serial devices found:')
    for device in search.serial_devices:
        print(device)

    print('Searching for Network Devices...')
    begin = time.time()
    Readable=False
##    search.Network(solicitation=True, verbose=True)
    search.Network(solicitation=False, verbose=True)
    end = time.time()
    print(f'Network search duration: {end - begin}\n')
    print(f'Network devices found:')
    for device in search.network_devices:
        print(device)
    if Readable==True:
        search.TLV_Readable_Dump()


if __name__ == '__main__':
    # Wut()
    Discover()
    # Testing()

# Version Log
# ---------------------------------------
# R1 - 2022-03-16 - Import code from KaLNB_TCP_Serial_Communication_R6.py. Rename module to include all LNBs.
# R2 - 2022-03-23 - Filter responses based on type of connection - class methods to return interested values
# R3 - 2022-04-04 - filter, attenuation, and faults not dependent on comm type.
# R4 - 2022-04-12 - updated faults
# R5 - 2022-05-04 - RS232 and Socket updated with /r/n
# R6 - 2022-05-05 - Manage decode exceptions, lengthen read delay for longest command ("cfg", ~150ms)
# R7 - 2022-07-27 - Minor timing and filter changes
# R8 - 2022-07-27 - Minor filter changes
# R9 - 2022-09-22 - Filter changes
# R10 - 20221108 -  Move Filter to external database.
#                   RS485 dump everything after ACK into item in list.
#                   Breakup send/read/query. Attenuation function round up 0.25 and 0.75 for TriKa units
# R11 - 20230216 -  Add Search class to find serial and network devices
#                   Proper pythonic annotation
#                   Rename/split attenuation functions for ease of use
