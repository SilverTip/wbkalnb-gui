from LNB_Command_Manager_R1 import *

class VLNB:
    def __init__(self, model: dict):
        self.name = model['name']
        self.filter = model['filter']
        self.default_settings = {
            "YYYYMMDD": "20230101",
            "HHMMSS": "000000",
            "UTCOFFSET": "-800",
            "UNIXTS": "754405215",
            "DHCP_Retries": "0",
            "Static_IP": "192.168.1.199",
            "Static_Gateway": "192.168.1.1",
            "Static_Subnet": "255.255.255.0",
            "MAC": "e8.eb.1b.06.b6.b5",
            "LLDP_Enable": "0",
            "LLDP_Timer": "30",
            "Temperature_High": "80",
            "Temperature_Low": "-50",
            "Voltage_High": "280",
            "Voltage_Low": "115",
            "Current_High": "850",
            "Current_Low": "0",
            "Hostname": "ORBRES-VLNB",
            'Stack_L': '40',
            'CPU_H': '80'
        }
        self.settings = self.default_settings.copy()

        self.default_buffer = {
            'CID': 'VLNB',
            'UID': '001',
            'FS': '0x00010000',
            'SS': '0x01000000',
            'Fault': '0x0000',
            'Current': '100',
            'Voltage': '180',
            'Temperature': '50',
            'Band': '0',
            'Uptime': '0',
            'DSA': ['0x02' for i in range(10)],
            'Mute': '0',
            'IP': '192.168.1.199',
            'Subnet': '192.168.1.1',
            'Gateway': '255.255.255.0'
        }
        self.var = self.default_buffer.copy()

    def UptimePlusOne(self):
        self.var['Uptime'] = str(int(self.var['Uptime']) + 1)

    def update(self):
        self.UptimePlusOne()

    def query(self, rx: str) -> list:
        message = rx.split(',')
        message.pop(-1)  # Remove *

        try:
            if self.filter == 0:
                return self.TriKa_Device(message)
            else:
                return self.WB_Device(message)
        except Exception as e:
            return [F'[ERROR]: {e}']

    def TriKa_Device(self, message: list) -> list:
        reply = []
        command = message[0]
        response = ''
        if command == '$build':
            response = F"$build,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},3C.126 Oct 26 2016 11:05:27,*"
        elif command == '$getst':
            response = f"$statr,{self.var['CID']},{self.var['UID']},,{self.var['Band']},,{self.var['Voltage']},{self.var['Temperature']},\
{self.var['Current']},{self.var['Fault']},*"
        elif command == '$setst':
            self.var['Band'] = message[1]
            response = f"$statr,{self.var['CID']},{self.var['UID']},,{self.var['Band']},,{self.var['Voltage']},{self.var['Temperature']},\
{self.var['Current']},{self.var['Fault']},*"
        elif command == '$setda':
            if len(message) > 2:
                self.var['DSA'][int(self.var['Band'])] = message[2]
            response = F"$setdar,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{int(self.var['Band'])-1},{self.var['DSA'][int(self.var['Band'])-1]},*"
        elif command == '$reset':
            self.var['Uptime'] = '0'
        reply.append(response)
        return reply

    def WB_Device(self, message: list) -> list:
        reply = []
        command = message[0]
        response = ''
        if command == '$build':
            response = F"$buildr,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},0.01.124,9.31,WBLNB,Jan 01 2023 00:00:00,*"
        elif command == '$getst':
            response = f"$statr,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.var['Uptime']},,{self.var['SS']},{self.var['FS']},,,,\
{self.var['Voltage']},,{self.var['Temperature']},{self.var['Band']},*"
        elif command == '$setst':
            self.var['Band'] = message[1]
            response = f"$statr,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.var['Uptime']},,\
{self.var['SS']},{self.var['FS']},,,,{self.var['Voltage']},,{self.var['Temperature']},\
{self.var['Band']},*"
        elif command == '$setda':
            if len(message) > 2:
                self.var['DSA'][int(self.var['Band'])] = message[2]
            response = F"$setdar,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},,{self.var['DSA'][int(self.var['Band'])]},*"
        elif command == '$reset':
            self.var['Uptime'] = '0'
        elif command == '$mute':
            if message[2] == '1':
                self.var['FS'] = hex(int(self.var['SS'], 64) ^ int('0x00100000', 64))
            else:
                self.var['FS'] = hex(int(self.var['SS'], 64) | int('0x00100000', 64))
        elif command == '$clral':
            self.var['SS'] = '0x01000000'
        elif command == '$wrrtc':
            if len(message) > 1:
                self.settings['YYYYMMDD'] = message[1]
                self.settings['HHMMSS'] = message[2]
                self.settings['UTCOFFSET'] = message[3]
            response = F"$TIME,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['YYYYMMDD']},{self.settings['HHMMSS']},{self.settings['UTCOFFSET']},,*"
        elif command == '$wrutc':
            if len(message) > 1:
                self.settings['UNIXTS'] = message[1]
            response = F"$TIME,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['YYYYMMDD']},{self.settings['HHMMSS']},{self.settings['UTCOFFSET']},,*"
        elif command == '$getip':
            response = F"$IPCF,{self.var['CID']},{self.var['UID']},{self.var['IP']},{self.var['Subnet']},{self.var['Gateway']},{self.settings['MAC']},*"
        elif command == '$setip':
            if len(message) > 1:
                self.settings['DHCP_Retries'] = message[1]
                self.settings['Static_IP'] = message[2]
                self.settings['Static_Gateway'] = message[3]
                self.settings['Static_Subnet'] = message[4]
                if self.settings['DHCP_Retries'] == '-1':
                    self.var['IP'] = self.settings['Static_IP']
                    self.var['Gateway'] = self.settings['Static_Gateway']
                    self.var['Subnet'] = self.settings['Static_Subnet']
                else:
                    self.var['IP'] = '10.10.10.10'
                    self.var['Gateway'] = '10.1.1.1'
                    self.var['Subnet'] = '255.255.255.0'
            response = F"$SETIPR,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['DHCP_Retries']},{self.settings['Static_IP']},{self.settings['Static_Gateway']},{self.settings['Static_Subnet']},*"
        elif command == '$setth':
            if len(message) > 1:
                self.settings['Current_High'] = message[1]
                self.settings['Current_Low'] = message[2]
                self.settings['Temperature_High'] = message[3]
                self.settings['Temperature_Low'] = message[4]
                self.settings['Voltage_High'] = message[5]
                self.settings['Voltage_Low'] = message[6]
                if len(message) > 7:
                    self.settings['STACK_L'] = message[7]
                    self.settings['CPU_H'] = message[8]
            if self.filter == 1:
                response = F"$SETTHR,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['Current_High']},{self.settings['Current_Low']},{self.settings['Temperature_High']},{self.settings['Temperature_Low']},{self.settings['Voltage_High']},{self.settings['Voltage_Low']},{self.settings['Stack_L']},{self.settings['CPU_H']},*"
            else:
                response = F"$SETTHR,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['Current_High']},{self.settings['Current_Low']},{self.settings['Temperature_High']},{self.settings['Temperature_Low']},{self.settings['Voltage_High']},{self.settings['Voltage_Low']},*"
        elif command == '$setan':
            if len(message) > 1:
                self.settings['LLDP_Enable'] = message[1]
                self.settings['LLDP_Timer'] = message[2]
            response = F"$SETANR,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['LLDP_Enable']},{self.settings['LLDP_Timer']},2,4,*"
        elif command == '$sethost':
            if len(message) > 1:
                self.settings['Hostname'] = message[1]
            response = F"$HOSTR,{self.var['CID']},{self.var['UID']},{self.settings['UNIXTS']},{self.settings['Hostname']},*"
        elif command == '$getmm':
            response = F"$MMR,{self.var['CID']}-{self.var['UID']},ORBITAL RESEARCH,{self.name},,{self.settings['Hostname']},*"

        reply.append(response)
        return reply
