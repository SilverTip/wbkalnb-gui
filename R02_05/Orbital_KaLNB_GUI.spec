# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['GUI_R02_05_01.py'],
             pathex=[],
             binaries=[],
             datas=[
			 ('orblogob2.png', '.'),
			 ('GUI_COMMAND_REF_R02_05.csv', '.'),
			 ('GUI_ERROR_REF_R02_00.csv', '.'),
			 ('GUI_MODEL_REF_R02_05.csv', '.')
			 ],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='Orbital KaLNB GUI R02_05_01',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None, 
		  icon='orbicon.ico')
